import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Login, LoginGoogle, LoginFacebook } from '../../core/store/auth.action';
import { AuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  public user: any = SocialUser;
  platform: any;

  constructor(private formBuilder: FormBuilder, private store: Store, private authService: AuthService) {
    this.form = this.formBuilder.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onLogIn(form: FormGroup) {
    this.formSubmitAttempt = true;
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new Login(form.value)).subscribe(
        () => {
          this.setSubmitButton(true);
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }

  signInWithGoogle(platform: any): void {
    platform = GoogleLoginProvider.PROVIDER_ID;
    this.authService.signIn(platform).then(response => {
      this.user = response.authToken;
      const payload = {
        access_token: response.authToken,
        provider: 'google'
      };
      this.store.dispatch(new LoginGoogle(payload)).subscribe(
        () => {
          console.log('success');
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  signInWithFacebook(platform: any): void {
    // this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    platform = FacebookLoginProvider.PROVIDER_ID;
    this.authService.signIn(platform).then(response => {
      this.user = response.authToken;
      const payload = {
        access_token: response.authToken,
        provider: response.provider
      };
      this.store.dispatch(new LoginFacebook(payload)).subscribe(
        () => {
          console.log('success');
        },
        err => {
          console.log(err);
        }
      );
    });
  }
}
