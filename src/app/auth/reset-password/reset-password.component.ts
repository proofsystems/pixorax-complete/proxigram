import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ResetPassword } from '../../core/store/auth.action';
import { ActivatedRoute } from '@angular/router';
import { checkIfMatchingPasswords } from '@app/shared/reusable-custom-validators/matching-passwords.validator';

// NGXS
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;

  constructor(private formBuilder: FormBuilder, private store: Store, private route: ActivatedRoute) {
    this.form = this.formBuilder.group(
      {
        email: [null, Validators.required],
        token: [null, Validators.required],
        password: [null, [Validators.required, Validators.minLength(8)]],
        password_confirmation: [null, [Validators.required, Validators.minLength(8)]]
      },
      { validator: checkIfMatchingPasswords('password', 'password_confirmation') }
    );

    this.route.url.subscribe(val => {
      console.log(val[2].path);
      console.log(val[3].path);
      this.form.get('email').setValue(val[2].path);
      this.form.get('token').setValue(val[3].path);
    });
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onResetPassword(form: FormGroup) {
    this.formSubmitAttempt = true;
    if (form.valid) {
      console.log(form.value);
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new ResetPassword(form.value)).subscribe(
        () => {
          this.setSubmitButton(true);
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}
