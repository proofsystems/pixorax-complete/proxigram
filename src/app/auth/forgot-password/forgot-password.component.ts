import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RequestResetPassword } from '../../core/store/auth.action';

// NGXS
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

  constructor(private formBuilder: FormBuilder, private store: Store) {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(this.emailRegEx)]]
    });
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onRequestResetPassword(form: FormGroup) {
    this.formSubmitAttempt = true;
    if (form.valid) {
      console.log(form.value);
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new RequestResetPassword(form.value)).subscribe(
        () => {
          this.setSubmitButton(true);
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}
