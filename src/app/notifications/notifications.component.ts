import { Component, HostListener } from '@angular/core';
import { Observable } from 'rxjs';
import { Notification } from '@app/core/models/notification.model';
import { environment } from '@env/environment';

// NGXS
import { NotificationState } from '../core/store/notification.state';
import { Select, Store } from '@ngxs/store';
import { GetNotificationsFirstPage, GetNotificationsPerPage } from '../core/store/notification.action';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent {
  currentPage = 1;
  lastPage: number;

  // NGXS
  @Select(NotificationState.GetAllNotifications) notifications$: Observable<Notification[]>;
  @Select(NotificationState.getLastPage) lastPage$: Observable<number>;

  constructor(private store: Store) {
    this.store.dispatch(new GetNotificationsFirstPage('', '1'));

    this.lastPage$.subscribe((val) => {
      this.lastPage = val;
    });
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  @HostListener("window:scroll", [])
  onScroll(): void {
    if (this.bottomReached()) {
      if (this.currentPage < this.lastPage) {
        this.currentPage += 1;
        this.store.dispatch(new GetNotificationsPerPage('', this.currentPage.toString()));
      }
    }
  }

  bottomReached(): boolean {
    return (window.innerHeight + window.scrollY) >= document.body.offsetHeight;
  }
}
