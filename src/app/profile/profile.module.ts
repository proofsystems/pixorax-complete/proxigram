import { NgModule } from '@angular/core';
import { ProfileRoutingModule } from './profile-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileFollowComponent } from './profile-follow/profile-follow.component';
import { ProfileViewOthersComponent } from './profile-view-others/profile-view-others.component';

@NgModule({
  declarations: [ProfileViewComponent, ProfileEditComponent, ProfileViewOthersComponent, ProfileFollowComponent],
  imports: [ProfileRoutingModule, SharedModule]
})
export class ProfileModule {}
