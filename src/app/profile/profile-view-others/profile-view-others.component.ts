import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile } from '@app/core/models/profile.model';
import { environment } from '@env/environment';
import { ActivatedRoute } from '@angular/router';
import { Post } from '@app/core/models/post.model';
import { Story } from '@app/core/models/story.model';
import { StoryModalComponent } from '@app/core/modals/story-modal/story-modal.component';

// NGXS
import { Store, Select } from '@ngxs/store';
import { ProfileState } from '@app/core/store/profile.state';
import { GetOtherProfile, FollowUser } from '@app/core/store/profile.action';
import { PostState } from '@app/core/store/post.state';
import { GetOthersPostsFirstPage, GetOthersPostsPerPage } from '@app/core/store/post.action';
import { SelectStory } from '@app/core/store/story.action';

// NG-Bootstrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-profile-view-others',
  templateUrl: './profile-view-others.component.html',
  styleUrls: ['./profile-view-others.component.scss']
})
export class ProfileViewOthersComponent {
  id: number;
  currentPage = 1;
  lastPage: number;

  // NGXS
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  @Select(PostState.getLastPage) lastPage$: Observable<number>;
  @Select(PostState.getOwnPost) posts$: Observable<Post[]>;

  constructor(private store: Store, private route: ActivatedRoute, private modalService: NgbModal) {
    this.route.params.subscribe(paramsId => {
      this.id = paramsId.id;
      this.store.dispatch(new GetOtherProfile(paramsId.id));
    });
    this.lastPage$.subscribe(val => {
      this.lastPage = val;
    });
    this.store.dispatch(new GetOthersPostsFirstPage('', '1', this.id));
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onFollow(profile: Profile) {
    this.store.dispatch(new FollowUser(profile)).subscribe(() => {
      this.store.dispatch(new GetOtherProfile(this.id));
    });
  }

  onScroll() {
    if (this.currentPage < this.lastPage) {
      this.currentPage += 1;
      this.store.dispatch(new GetOthersPostsPerPage('', this.currentPage.toString(), this.id));
    }
  }

  withStory(stories: Story[]) {
    if (stories) {
      return true;
    } else {
      return false;
    }
  }

  openStoryModal(stories: Story[]) {
    this.store.dispatch(new SelectStory(stories)).subscribe(() => {
      this.modalService.open(StoryModalComponent);
    });
  }
}
