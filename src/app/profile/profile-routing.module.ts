import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
// import { EditProfileGuard } from '@app/core/guards/edit-profile-guard.service';
import { ProfileFollowComponent } from './profile-follow/profile-follow.component';
import { ProfileViewOthersComponent } from './profile-view-others/profile-view-others.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileViewComponent
  },
  {
    path: 'edit',
    component: ProfileEditComponent
  },
  {
    path: 'follow',
    component: ProfileFollowComponent
  },
  {
    path: ':id',
    component: ProfileViewOthersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}
