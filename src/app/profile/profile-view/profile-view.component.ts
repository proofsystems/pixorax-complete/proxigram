import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile } from '@app/core/models/profile.model';
import { environment } from '@env/environment';
import { Story } from '@app/core/models/story.model';
import { StoryModalComponent } from '@app/core/modals/story-modal/story-modal.component';

// NGXS
import { Store, Select } from '@ngxs/store';
import { ProfileState } from '../../core/store/profile.state';
import { GetProfile } from '../../core/store/profile.action';
import { PostState } from '@app/core/store/post.state';
import { Post } from '@app/core/models/post.model';
import { GetOwnPostsFirstPage, GetOwnPostsPerPage } from '@app/core/store/post.action';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { AuthState } from '@app/core/store/auth.state';
import { Logout } from '@app/core/store/auth.action';
import { SelectStory } from '@app/core/store/story.action';

// NG-Bootstrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent {
  currentPage = 1;
  lastPage: number;

  // NGXS
  @Select(PostState.getLastPage) lastPage$: Observable<number>;
  provider = this.store.selectSnapshot(AuthState.getProvider);
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  @Select(PostState.getOwnPost) posts$: Observable<Post[]>;

  // Font Awesome
  faPen = faPen;

  constructor(private store: Store, private modalService: NgbModal) {
    this.lastPage$.subscribe(val => {
      this.lastPage = val;
    });

    this.store.dispatch(new GetProfile());
    this.store.dispatch(new GetOwnPostsFirstPage('', '1'));
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onScroll() {
    if (this.currentPage < this.lastPage) {
      this.currentPage += 1;
      this.store.dispatch(new GetOwnPostsPerPage('', this.currentPage.toString()));
    }
  }

  onLogoutOut() {
    this.store.dispatch(new Logout());
  }

  withStory(stories: Story[]) {
    if (stories) {
      return true;
    } else {
      return false;
    }
  }

  openStoryModal(stories: Story[]) {
    this.store.dispatch(new SelectStory(stories)).subscribe(() => {
      this.modalService.open(StoryModalComponent);
    });
  }
}
