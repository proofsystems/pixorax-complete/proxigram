import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile } from '@app/core/models/profile.model';
import { Post } from '@app/core/models/post.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Country } from '@app/core/models/country.model';
import { setAsTouched } from '@app/shared/reusable-custom-validators/set-as-touched.validator';
import { environment } from '@env/environment';

// NGXS
import { Store, Select } from '@ngxs/store';
import { ProfileState } from '../../core/store/profile.state';
import { GetProfile, EditProfile } from '../../core/store/profile.action';
import { PreviewImageVideo } from '@app/core/store/post.action';
import { PostState } from '@app/core/store/post.state';
import { Navigate } from '@ngxs/router-plugin';
import { GetAllCountries } from '@app/shared/store/country.action';
import { CountryState } from '@app/shared/store/country.state';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent {
  url: string | ArrayBuffer;
  selectedImage: File;
  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  username: string;

  // NGXS
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  @Select(PostState.getFileUrl) url$: Observable<Post>;
  @Select(CountryState.getAllCountries) countries$: Observable<Country[]>;

  constructor(private store: Store, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      fname: [null, Validators.required],
      lname: [null, Validators.required],
      email: [null, Validators.required],
      username: [null, Validators.required],
      country_id: [null, Validators.required],
      description: [null, Validators.required],
      image: [null]
    });
    this.store.dispatch(new GetAllCountries());

    this.store.dispatch(new GetProfile()).subscribe(val => {
      console.log(val.profile.profile);
      setAsTouched(this.form);
      // this.form.patchValue(val.profile.profile);
      this.form.get('fname').setValue(val.profile.profile.fname);
      this.form.get('lname').setValue(val.profile.profile.lname);
      this.form.get('email').setValue(val.profile.profile.email);
      this.form.get('country_id').setValue(val.profile.profile.country_id);
      this.form.get('username').setValue(val.profile.profile.username);
      this.form.get('description').setValue(val.profile.profile.description);
      this.username = val.profile.profile.username;
    });
  }

  getUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onSelectImage(event: Event) {
    this.store.dispatch(new PreviewImageVideo(event, this.url));
    this.selectedImage = (event.target as HTMLInputElement).files[0];
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onEditProfile(form: FormGroup) {
    console.log(form.value);
    const formData = new FormData();
    formData.append('fname', form.value.fname);
    formData.append('lname', form.value.lname);
    formData.append('country_id', form.value.country_id);
    if (this.username !== form.value.username) {
      formData.append('username', form.value.username);
    }
    formData.append('description', form.value.description);
    if (this.selectedImage) {
      formData.append('image', this.selectedImage);
    }
    console.log(formData);
    this.formSubmitAttempt = true;
    if (form.valid) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      console.log('yeah');
      this.store.dispatch(new EditProfile(formData)).subscribe(
        () => {
          this.setSubmitButton(true);
          this.store.dispatch(new Navigate(['/profile']));
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}
