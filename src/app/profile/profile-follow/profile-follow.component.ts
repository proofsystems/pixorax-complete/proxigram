import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Profile } from '@app/core/models/profile.model';
import { environment } from '@env/environment';
import { StoryModalComponent } from '@app/core/modals/story-modal/story-modal.component';
import { Story } from '@app/core/models/story.model';

// NGXS
import { Store, Select } from '@ngxs/store';
import {
  GetOtherFollowersFirstPage,
  GetOwnFollowersFirstPage,
  GetOwnFollowingsFirstPage,
  GetOwnFollowersPerPage,
  GetOwnFollowingsPerPage,
  GetOtherFollowingsFirstPage,
  GetOtherFollowersPerPage,
  GetOtherFollowingsPerPage,
  FollowUser
} from '../../core/store/profile.action';
import { ProfileState } from '@app/core/store/profile.state';
import { SelectStory } from '@app/core/store/story.action';
import { Navigate } from '@ngxs/router-plugin';
import { AuthState } from '@app/core/store/auth.state';

// NG-Bootstrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-profile-follow',
  templateUrl: './profile-follow.component.html',
  styleUrls: ['./profile-follow.component.scss']
})
export class ProfileFollowComponent {
  currentPage = 1;
  lastPage: number;
  type: string;
  id: number;

  // NGXS
  @Select(ProfileState.getFollow) follows$: Observable<Profile[]>;
  userId = this.store.selectSnapshot(AuthState.getUserId);

  constructor(private route: ActivatedRoute, private store: Store, private modalService: NgbModal) {
    this.id = this.route.snapshot.queryParams['id'];
    this.type = this.route.snapshot.queryParams['type'];
    if (this.id) {
      if (this.type === 'followers') {
        this.store.dispatch(new GetOtherFollowersFirstPage('', '1', this.id)).subscribe(() => {});
      } else if (this.type === 'following') {
        this.store.dispatch(new GetOtherFollowingsFirstPage('', '1', this.id)).subscribe(() => {});
      }
    } else {
      if (this.type === 'followers') {
        this.store.dispatch(new GetOwnFollowersFirstPage('', '1'));
      } else if (this.type === 'following') {
        this.store.dispatch(new GetOwnFollowingsFirstPage('', '1'));
      }
    }
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onScroll() {
    if (this.currentPage < this.lastPage) {
      this.currentPage += 1;
      if (this.id) {
        if (this.type === 'followers') {
          this.store
            .dispatch(new GetOtherFollowersPerPage('', this.currentPage.toString(), this.id))
            .subscribe(() => {});
        } else if (this.type === 'following') {
          this.store
            .dispatch(new GetOtherFollowingsPerPage('', this.currentPage.toString(), this.id))
            .subscribe(() => {});
        }
      } else {
        if (this.type === 'followers') {
          this.store.dispatch(new GetOwnFollowersPerPage('', this.currentPage.toString()));
        } else if (this.type === 'following') {
          this.store.dispatch(new GetOwnFollowingsPerPage('', this.currentPage.toString()));
        }
      }
    }
  }

  onFollow(profile: Profile) {
    this.store.dispatch(new FollowUser(profile)).subscribe(() => {
      // this.store.dispatch(new GetOtherProfile(this.id));
    });
  }

  withStory(stories: Story[]) {
    if (stories) {
      return true;
    } else {
      return false;
    }
  }

  openStoryModal(stories: Story[]) {
    this.store.dispatch(new SelectStory(stories)).subscribe(() => {
      this.modalService.open(StoryModalComponent);
    });
  }

  navigateBasedOnStoriesAndUserId(hasStories: Story[], userId: number) {
    hasStories.length > 0
      ? []
      : userId === this.userId
      ? this.store.dispatch(new Navigate(['/profile']))
      : this.store.dispatch(new Navigate(['/profile/' + userId]));
  }

  navigateBasedOnUserId(userId: number) {
    userId === this.userId
      ? this.store.dispatch(new Navigate(['/profile']))
      : this.store.dispatch(new Navigate(['/profile/' + userId]));
  }
}
