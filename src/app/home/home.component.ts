import { Component, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '@app/core/models/post.model';
import { Story } from '@app/core/models/story.model';
import { environment } from '@env/environment';
import { Profile } from '@app/core/models/profile.model';
import { Comment } from '@app/core/models/comment.model';
import { NgForm } from '@angular/forms';
import { confirmDelete } from '@app/shared/reusable-functions/sweet-alerts';
import { StoryModalComponent } from '@app/core/modals/story-modal/story-modal.component';

// NGXS
import { Store, Select } from '@ngxs/store';
import {
  GetPostsPerPage,
  GetPostsFirstPage,
  DeletePost,
  DeleteCommentInPosts,
  AddCommentInPosts,
  LikeCommentInPosts
} from '@app/core/store/post.action';
import { PostState } from '@app/core/store/post.state';
import { GetStory, SelectStory } from '@app/core/store/story.action';
import { StoryState } from '@app/core/store/story.state';
import { GetProfile, GetSuggestedUsers, FollowUser } from '@app/core/store/profile.action';
import { ProfileState } from '@app/core/store/profile.state';
import { AuthState } from '@app/core/store/auth.state';
import { LikePostInPosts } from '@app/core/store/post.action';
import { Navigate } from '@ngxs/router-plugin';

// NG-Bootstrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewChecked {
  // profilePhoto: string;
  currentPage = 1;
  lastPage: number;
  posts: [];
  submitButtonStatus = true;

  // NGXS
  @Select(PostState.getAllPosts) posts$: Observable<Post[]>;
  @Select(PostState.getLastPage) lastPage$: Observable<number>;
  @Select(StoryState.getStories) stories$: Observable<Story[]>;
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  @Select(ProfileState.getSuggestedUsers) suggestedUsers$: Observable<Profile[]>;
  provider = this.store.selectSnapshot(AuthState.getProvider);
  userId = this.store.selectSnapshot(AuthState.getUserId);

  constructor(private store: Store, private modalService: NgbModal, private cdr: ChangeDetectorRef) {
    this.store.dispatch(new GetPostsFirstPage('', '1'));
    // this.store.dispatch(new GetPostsFirstPage('', '1')).subscribe(val => {
    //   this.posts = val.post.posts;
    // });
    this.lastPage$.subscribe(val => {
      this.lastPage = val;
    });

    this.store.dispatch(new GetStory());
    this.store.dispatch(new GetProfile());
    this.store.dispatch(new GetSuggestedUsers());

    // this.profilePhoto = this.store.selectSnapshot(AuthState.getProfilePhoto);
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  convertHashTags(str: string) {
    const spanned = `<span>${str}</span>`;
    return spanned.replace(/#([\w]+)/g, `<a href="/explore/tags/$1">#$1</a>`);
  }

  goToHashTag(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    const href = (event.target as HTMLAnchorElement).href;
    const tag = href.substring(href.indexOf('/explore/tags/') + '/explore/tags/'.length);
    console.log(tag);
    this.store.dispatch(new Navigate(['/explore/tags', tag]));
  }

  onScroll() {
    if (this.currentPage < this.lastPage) {
      this.currentPage += 1;
      // this.store.dispatch(new GetPostsPerPage('', this.currentPage.toString())).subscribe(val => {
      //   this.posts = val.post.posts;
      // });
      this.store.dispatch(new GetPostsPerPage('', this.currentPage.toString()));
    }
  }

  withStory(stories: Story[]) {
    if (stories) {
      return true;
    } else {
      return false;
    }
  }

  openStoryModal(stories: Story[]) {
    this.store.dispatch(new SelectStory(stories)).subscribe(() => {
      this.modalService.open(StoryModalComponent);
    });
  }

  navigateBasedOnStoriesAndUserId(hasStories: Story[], userId: number) {
    hasStories.length > 0
      ? []
      : userId === this.userId
      ? this.store.dispatch(new Navigate(['/profile']))
      : this.store.dispatch(new Navigate(['/profile/' + userId]));
  }

  navigateBasedOnUserId(userId: number) {
    userId === this.userId
      ? this.store.dispatch(new Navigate(['/profile']))
      : this.store.dispatch(new Navigate(['/profile/' + userId]));
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  isLiked(isLiked: number) {
    if (isLiked === 0) {
      return false;
    } else if (isLiked === 1) {
      return true;
    }
  }

  onLikePost(data: Post) {
    this.store.dispatch(new LikePostInPosts(data)).subscribe();
  }

  onDeletePost(caption: string, id: number) {
    confirmDelete(caption).then(result => {
      console.log(result);
      if (result.value) {
        this.store.dispatch(new DeletePost(id));
      }
    });
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onAddComment(f: NgForm, id: number) {
    this.setSubmitButton(false);
    const formdata = {
      comment: f.value.comment,
      post_id: id
    } as Comment;
    this.store.dispatch(new AddCommentInPosts(formdata)).subscribe(() => {
      f.reset();
      this.setSubmitButton(true);
    });
  }

  onLikeComment(data: Comment) {
    this.store.dispatch(new LikeCommentInPosts(data)).subscribe();
  }

  onDeleteComment(postId: number, comment: Comment) {
    confirmDelete(comment.comment).then(result => {
      console.log(result);
      if (result.value) {
        this.store.dispatch(new DeleteCommentInPosts(postId, comment.id));
      }
    });
  }

  onFollow(profile: Profile) {
    this.store.dispatch(new FollowUser(profile)).subscribe(() => {
      this.store.dispatch(new GetSuggestedUsers());
    });
  }
}
