import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DateAgoPipe } from './pipes/date-ago.pipe';
import { FormsModule } from '@angular/forms';

// NG-Bootstrap
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';

// NGX Infinite Scroll
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [HomeComponent, DateAgoPipe],
  imports: [CommonModule, RouterModule, FormsModule, NgbModalModule, InfiniteScrollModule, NgbCarouselModule]
})
export class HomeModule {}
