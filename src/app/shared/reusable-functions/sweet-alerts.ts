import swal from 'sweetalert2';

// Confirm Update Function
export function confirmUpdate() {
  return swal.fire({
    title: 'Update',
    text: 'Are you sure you want to update changes?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#0CC27E',
    cancelButtonColor: '#FF586B',
    confirmButtonText: 'Confirm',
    cancelButtonText: 'Cancel',
    confirmButtonClass: 'btn btn-success btn-raised mr-5',
    cancelButtonClass: 'btn btn-danger btn-raised',
    buttonsStyling: false
  });
}

// Confirm Delete Function
export function confirmDelete(text: string) {
  return swal.fire({
    title: 'Delete',
    text: `Are you sure you want to delete ${text}?`,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#0CC27E',
    cancelButtonColor: '#FF586B',
    confirmButtonText: 'Confirm',
    cancelButtonText: 'Cancel',
    confirmButtonClass: 'btn btn-success btn-raised mr-5',
    cancelButtonClass: 'btn btn-danger btn-raised',
    buttonsStyling: false
  });
}

// Confirm Function
export function customConfirm(title: string, text: string) {
  return swal.fire({
    title: `${title}`,
    text: `Are you sure you want to ${title} ${text}?`,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#0CC27E',
    cancelButtonColor: '#FF586B',
    confirmButtonText: 'Confirm',
    cancelButtonText: 'Cancel',
    confirmButtonClass: 'btn btn-success btn-raised mr-5',
    cancelButtonClass: 'btn btn-danger btn-raised',
    buttonsStyling: false
  });
}
