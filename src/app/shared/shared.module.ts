import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { DateAgoPipe } from './pipes/date-ago.pipe';

// NGXS
import { NgxsModule } from '@ngxs/store';
import { CountryState } from './store/country.state';

// NG-Select
import { NgSelectModule } from '@ng-select/ng-select';

// NG-Bootstrap
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';

// Font Awesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// NG-Inline-SVG
import { InlineSVGModule } from 'ng-inline-svg';

// NGX Infinite Scroll
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

export const MODULES = [
  CommonModule,
  RouterModule,
  ReactiveFormsModule,
  FontAwesomeModule,
  NgSelectModule,
  NgbModalModule,
  InlineSVGModule,
  InfiniteScrollModule,
  NgbCarouselModule
];

@NgModule({
  imports: [MODULES, NgxsModule.forFeature([CountryState])],
  exports: [MODULES, DateAgoPipe],
  declarations: [DateAgoPipe]
})
export class SharedModule {}
