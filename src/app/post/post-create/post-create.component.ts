import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Post } from '@app/core/models/post.model';

// NGXS
import { Store, Select } from '@ngxs/store';
import { AddPost } from '../../core/store/post.action';
import { Navigate } from '@ngxs/router-plugin';
import { PostState } from '../../core/store/post.state';

// Font Awesome
import { faArrowAltCircleUp, faTrash } from '@fortawesome/free-solid-svg-icons';

// SweetAlert
import swal from 'sweetalert2';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss']
})
export class PostCreateComponent {
  @ViewChild('caption', { static: false }) caption: ElementRef;

  form: FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  selectedFile: File;
  length: number;
  imageUrls = [];
  videoUrls = [];

  // Font Awesome
  faArrowAltCircleUp = faArrowAltCircleUp;
  faTrash = faTrash;

  // NGXS
  @Select(PostState.getFileUrl) url$: Observable<Post>;
  @Select(PostState.getFormat) format$: Observable<Post>;

  constructor(private formBuilder: FormBuilder, private store: Store) {
    this.form = this.formBuilder.group({
      caption: [null]
    });

    this.form.valueChanges.subscribe(() => {
      this.caption.nativeElement.style.height = 'auto';
      this.caption.nativeElement.style.height = `${this.caption.nativeElement.scrollHeight}px`;
    });
  }

  onSelectFile(event) {
    this.selectedFile = event.target.files as File;
    this.length = event.target.files.length as number;
    const files = event.target.files;
    if (files) {
      for (const file of files) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          if (file.type.indexOf('image') > -1) {
            this.imageUrls.push(e.target.result);
          } else if (file.type.indexOf('video') > -1) {
            this.videoUrls.push(e.target.result);
          }
        };
        reader.readAsDataURL(file);
      }
    }
  }

  // removeFile() {
  //   this.selectedFile = null;
  //   this.form.get('image').setValue(null);
  // }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onAddPost(form: FormGroup) {
    const formData = new FormData();
    if (form.value.caption) {
      formData.append('caption', form.value.caption);
    }
    for (let i = 0; i < this.length; i++) {
      formData.append('content[]', this.selectedFile[i]);
    }
    console.log(formData);
    this.formSubmitAttempt = true;
    if (!this.selectedFile) {
      swal.fire('Warning', 'Please Select at Least One Image/Video', 'warning');
    }
    if (form.valid && this.selectedFile) {
      this.setSubmitButton(false);
      this.formSubmitAttempt = false;
      this.store.dispatch(new AddPost(formData)).subscribe(
        () => {
          this.setSubmitButton(true);
          this.store.dispatch(new Navigate(['/home']));
          window.scroll(0, 0);
        },
        err => {
          console.log(err);
          this.setSubmitButton(true);
        }
      );
    }
  }
}
