import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { confirmDelete } from '@app/shared/reusable-functions/sweet-alerts';
import { Comment } from '@app/core/models/comment.model';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Story } from '@app/core/models/story.model';
import { StoryModalComponent } from '@app/core/modals/story-modal/story-modal.component';
import { Profile } from '@app/core/models/profile.model';

// NGXS
import { Store, Select } from '@ngxs/store';
import { PostState } from '@app/core/store/post.state';
import { Post } from '@app/core/models/post.model';
import { AuthState } from '@app/core/store/auth.state';
import {
  GetPost,
  DeletePost,
  LikePostInPost,
  DeleteCommentInPost,
  LikeCommentInPost,
  AddCommentInPost
} from '@app/core/store/post.action';
import { SelectStory } from '@app/core/store/story.action';
import { ProfileState } from '@app/core/store/profile.state';
import { GetProfile } from '@app/core/store/profile.action';

// SweetAlert2
import swal from 'sweetalert2';
import { Navigate } from '@ngxs/router-plugin';

// NG-Bootstrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent {
  id: number;
  form: FormGroup;
  profilePhoto: string;
  submitButtonStatus = true;
  // private scrollExecuted = false;
  highlightColor = {
    // 'background': '#fff9d7'
  };

  // NGXS
  @Select(PostState.getPost) post$: Observable<Post>;
  @Select(ProfileState.getProfile) profile$: Observable<Profile>;
  userId = this.store.selectSnapshot(AuthState.getUserId);

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private route: ActivatedRoute,
    private location: Location,
    private modalService: NgbModal,
    // private cdr: ChangeDetectorRef
  ) {
    this.getPost();
    this.store.dispatch(new GetProfile());
    this.form = this.fb.group({
      post_id: [null, Validators.required],
      comment: [null, Validators.required]
    });
    this.profilePhoto = this.store.selectSnapshot(AuthState.getProfilePhoto);

    // this.meta.updateTag({ property: 'og:title', content: 'HAHAHA' });
    // this.meta.updateTag({ property: 'og:description', content: 'DESC' });
    // this.meta.updateTag({
    //   property: 'og:image',
    //   content:
    //     'https://www.esa.int/var/esa/storage/images/esa_multimedia/videos/2018/05/mars_sample_return/17493376-1-eng-GB/Mars_sample_return_pillars.jpg'
    // });
    // this.meta.updateTag({
    //   property: 'og:fb:app_id',
    //   content: '495665254407383'
    // });
  }

  // ngAfterViewChecked() {
  //   this.cdr.detectChanges();
  //   if (!this.scrollExecuted) {
  //     let routeFragmentSubscription: Subscription;
  //     // Automatic scroll
  //     routeFragmentSubscription = this.route.fragment.subscribe(fragment => {
  //       if (fragment) {
  //         const element = document.getElementById(fragment);
  //         if (element) {
  //           element.scrollIntoView();
  //           this.scrollExecuted = true;
  //           // Free resources
  //           // this.className = '.red';
  //           setTimeout(() => {
  //             this.highlightColor = {
  //               'background': ''
  //             };
  //             // this.className = '';
  //             console.log('routeFragmentSubscription unsubscribe');
  //             routeFragmentSubscription.unsubscribe();
  //           }, 1000);
  //         }
  //       }
  //     });
  //   }
  // }

  convertHashTags(str: string) {
    const spanned = `<span>${str}</span>`;
    return spanned.replace(/#([\w]+)/g, `<a href="/explore/tags/$1">#$1</a>`);
  }

  goToHashTag(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    const href = (event.target as HTMLAnchorElement).href;
    const tag = href.substring(href.indexOf('/explore/tags/') + '/explore/tags/'.length);
    console.log(tag);
    this.store.dispatch(new Navigate(['/explore/tags', tag]));
  }

  getPost() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.store.dispatch(new GetPost(params.id)).subscribe(val => {
        console.log(val);
        this.form.get('post_id').setValue(params.id);
      });
    });
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  navigateBasedOnStoriesAndUserId(hasStories: Story[], userId: number) {
    hasStories.length > 0
      ? []
      : userId === this.userId
        ? this.store.dispatch(new Navigate(['/profile']))
        : this.store.dispatch(new Navigate(['/profile/' + userId]));
  }

  navigateBasedOnUserId(userId: number) {
    userId === this.userId
      ? this.store.dispatch(new Navigate(['/profile']))
      : this.store.dispatch(new Navigate(['/profile/' + userId]));
  }

  openStoryModal(stories: Story[]) {
    this.store.dispatch(new SelectStory(stories)).subscribe(() => {
      this.modalService.open(StoryModalComponent);
    });
  }

  withStory(stories: Story[]) {
    if (stories) {
      return true;
    } else {
      return false;
    }
  }

  onDeletePost(data: Post) {
    confirmDelete(`${data.caption}`).then(result => {
      console.log(result);
      if (result.value) {
        this.store.dispatch(new DeletePost(data.id)).subscribe(
          () => {
            swal.fire('Success', 'Successfully Deleted', 'success').then(() => {
              this.location.back();
            });
          },
          err => {
            console.log(err);
          }
        );
      }
    });
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  onAddComment(form: FormGroup) {
    console.log(form.value);
    this.setSubmitButton(false);
    this.store.dispatch(new AddCommentInPost(form.value)).subscribe(
      () => {
        form.reset();
        this.setSubmitButton(true);
      },
      err => {
        console.log(err);
      }
    );
  }

  onDeleteComment(data: Comment) {
    console.log(data);
    confirmDelete(`${data.comment}`).then(() => {
      this.store.dispatch(new DeleteCommentInPost(data.id));
    });
  }

  onLikeComment(data: Comment) {
    console.log(data);
    this.store.dispatch(new LikeCommentInPost(data)).subscribe();
  }

  onLikePost(data: Post) {
    this.store.dispatch(new LikePostInPost(data)).subscribe();
  }

  isLiked(isLiked: number) {
    if (isLiked === 0) {
      return false;
    } else if (isLiked === 1) {
      return true;
    }
  }
}
