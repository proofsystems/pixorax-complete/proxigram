import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [AppComponent],
  imports: [CoreModule, AppRoutingModule, BrowserModule.withServerTransition({ appId: 'serverApp' })],

  bootstrap: [AppComponent]
})
export class AppModule {}
