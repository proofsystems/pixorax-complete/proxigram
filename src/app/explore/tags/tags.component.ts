import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { PostState } from '@app/core/store/post.state';
import { Select, Store } from '@ngxs/store';
import { Post } from '@app/core/models/post.model';
import { GetPostsbyHashtagPerPage, GetPostsbyHashtagFirstPage } from '@app/core/store/post.action';
import { environment } from '@env/environment';
import { ActivatedRoute } from '@angular/router';
import { FollowHashtag } from '@app/core/store/post.action';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent {
  currentPage = 1;
  lastPage: number;
  id: number;

  // NGXS
  @Select(PostState.getLastPage) lastPage$: Observable<number>;
  @Select(PostState.getAllPosts) posts$: Observable<Post[]>;
  @Select(PostState.getHashtag) hashtag$: Observable<string>;
  @Select(PostState.getPostsCount) postsCount$: Observable<number>;
  @Select(PostState.getHashtagID) hashtag_id$: Observable<number>;
  @Select(PostState.getIsFollowing) is_following$: Observable<number>;

  constructor(private store: Store, private route: ActivatedRoute) {
    this.getPost();
  }

  getPost() {
    this.route.params.subscribe(params => {
      console.log(params);
      this.store.dispatch(new GetPostsbyHashtagFirstPage('1', params.string));
    });
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onScroll() {
    if (this.currentPage < this.lastPage) {
      this.currentPage += 1;
      this.store.dispatch(new GetPostsbyHashtagPerPage('', this.currentPage.toString()));
    }
  }

  onFollowHashtag(id: number) {
    this.store.dispatch(new FollowHashtag(id)).subscribe();
  }
}
