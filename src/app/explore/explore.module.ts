import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExploreComponent } from './explore.component';
import { TagsComponent } from './tags/tags.component';
import { SharedModule } from '@app/shared/shared.module';
import { ExploreRoutingModule } from './explore-routing.module';

// NGXS
import { NgxsModule } from '@ngxs/store';

@NgModule({
  declarations: [ExploreComponent, TagsComponent],
  imports: [CommonModule, SharedModule, ExploreRoutingModule]
})
export class ExploreModule {}
