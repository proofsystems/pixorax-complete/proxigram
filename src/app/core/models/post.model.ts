import { Comment } from './comment.model';
import { Profile } from './profile.model';

export interface Post {
  id: number;
  caption: string;
  comments: Comment[];
  contents: [];
  likes: [];
  user_id: number;
  type: string;
  user: Profile;
  created_at: string;
  likes_count: number;
  is_liked: number;
  hashtag: string;
  posts_count: number;
  is_following: number;
  hashtag_id: number;
}

export class PostStateModel {
  posts: Post[];
  post: Post;
  fileUrl?: string | ArrayBuffer;
  format?: string;
  searchResults: Post[];
  lastPage: number;
  hashtag: string;
  posts_count: number;
  is_following: number;
  hashtag_id: number;
}
