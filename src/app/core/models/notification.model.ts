import { Profile } from '@app/core/models/profile.model';

export interface Notification {
  id: number;
  notifiable_id: number;
  sender: {
    id: number;
    fname: string;
    lname: string;
    image: string;
    social_account: {};
  };
  notifiable: {
    id: number;
    post_id: number;
    contents: [];
  };
  message: string;
  created_at: string;
  user: Profile;
  action: string;
}

export class NotificationStateModel {
  recentNotifications: Notification[];
  notifications: Notification[];
  lastPage: number;
}
