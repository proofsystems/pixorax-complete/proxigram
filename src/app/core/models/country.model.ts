export interface Country {
  country_id: number;
  country_name: string;
}

export class CountryStateModel {
  countries: Country[];
}
