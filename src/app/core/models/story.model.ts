export interface Story {
  id: number;
  content: string;
  image_address: string;
  type: string;
}

export class StoryStateModel {
  stories: Story[];
  selectedStory: Story[];
}
