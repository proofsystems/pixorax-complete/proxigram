import { Country } from './country.model';
import { Story } from './story.model';

export interface Profile {
  id: number;
  country: Country;
  email: string;
  fname: string;
  lname: string;
  image: string;
  social_account: {};
  has_stories: Story[];
  followers_count: number;
  following_count: number;
  num_post: number;
  description: string;
  is_followed: number;
}

export class ProfileStateModel {
  profile: Profile;
  follows: Profile[];
  suggestedUsers: Profile[];
  searchResults: Profile[];
  lastPage: number;
}
