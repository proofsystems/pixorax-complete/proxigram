import { Profile } from '@app/core/models/profile.model';

export class Hashtag {
  id: number;
  post_id: number;
  comment: string;
  user_id: number;
  user: Profile;
  created_at: string;
  image: string;
  likes_count: number;
  is_liked: number;
}

export class HashtagStateModel {
  hashtags: Hashtag[];
}
