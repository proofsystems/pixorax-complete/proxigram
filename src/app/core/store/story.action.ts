import { Story } from '@app/core/models/story.model';

export class GetAllStories {
  static readonly type = '[Home Page] GetAllStories';
}

export class GetStory {
  static readonly type = '[Home Page] GetStory';
}

export class AddStory {
  static readonly type = '[Home Page] AddStory';
  constructor(public payload: Story) {}
}

export class SelectStory {
  static readonly type = '[Home Page] SelectStory';
  constructor(public payload: Story[]) {}
}
