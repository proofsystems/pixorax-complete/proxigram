import { PostsService } from '@app/core/services/posts.service';
import { Post, PostStateModel } from '@app/core/models/post.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { CommentsService } from '@app/core/services/comments.service';
import { Comment } from '@app/core/models/comment.model';
import { HashtagsService } from '@app/core/services/hashtags.service';

// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { patch, updateItem, removeItem, append } from '@ngxs/store/operators';
import {
  GetAllPosts,
  AddPost,
  GetPost,
  DeletePost,
  PreviewImageVideo,
  GetPostsPerPage,
  GetPostsFirstPage,
  GetOwnPosts,
  SearchPost,
  GetOwnPostsFirstPage,
  GetOwnPostsPerPage,
  GetOthersPostsFirstPage,
  GetOthersPostsPerPage,
  AddCommentInPosts,
  DeleteCommentInPosts,
  LikeCommentInPosts,
  AddCommentInPost,
  DeleteCommentInPost,
  LikeCommentInPost,
  LikePostInPosts,
  LikePostInPost,
  SearchHashtag,
  ClearSearchHashtag,
  GetPostsbyHashtagFirstPage,
  GetPostsbyHashtagPerPage,
  FollowHashtag
} from './post.action';

@State<PostStateModel>({
  name: 'post',
  defaults: {
    posts: [],
    post: null,
    fileUrl: null,
    format: null,
    searchResults: [],
    lastPage: null,
    hashtag: null,
    posts_count: null,
    is_following: null,
    hashtag_id: null
  }
})
export class PostState {
  @Selector()
  static getAllPosts(state: PostStateModel) {
    return state.posts;
  }

  @Selector()
  static getPost(state: PostStateModel) {
    return state.post;
  }

  @Selector()
  static getOwnPost(state: PostStateModel) {
    return state.posts;
  }

  @Selector()
  static getFileUrl(state: PostStateModel) {
    return state.fileUrl;
  }

  @Selector()
  static getFormat(state: PostStateModel) {
    return state.format;
  }

  @Selector()
  static getLastPage(state: PostStateModel) {
    return state.lastPage;
  }

  @Selector()
  static getSearchResults(state: PostStateModel) {
    return state.searchResults;
  }

  @Selector()
  static getHashtag(state: PostStateModel) {
    return state.hashtag;
  }

  @Selector()
  static getPostsCount(state: PostStateModel) {
    return state.posts_count;
  }

  @Selector()
  static getIsFollowing(state: PostStateModel) {
    return state.is_following;
  }

  @Selector()
  static getHashtagID(state: PostStateModel) {
    return state.hashtag_id;
  }

  constructor(
    private postsService: PostsService,
    private commentsService: CommentsService,
    private hashtagsService: HashtagsService
  ) {}

  @Action(GetAllPosts)
  getAllPosts({ patchState }: StateContext<PostStateModel>, {  }: GetAllPosts) {
    return this.postsService.getAll().pipe(
      tap((result: Post[]) => {
        console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetPostsFirstPage)
  getPostsFirstPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetPostsFirstPage) {
    return this.postsService.getPostsPerPage(key, pageNumber).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetPostsPerPage)
  getPostsPerPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetPostsPerPage) {
    const posts = ctx.getState().posts;
    console.log(posts);
    return this.postsService.getPostsPerPage(key, pageNumber).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: [...posts, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnPostsFirstPage)
  getOwnPostsFirstPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetOwnPostsFirstPage) {
    return this.postsService.getOwnPostsPerPage(key, pageNumber).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnPostsPerPage)
  getOwnPostsPerPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetOwnPostsPerPage) {
    const posts = ctx.getState().posts;
    console.log(posts);
    return this.postsService.getOwnPostsPerPage(key, pageNumber).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: [...posts, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOthersPostsFirstPage)
  getOthersPostsFirstPage(ctx: StateContext<PostStateModel>, { key, pageNumber, id }: GetOthersPostsFirstPage) {
    return this.postsService.getOthersPostsPerPage(key, pageNumber, id).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOthersPostsPerPage)
  getOthersPostsPerPage(ctx: StateContext<PostStateModel>, { key, pageNumber, id }: GetOthersPostsPerPage) {
    const posts = ctx.getState().posts;
    console.log(posts);
    return this.postsService.getOthersPostsPerPage(key, pageNumber, id).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: [...posts, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetPost)
  getPost(ctx: StateContext<PostStateModel>, { id }: GetPost) {
    return this.postsService.getPost(id).pipe(
      tap((result: Post) => {
        console.log(result);
        ctx.patchState({
          post: result
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnPosts)
  getOwnPosts({ patchState }: StateContext<PostStateModel>, {  }: GetOwnPosts) {
    return this.postsService.getOwn().pipe(
      tap((result: Post[]) => {
        console.log(result);
        patchState({
          posts: result
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  // @Action(SearchPost)
  // setFilter({ patchState }: StateContext<PostStateModel>, { payload }: SearchPost) {
  //   patchState({
  //     searchText: payload.val
  //   });
  // }

  @Action(SearchHashtag)
  searchHashtag({ patchState }: StateContext<PostStateModel>, { payload }: SearchHashtag) {
    return this.postsService.searchHashtag(payload).pipe(
      tap((result: Post[]) => {
        console.log(result);
        patchState({
          searchResults: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(ClearSearchHashtag)
  clearSearchHashtag({ patchState }: StateContext<PostStateModel>, {  }: ClearSearchHashtag) {
    patchState({
      searchResults: null
    });
  }

  @Action(GetPostsbyHashtagFirstPage)
  getPostsbyHashtagFirstPage(ctx: StateContext<PostStateModel>, { pageNumber, payload }: GetPostsbyHashtagFirstPage) {
    return this.postsService.viewPostsbyHashtag(pageNumber, payload).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: result['posts_paginated']['data'],
          lastPage: result['last_page'],
          hashtag: result['hashtag'],
          posts_count: result['posts_count'],
          is_following: result['is_following'],
          hashtag_id: result['id']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetPostsbyHashtagPerPage)
  getPostsbyHashtagPerPage(ctx: StateContext<PostStateModel>, { pageNumber, payload }: GetPostsbyHashtagPerPage) {
    const posts = ctx.getState().posts;
    console.log(posts);
    return this.postsService.viewPostsbyHashtag(pageNumber, payload).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          posts: [...posts, ...result['posts_paginated']['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(FollowHashtag)
  followHashtag(ctx: StateContext<PostStateModel>, { id }: FollowHashtag) {
    return this.hashtagsService.followHashtag(id).pipe(
      tap((result: Post[]) => {
        console.log(result);
        ctx.patchState({
          is_following: result['data']['is_following']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(AddPost)
  addPost(ctx: StateContext<PostStateModel>, { payload }: AddPost) {
    return this.postsService.addPost(payload).pipe(
      tap(() => {
        // return ctx.dispatch(new GetBranchesPerPage('', '1'));
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(DeletePost)
  deletePost({ getState, setState }: StateContext<PostStateModel>, { id }: DeletePost) {
    return this.postsService.deletePost(id).pipe(
      tap(() => {
        const state = getState();
        const filteredArray = state.posts.filter(item => item.id !== id);
        setState({
          ...state,
          posts: filteredArray
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(PreviewImageVideo)
  previewImageVideo(ctx: StateContext<PostStateModel>, { event, url }: PreviewImageVideo) {
    const file = (event.target as HTMLInputElement).files && (event.target as HTMLInputElement).files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      if (file.type.indexOf('image') > -1) {
        ctx.patchState({
          format: 'image'
        });
      } else if (file.type.indexOf('video') > -1) {
        ctx.patchState({
          format: 'video'
        });
      }
      reader.onload = event2 => {
        url = (event2.target as FileReader).result;
        ctx.patchState({
          fileUrl: url
        });
      };
    }
  }

  @Action(LikePostInPosts)
  likePostInPosts({ setState }: StateContext<PostStateModel>, { payload }: LikePostInPosts) {
    return this.postsService.likePost(payload).pipe(
      tap((result: any) => {
        if (result.message === 'Unlike') {
          payload.likes_count = payload.likes_count - 1;
          payload.is_liked = 0;
        } else {
          payload.likes_count = payload.likes_count + 1;
          payload.is_liked = 1;
        }
        setState(
          patch<PostStateModel>({
            posts: updateItem<Post>(item => item.id === payload.id, payload)
          })
        );
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(LikePostInPost)
  likePostInPost(ctx: StateContext<PostStateModel>, { payload }: LikePostInPost) {
    return this.postsService.likePost(payload).pipe(
      tap((result: any) => {
        console.log(result);
        if (result.message === 'Unlike') {
          payload.likes_count = payload.likes_count - 1;
          payload.is_liked = 0;
        } else {
          payload.likes_count = payload.likes_count + 1;
          payload.is_liked = 1;
        }
        console.log(payload);
        ctx.patchState({
          post: payload
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(AddCommentInPosts)
  addCommentInPosts({ setState }: StateContext<PostStateModel>, { payload }: AddCommentInPosts) {
    return this.commentsService.addComment(payload).pipe(
      tap((result: {}) => {
        console.log(result);
        setState(
          patch<PostStateModel>({
            posts: updateItem<Post>(
              p => p.id === payload.post_id,
              patch<Post>({
                comments: append<Comment>([result['data']])
              })
            )
          })
        );
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(DeleteCommentInPosts)
  deleteCommentInPosts({ setState }: StateContext<PostStateModel>, { postId, commentId }: DeleteCommentInPosts) {
    return this.commentsService.deleteComment(commentId).pipe(
      tap(() => {
        setState(
          patch<PostStateModel>({
            posts: updateItem<Post>(
              p => p.id === postId,
              patch<Post>({
                comments: removeItem<Comment>(c => c.id === commentId)
              })
            )
          })
        );
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(LikeCommentInPosts)
  likeCommentInPosts({ setState }: StateContext<PostStateModel>, { payload }: LikeCommentInPosts) {
    return this.commentsService.likeComment(payload).pipe(
      tap((result: any) => {
        if (result.message === 'Unlike') {
          payload.likes_count = payload.likes_count - 1;
          payload.is_liked = 0;
        } else {
          payload.likes_count = payload.likes_count + 1;
          payload.is_liked = 1;
        }
        setState(
          patch<PostStateModel>({
            posts: updateItem<Post>(
              p => p.id === payload.post_id,
              patch<Post>({
                comments: updateItem<Comment>(item => item.id === payload.id, payload)
              })
            )
          })
        );
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(AddCommentInPost)
  addCommentInPost(ctx: StateContext<PostStateModel>, { payload }: AddCommentInPost) {
    return this.commentsService.addComment(payload).pipe(
      tap((result: any) => {
        console.log(result);
        const state = ctx.getState();
        ctx.patchState({
          post: {
            ...state.post,
            comments: [...state.post.comments, result.data]
          }
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(DeleteCommentInPost)
  deleteCommentInPost(ctx: StateContext<PostStateModel>, { commentId }: DeleteCommentInPost) {
    return this.commentsService.deleteComment(commentId).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.patchState({
          post: {
            ...state.post,
            comments: state.post.comments.filter(comment => comment.id !== commentId)
          }
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(LikeCommentInPost)
  likeComment(ctx: StateContext<PostStateModel>, { payload }: LikeCommentInPost) {
    return this.commentsService.likeComment(payload).pipe(
      tap((result: any) => {
        if (result.message === 'Unlike') {
          payload.likes_count = payload.likes_count - 1;
          payload.is_liked = 0;
        } else {
          payload.likes_count = payload.likes_count + 1;
          payload.is_liked = 1;
        }
        ctx.setState(
          patch({
            post: patch({
              comments: updateItem<Comment>(comment => comment.id === payload.id, patch(payload))
            })
          })
        );
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }
}
