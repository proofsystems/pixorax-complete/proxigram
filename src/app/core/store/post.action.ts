import { Post } from '@app/core/models/post.model';
import { Comment } from '@app/core/models/comment.model';

export class GetAllPosts {
  static readonly type = '[Home Page] GetAllPosts';
}

export class GetPostsFirstPage {
  static readonly type = '[Home Page] GetPostsFirstPage';

  constructor(public key: string, public pageNumber: string) {}
}

export class GetPostsPerPage {
  static readonly type = '[Home Page] GetPostsPerPage';

  constructor(public key: string, public pageNumber: string) {}
}

export class GetOwnPostsFirstPage {
  static readonly type = '[Profile Page] GetOwnPostsFirstPage';

  constructor(public key: string, public pageNumber: string) {}
}

export class GetOwnPostsPerPage {
  static readonly type = '[Profile Page] GetOwnPostsPerPage';

  constructor(public key: string, public pageNumber: string) {}
}

export class GetOthersPostsFirstPage {
  static readonly type = '[Profile Page] GetOthersPostsFirstPage';

  constructor(public key: string, public pageNumber: string, public id: number) {}
}

export class GetOthersPostsPerPage {
  static readonly type = '[Profile Page] GetOthersPostsPerPage';

  constructor(public key: string, public pageNumber: string, public id: number) {}
}

export class GetPost {
  static readonly type = '[Home Page Modal] GetPost';

  constructor(public id: number) {}
}

export class GetOwnPosts {
  static readonly type = '[Profile Page] GetOwnPosts';
}

export class SearchPost {
  static readonly type = '[Navbar] SearchPost';
  constructor(public payload: { val: string }) {}
}

export class SearchHashtag {
  static readonly type = '[Search Bar] SearchHashtag';
  constructor(public payload: string) {}
}

export class GetPostsbyHashtagFirstPage {
  static readonly type = '[Explore Page] GetPostsbyHashtagFirstPage';
  constructor(public pageNumber: string, public payload: string) {}
}

export class GetPostsbyHashtagPerPage {
  static readonly type = '[Explore Page] GetPostsbyHashtagPerPage';
  constructor(public pageNumber: string, public payload: string) {}
}

export class ClearSearchHashtag {
  static readonly type = '[Search Bar] ClearSearchHashtag';
}

export class FollowHashtag {
  static readonly type = '[Explore Page] FollowHashtag';
  constructor(public id: number) {}
}

export class AddPost {
  static readonly type = '[Post Create Page] AddPost';
  constructor(public payload: FormData) {}
}

export class DeletePost {
  static readonly type = '[Home Page] DeletePost';
  constructor(public id: number) {}
}

export class EditPost {
  static readonly type = '[Home Page] EditPost';
  constructor(public payload: Post) {}
}

export class LikePostInPosts {
  static readonly type = '[Home Page] LikePostInPosts';
  constructor(public payload: Post) {}
}

export class LikePostInPost {
  static readonly type = '[Post Detail] LikePostInPost';
  constructor(public payload: Post) {}
}

export class PreviewImageVideo {
  static readonly type = '[Post Create Page] PreviewImageVideo';
  constructor(public event: Event, public url: string | ArrayBuffer) {}
}

// COMMENTS
export class AddCommentInPosts {
  static readonly type = '[Home Page] AddCommentInPosts';
  constructor(public payload: Comment) {}
}

export class DeleteCommentInPosts {
  static readonly type = '[Home Page] DeleteCommentInPosts';
  constructor(public postId: number, public commentId: number) {}
}

export class LikeCommentInPosts {
  static readonly type = '[Home Page] LikeCommentInPosts';
  constructor(public payload: Comment) {}
}

export class AddCommentInPost {
  static readonly type = '[Post Detail] AddCommentInPost';
  constructor(public payload: Comment) {}
}

export class DeleteCommentInPost {
  static readonly type = '[Home Page] DeleteCommentInPost';
  constructor(public commentId: number) {}
}

export class LikeCommentInPost {
  static readonly type = '[Home Page] LikeCommentInPost';
  constructor(public payload: Comment) {}
}
