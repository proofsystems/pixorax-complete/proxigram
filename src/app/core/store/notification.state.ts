import { NotificationsService } from '@app/core/services/notifications.service';
import { Notification, NotificationStateModel } from '@app/core/models/notification.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

// NGXS
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { GetRecent10, GetAllNotifications, GetNotificationsPerPage, GetNotificationsFirstPage } from './notification.action';

@State<NotificationStateModel>({
  name: 'notification',
  defaults: {
    recentNotifications: [],
    notifications: [],
    lastPage: null
  }
})
export class NotificationState {
  @Selector()
  static GetRecent10(state: NotificationStateModel) {
    return state.recentNotifications;
  }

  @Selector()
  static GetAllNotifications(state: NotificationStateModel) {
    return state.notifications;
  }

  @Selector()
  static getLastPage(state: NotificationStateModel) {
    return state.lastPage;
  }

  constructor(private notificationsService: NotificationsService) { }

  @Action(GetRecent10)
  getRecent10({ patchState }: StateContext<NotificationStateModel>, { }: GetRecent10) {
    return this.notificationsService.getRecent10().pipe(
      tap((result: Notification[]) => {
        console.log(result);
        patchState({
          recentNotifications: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllNotifications)
  getAllNotifications({ patchState }: StateContext<NotificationStateModel>, { }: GetAllNotifications) {
    return this.notificationsService.getAll().pipe(
      tap((result: Notification[]) => {
        console.log(result);
        patchState({
          notifications: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetNotificationsFirstPage)
  getNotificationsFirstPage(ctx: StateContext<NotificationStateModel>, { key, pageNumber }: GetNotificationsFirstPage) {
    return this.notificationsService.getNotificationsPerPage(key, pageNumber).pipe(
      tap((result: Notification[]) => {
        console.log(result);
        ctx.patchState({
          notifications: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetNotificationsPerPage)
  getNotificationsPerPage(ctx: StateContext<NotificationStateModel>, { key, pageNumber }: GetNotificationsPerPage) {
    const notifications = ctx.getState().notifications;
    console.log(notifications);
    return this.notificationsService.getNotificationsPerPage(key, pageNumber).pipe(
      tap((result: Notification[]) => {
        console.log(result);
        ctx.patchState({
          notifications: [...notifications, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

}
