import { AuthService } from '@app/core/services/auth.service';
import { Profile, ProfileStateModel } from '@app/core/models/profile.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UsersService } from '@app/core/services/users.service';

// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import {
  GetProfile,
  EditProfile,
  GetAllFollowers,
  GetAllFollowings,
  GetOtherProfile,
  FollowUser,
  GetSuggestedUsers,
  SearchUser,
  GetAllFollowingsofOtherUsers,
  GetAllFollowersofOtherUsers,
  ClearSearch,
  GetOwnFollowersFirstPage,
  GetOwnFollowersPerPage,
  GetOwnFollowingsFirstPage,
  GetOwnFollowingsPerPage,
  GetOtherFollowingsFirstPage,
  GetOtherFollowingsPerPage,
  GetOtherFollowersFirstPage,
  GetOtherFollowersPerPage
} from './profile.action';
import { ReplaceProfilePhoto } from '@app/core/store/auth.action';
import { patch, updateItem } from '@ngxs/store/operators';

@State<ProfileStateModel>({
  name: 'profile',
  defaults: {
    profile: null,
    follows: [],
    suggestedUsers: [],
    searchResults: [],
    lastPage: null
  }
})
export class ProfileState {
  @Selector()
  static getProfile(state: ProfileStateModel) {
    return state.profile;
  }

  @Selector()
  static getFollow(state: ProfileStateModel) {
    return state.follows;
  }

  @Selector()
  static getSuggestedUsers(state: ProfileStateModel) {
    return state.suggestedUsers;
  }

  @Selector()
  static getSearchResults(state: ProfileStateModel) {
    return state.searchResults;
  }

  @Selector()
  static getLastPage(state: ProfileStateModel) {
    return state.lastPage;
  }

  constructor(private authService: AuthService, private usersService: UsersService, private store: Store) {}

  @Action(GetProfile)
  getProfile({ patchState }: StateContext<ProfileStateModel>, {  }: GetProfile) {
    return this.authService.getProfile().pipe(
      tap((result: Profile) => {
        this.store.dispatch(new ReplaceProfilePhoto(result.image));
        console.log(result);
        patchState({
          profile: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(EditProfile)
  editProfile({ patchState }: StateContext<ProfileStateModel>, { payload }: EditProfile) {
    return this.usersService.updateUser(payload).pipe(
      tap((result: Profile) => {
        console.log(result);
        patchState({
          profile: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetOtherProfile)
  getOtherProfile({ patchState }: StateContext<ProfileStateModel>, { id }: GetOtherProfile) {
    return this.usersService.getUser(id).pipe(
      tap((result: Profile) => {
        console.log(result);
        patchState({
          profile: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetSuggestedUsers)
  getSuggestedUsers({ patchState }: StateContext<ProfileStateModel>, {  }: GetSuggestedUsers) {
    return this.usersService.getSuggestedUsers().pipe(
      tap((result: Profile[]) => {
        console.log(result);
        patchState({
          suggestedUsers: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllFollowers)
  getAllFollowers({ patchState }: StateContext<ProfileStateModel>, {  }: GetAllFollowers) {
    return this.usersService.getAllFollowers().pipe(
      tap((result: Profile[]) => {
        console.log(result);
        patchState({
          follows: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllFollowings)
  getAllFollowings({ patchState }: StateContext<ProfileStateModel>, {  }: GetAllFollowings) {
    return this.usersService.getAllFollowings().pipe(
      tap((result: Profile[]) => {
        console.log(result);
        patchState({
          follows: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllFollowersofOtherUsers)
  getAllFollowersofOtherUsers({ patchState }: StateContext<ProfileStateModel>, { id }: GetAllFollowersofOtherUsers) {
    return this.usersService.getAllFollowersOfOtherUsers(id).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        patchState({
          follows: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetAllFollowingsofOtherUsers)
  getAllFollowingsofOtherUsers({ patchState }: StateContext<ProfileStateModel>, { id }: GetAllFollowingsofOtherUsers) {
    return this.usersService.getAllFollowingsofOtherUsers(id).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        patchState({
          follows: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetOwnFollowersFirstPage)
  getOwnFollowersFirstPage(ctx: StateContext<ProfileStateModel>, { key, pageNumber }: GetOwnFollowersFirstPage) {
    return this.usersService.getOwnFollowersPerPage(key, pageNumber).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnFollowersPerPage)
  getOwnFollowersPerPage(ctx: StateContext<ProfileStateModel>, { key, pageNumber }: GetOwnFollowersPerPage) {
    const follows = ctx.getState().follows;
    return this.usersService.getOwnFollowersPerPage(key, pageNumber).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: [...follows, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnFollowingsFirstPage)
  getOwnFollowingsFirstPage(ctx: StateContext<ProfileStateModel>, { key, pageNumber }: GetOwnFollowingsFirstPage) {
    return this.usersService.getOwnFollowingsPerPage(key, pageNumber).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOwnFollowingsPerPage)
  getOwnFollowingsPerPage(ctx: StateContext<ProfileStateModel>, { key, pageNumber }: GetOwnFollowingsPerPage) {
    const follows = ctx.getState().follows;
    console.log(follows);
    return this.usersService.getOwnFollowingsPerPage(key, pageNumber).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: [...follows, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOtherFollowingsFirstPage)
  getOtherFollowingsFirstPage(
    ctx: StateContext<ProfileStateModel>,
    { key, pageNumber, id }: GetOtherFollowingsFirstPage
  ) {
    return this.usersService.getOtherFollowingsPerPage(key, pageNumber, id).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOtherFollowingsPerPage)
  getOtherFollowingsPerPage(ctx: StateContext<ProfileStateModel>, { key, pageNumber, id }: GetOtherFollowingsPerPage) {
    const follows = ctx.getState().follows;
    console.log(follows);
    return this.usersService.getOtherFollowingsPerPage(key, pageNumber, id).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: [...follows, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOtherFollowersFirstPage)
  getOtherFollowersFirstPage(
    ctx: StateContext<ProfileStateModel>,
    { key, pageNumber, id }: GetOtherFollowersFirstPage
  ) {
    return this.usersService.getOtherFollowersPerPage(key, pageNumber, id).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: result['data'],
          lastPage: result['last_page']
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(GetOtherFollowersPerPage)
  getOtherFollowersPerPage(ctx: StateContext<ProfileStateModel>, { key, pageNumber, id }: GetOtherFollowersPerPage) {
    const follows = ctx.getState().follows;
    console.log(follows);
    return this.usersService.getOtherFollowersPerPage(key, pageNumber, id).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        ctx.patchState({
          follows: [...follows, ...result['data']]
        });
      }),
      catchError(err => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  @Action(FollowUser)
  followUser({ setState }: StateContext<ProfileStateModel>, { payload }: FollowUser) {
    return this.usersService.followUser(payload).pipe(
      tap((result: any) => {
        if (result.message === 'Unfollowed!') {
          payload.is_followed = 0;
        } else {
          payload.is_followed = 1;
        }
        setState(
          patch<ProfileStateModel>({
            follows: updateItem<Profile>(item => item.id === payload.id, payload)
          })
        );
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(SearchUser)
  searchUser({ patchState }: StateContext<ProfileStateModel>, { payload }: SearchUser) {
    return this.usersService.searchUser(payload).pipe(
      tap((result: Profile[]) => {
        console.log(result);
        patchState({
          searchResults: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(ClearSearch)
  clearSearch({ patchState }: StateContext<ProfileStateModel>, {  }: ClearSearch) {
    patchState({
      searchResults: null
    });
  }
}
