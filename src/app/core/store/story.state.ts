import { StoryStateModel, Story } from '../models/story.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { StoriesService } from './../services/stories.service';

// NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { GetStory, AddStory, GetAllStories, SelectStory } from './story.action';

// NG-Bootstrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@State<StoryStateModel>({
  name: 'story',
  defaults: {
    stories: [],
    selectedStory: null
  }
})
export class StoryState {
  @Selector()
  static getStories(state: StoryStateModel) {
    return state.stories;
  }

  @Selector()
  static getSelectedStory(state: StoryStateModel) {
    return state.selectedStory;
  }

  constructor(private store: Store, private storiesService: StoriesService, private modalService: NgbModal) {}

  @Action(GetAllStories)
  getAllStories({ patchState }: StateContext<StoryStateModel>, {  }: GetAllStories) {
    return this.storiesService.getAll().pipe(
      tap((result: Story[]) => {
        console.log(result);
        patchState({
          stories: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(GetStory)
  getStory({ patchState }: StateContext<StoryStateModel>, {  }: GetStory) {
    return this.storiesService.getStory().pipe(
      tap((result: Story[]) => {
        // this.store.dispatch(new ReplaceProfilePhoto(result.image));
        console.log(result);
        patchState({
          stories: result
        });
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  @Action(SelectStory)
  selectStory({ patchState }: StateContext<StoryStateModel>, { payload }: SelectStory) {
    patchState({
      selectedStory: payload
    });
  }
}
