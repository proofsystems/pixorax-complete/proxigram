import { Auth, SocialAuth } from '@app/core/models/auth.model';

export class Login {
  static readonly type = '[Login Page] LoginUser';
  constructor(public payload: Auth) { }
}

export class Logout {
  static readonly type = '[Navbar] LogoutUser';
}

export class LoginGoogle {
  static readonly type = '[Login Page] LoginWithGoogle';
  constructor(public payload: SocialAuth) { }
}

export class LoginFacebook {
  static readonly type = '[Login Page] LoginWithFacebook';
  constructor(public payload: SocialAuth) { }
}

export class SignUp {
  static readonly type = '[Sign Up Page] SignUpUser';
  constructor(public payload: Auth) { }
}

export class RequestResetPassword {
  static readonly type = '[Forgot Password Page] RequestResetPassword';
  constructor(public payload: Auth) { }
}

export class ResetPassword {
  static readonly type = '[Reset Password Page] ResetPassword';
  constructor(public payload: Auth) { }
}

export class ReplaceProfilePhoto {
  static readonly type = '[Edit Profile Page] ReplaceProfilePhoto';
  constructor(public payload: string) { }
}
