export class GetRecent10 {
  static readonly type = '[Navbar] GetRecent10';
}

export class GetAllNotifications {
  static readonly type = '[Notificatons Page] GetAllNotifications';
}

export class GetNotificationsFirstPage {
  static readonly type = '[Notification Page] GetNotificationsFirstPage';

  constructor(public key: string, public pageNumber: string) { }
}

export class GetNotificationsPerPage {
  static readonly type = '[Notification Page] GetNotificationsPerPage';

  constructor(public key: string, public pageNumber: string) { }
}
