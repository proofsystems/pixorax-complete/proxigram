import { Profile } from '@app/core/models/profile.model';

export class GetProfile {
  static readonly type = '[Profile Page] GetProfile';
}

export class EditProfile {
  static readonly type = '[Edit Profile Page] EditProfile';

  constructor(public payload: FormData) { }
}

export class GetOtherProfile {
  static readonly type = '[Profile Page] GetOtherProfile';

  constructor(public id: number) { }
}

export class GetSuggestedUsers {
  static readonly type = '[Home Page] GetSuggestedUsers';
}

export class GetAllFollowers {
  static readonly type = '[Follow Page] GetAllFollowers';
}

export class GetAllFollowings {
  static readonly type = '[Follow Page] GetAllFollowings';
}

export class GetAllFollowersofOtherUsers {
  static readonly type = '[Follow Page] GetAllFollowersofOtherUsers';

  constructor(public id: number) { }
}

export class GetAllFollowingsofOtherUsers {
  static readonly type = '[Follow Page] GetAllFollowingsofOtherUsers';

  constructor(public id: number) { }
}


export class GetOwnFollowersFirstPage {
  static readonly type = '[Follow Page] GetOwnFollowersFirstPage';

  constructor(public key: string, public pageNumber: string) { }
}

export class GetOwnFollowersPerPage {
  static readonly type = '[Follow Page] GetOwnFollowersPerPage';

  constructor(public key: string, public pageNumber: string) { }
}

export class GetOwnFollowingsFirstPage {
  static readonly type = '[Follow Page] GetOwnFollowingsFirstPage';

  constructor(public key: string, public pageNumber: string) { }
}

export class GetOwnFollowingsPerPage {
  static readonly type = '[Follow Page] GetOwnFollowingsPerPage';

  constructor(public key: string, public pageNumber: string) { }
}

export class GetOtherFollowersFirstPage {
  static readonly type = '[Follow Page] GetOtherFollowersFirstPage';

  constructor(public key: string, public pageNumber: string, public id: number) { }
}

export class GetOtherFollowersPerPage {
  static readonly type = '[Follow Page] GetOtherFollowersPerPage';

  constructor(public key: string, public pageNumber: string, public id: number) { }
}

export class GetOtherFollowingsFirstPage {
  static readonly type = '[Follow Page] GetOtherFollowingsFirstPage';

  constructor(public key: string, public pageNumber: string, public id: number) { }
}

export class GetOtherFollowingsPerPage {
  static readonly type = '[Follow Page] GetOtherFollowingsPerPage';

  constructor(public key: string, public pageNumber: string, public id: number) { }
}

export class FollowUser {
  static readonly type = '[Other Users Profile Page] FollowUser';

  constructor(public payload: Profile) { }
}

export class SearchUser {
  static readonly type = '[Search Bar] SearchUser';

  constructor(public payload: {}) { }
}

export class ClearSearch {
  static readonly type = '[Search Bar] ClearSearch';
}

