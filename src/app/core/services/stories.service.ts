import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Story } from './../models/story.model';

@Injectable({
  providedIn: 'root'
})
export class StoriesService {
  url = `${environment.url}/stories`;

  constructor(private httpClient: HttpClient) {}

  getAll() {
    return this.httpClient.get<Story[]>(`${this.url}/all`);
  }

  getStory() {
    return this.httpClient.get<Story[]>(`${this.url}/per-user`);
  }

  addStory(payload: Story) {
    return this.httpClient.post(`${this.url}`, payload);
  }
}
