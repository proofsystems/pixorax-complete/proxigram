import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class HashtagsService {
  url = `${environment.url}/hashtags`;

  constructor(private httpClient: HttpClient) {}

  followHashtag(hashtag_id: number) {
    return this.httpClient.post(`${this.url}`, JSON.stringify({ hashtag_id: hashtag_id }));
  }
}
