import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Post } from '@app/core/models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  url = `${environment.url}/posts`;

  constructor(private httpClient: HttpClient) {}

  getAll() {
    return this.httpClient.get<Post[]>(`${this.url}/all`);
  }

  getPostsPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Post[]>(`${this.url}/paginated/feeds?key=${key}&page=${pageNumber}`);
  }

  getOwnPostsPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Post[]>(`${this.url}/paginated/own?key=${key}&page=${pageNumber}`);
  }

  getOthersPostsPerPage(key: string, pageNumber: string, id: number) {
    return this.httpClient.get<Post[]>(`${this.url}/paginated/others/${id}?key=${key}&page=${pageNumber}`);
  }

  getPost(id: number) {
    return this.httpClient.get(this.getPostUrl(id));
  }

  private getPostUrl(id: number) {
    return `${this.url}/${id}`;
  }

  getOwn() {
    return this.httpClient.get<Post[]>(`${this.url}/own`);
  }

  addPost(payload: FormData) {
    return this.httpClient.post(`${this.url}`, payload);
  }

  deletePost(id: number) {
    return this.httpClient.delete(`${this.url}/${id}`);
  }

  likePost(payload: Post) {
    return this.httpClient.get(`${this.url}/like/${payload.id}`);
  }

  searchHashtag(payload: string) {
    return this.httpClient.get(`${this.url}/hashtags/search?key=${payload.substr(1)}`);
  }

  viewPostsbyHashtag(pageNumber: string, payload: string) {
    return this.httpClient.get(`${this.url}/hashtags/view/${payload}/paginated?page=${pageNumber}`);
  }
}
