import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Notification } from '@app/core/models/notification.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  url = `${environment.url}/users`;

  constructor(private httpClient: HttpClient) { }

  getRecent10() {
    return this.httpClient.get<Notification[]>(`${this.url}/notifications`);
  }

  getAll() {
    return this.httpClient.get<Notification[]>(`${this.url}/all_notifications`);
  }

  getNotificationsPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Notification[]>(`${this.url}/paginated/notifications?key=${key}&page=${pageNumber}`);
  }
}
