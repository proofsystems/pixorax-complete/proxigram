import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Auth, SocialAuth } from '../models/auth.model';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = `${environment.url}/auth`;

  constructor(private httpClient: HttpClient) {}

  login(payload: Auth) {
    return this.httpClient.post(`${this.url}/login`, JSON.stringify(payload));
  }

  logout(): Observable<null> {
    return of(null);
  }

  signUp(payload: Auth) {
    return this.httpClient.post(`${this.url}/register`, JSON.stringify(payload));
  }

  getProfile() {
    return this.httpClient.get(`${this.url}/me`);
  }

  loginWithGoogle(payload: SocialAuth) {
    return this.httpClient.post(`${this.url}/socialLogin`, JSON.stringify(payload));
  }

  loginWithFacebook(payload: SocialAuth) {
    return this.httpClient.post(`${this.url}/socialLogin`, JSON.stringify(payload));
  }

  editProfile(payload: Auth) {
    return this.httpClient.put(`${this.url}/me`, JSON.stringify(payload));
  }

  requestForResetPassword(payload: Auth) {
    return this.httpClient.post(`${this.url}/password/email`, JSON.stringify(payload));
  }

  resetPassword(payload: Auth) {
    return this.httpClient.post(`${this.url}/password/reset`, JSON.stringify(payload));
  }
}
