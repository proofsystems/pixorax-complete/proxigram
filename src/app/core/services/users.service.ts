import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { Profile } from '@app/core/models/profile.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  url = `${environment.url}/users`;
  followUrl = `${environment.url}/followers`;

  constructor(private httpClient: HttpClient) { }

  getUser(id: number) {
    return this.httpClient.get(this.getUserUrl(id));
  }

  private getUserUrl(id: number) {
    return `${this.url}/${id}`;
  }

  updateUser(payload: FormData) {
    return this.httpClient.post(`${this.url}/edit`, payload);
  }

  getAllFollowers() {
    return this.httpClient.get<Profile[]>(`${this.url}/followers`);
  }

  getAllFollowings() {
    return this.httpClient.get<Profile[]>(`${this.url}/following`);
  }

  getAllFollowersOfOtherUsers(id: number) {
    return this.httpClient.get<Profile[]>(`${this.url}/${id}/followers`);
  }

  getAllFollowingsofOtherUsers(id: number) {
    return this.httpClient.get<Profile[]>(`${this.url}/${id}/following`);
  }

  getOwnFollowersPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Profile[]>(`${this.url}/paginated/followers?key=${key}&page=${pageNumber}`);
  }

  getOwnFollowingsPerPage(key: string, pageNumber: string) {
    return this.httpClient.get<Profile[]>(`${this.url}/paginated/following?key=${key}&page=${pageNumber}`);
  }

  getOtherFollowersPerPage(key: string, pageNumber: string, id: number) {
    return this.httpClient.get<Profile[]>(`${this.url}/paginated/${id}/followers?key=${key}&page=${pageNumber}`);
  }

  getOtherFollowingsPerPage(key: string, pageNumber: string, id: number) {
    return this.httpClient.get<Profile[]>(`${this.url}/paginated/${id}/following?key=${key}&page=${pageNumber}`);
  }

  followUser(payload: Profile) {
    return this.httpClient.post(`${this.followUrl}/follow/${payload.id}`, payload.id);
  }

  getSuggestedUsers() {
    return this.httpClient.get<Profile[]>(`${this.url}/suggestions`);
  }

  searchUser(payload: {}) {
    return this.httpClient.post<Profile[]>(`${this.url}/search/`, payload);
  }
}
