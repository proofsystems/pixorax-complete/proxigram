import { Component, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { Story } from '@app/core/models/story.model';

// NGXS
import { Select } from '@ngxs/store';
import { StoryState } from '@app/core/store/story.state';

@Component({
  selector: 'app-story-modal',
  templateUrl: './story-modal.component.html',
  styleUrls: ['./story-modal.component.scss']
})
export class StoryModalComponent implements AfterViewChecked {
  // NGXS
  @Select(StoryState.getSelectedStory) selectedStory$: Observable<Story>;

  constructor(private cdr: ChangeDetectorRef) {}

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }
}
