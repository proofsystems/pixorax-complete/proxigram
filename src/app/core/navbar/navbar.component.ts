import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Notification } from '@app/core/models/notification.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Profile } from '@app/core/models/profile.model';
import { environment } from '@env/environment';
import { Story } from '@app/core/models/story.model';
import { StoryModalComponent } from '@app/core/modals/story-modal/story-modal.component';
import { Post } from '@app/core/models/post.model';

// NGXS
import { Logout } from '@app/core/store/auth.action';
import { Select, Store } from '@ngxs/store';
import { GetRecent10 } from '@app/core/store/notification.action';
import { NotificationState } from '@app/core/store/notification.state';
import { debounceTime } from 'rxjs/operators';
import { AuthState } from '@app/core/store/auth.state';
import { SearchUser } from '../store/profile.action';
import { ProfileState } from '@app/core/store/profile.state';
import { Navigate } from '@ngxs/router-plugin';
import { ClearSearch } from './../store/profile.action';
import { SelectStory } from '@app/core/store/story.action';
import { SearchHashtag, ClearSearchHashtag } from './../store/post.action';
import { PostState } from '@app/core/store/post.state';

// NG-Bootstrap
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  dropdownState = false;
  dropdownStateNotif = false;
  searchForm: FormGroup;
  profilePhoto: string;

  // NGXS
  @Select(NotificationState.GetRecent10) recentNotifications$: Observable<Notification[]>;
  @Select(AuthState.getProfilePhoto) profilePhoto$: Observable<string>;
  @Select(ProfileState.getSearchResults) searchResults$: Observable<Profile[]>;
  @Select(PostState.getSearchResults) searchResultTags$: Observable<Post[]>;
  userId = this.store.selectSnapshot(AuthState.getUserId);

  constructor(private store: Store, private fb: FormBuilder, private modalService: NgbModal) {
    this.searchForm = this.fb.group({
      name: [null, [Validators.required]]
    });
    this.profilePhoto$.subscribe(val => {
      this.profilePhoto = val;
    });

    this.searchForm
      .get('name')
      .valueChanges.pipe(debounceTime(500))
      .subscribe((name: string) => {
        console.log(name);
        if (name) {
          if (name.startsWith('#')) {
            console.log('####');
            this.store.dispatch(new SearchHashtag(name));
            this.store.dispatch(new ClearSearch());
          } else {
            console.log('normal');
            this.store.dispatch(new SearchUser({ keyword: name }));
            this.store.dispatch(new ClearSearchHashtag());
          }
        } else if (!name) {
          this.store.dispatch(new ClearSearch());
          this.store.dispatch(new ClearSearchHashtag());
        }
      });
  }

  clearSearch() {
    this.searchForm.get('name').setValue(null);
    this.store.dispatch(new ClearSearch());
  }

  withStory(stories: Story[]) {
    if (stories) {
      return true;
    } else {
      return false;
    }
  }

  openStoryModal(stories: Story[]) {
    this.store.dispatch(new SelectStory(stories)).subscribe(() => {
      this.clearSearch();
      this.modalService.open(StoryModalComponent);
    });
  }

  navigateBasedOnStoriesAndUserId(hasStories: Story[], userId: number) {
    hasStories.length > 0
      ? []
      : userId === this.userId
      ? this.store.dispatch(new Navigate(['/profile'])).subscribe(() => this.clearSearch())
      : this.store.dispatch(new Navigate(['/profile/' + userId])).subscribe(() => this.clearSearch());
  }

  navigateBasedOnUserId(userId: number) {
    userId === this.userId
      ? this.store.dispatch(new Navigate(['/profile'])).subscribe(() => this.clearSearch())
      : this.store.dispatch(new Navigate(['/profile/' + userId])).subscribe(() => this.clearSearch());
  }

  goToPostsByTag(hashtags: string) {
    this.store.dispatch(new Navigate(['/explore/tags', hashtags])).subscribe(() => this.clearSearch());
  }

  getImageUrl(url: string) {
    return `${environment.urlForImage}${url}`;
  }

  onLogoutOut() {
    this.store.dispatch(new Logout());
  }

  loadNotif() {
    if (this.dropdownStateNotif) {
      this.store.dispatch(new GetRecent10());
    }
  }

  closeDropdownNotif() {
    this.dropdownStateNotif = false;
  }

  closeDropdownProfile() {
    this.dropdownState = false;
  }
}
