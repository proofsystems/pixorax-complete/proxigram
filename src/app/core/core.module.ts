import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { PublicLayoutComponent } from './layouts/public/public-layout.component';
import { PrivateLayoutComponent } from './layouts/private/private-layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpRequestInterceptor } from './interceptors/http-request.interceptor';
import { HomeModule } from '@app/home/home.module';
import { environment } from '@env/environment';
import { LoaderComponent } from './loader/loader.component';
import { CommonModule } from '@angular/common';
import { DateAgoPipe } from './pipes/date-ago.pipe';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { StoryModalComponent } from './modals/story-modal/story-modal.component';

// NG-Bootstrap
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';

// NG-Inline-SVG
import { InlineSVGModule } from 'ng-inline-svg';

// NGXS
import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NGXS_PLUGINS } from '@ngxs/store';
import { logoutPlugin } from '@app/auth/plugins/logout.plugin';
import { AuthState } from '@app/core/store/auth.state';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { PostState } from '@app/core/store/post.state';
import { NotificationState } from '@app/core/store/notification.state';
import { StoryState } from './store/story.state';
import { ProfileState } from '@app/core/store/profile.state';

export const COMPONENTS = [
  PrivateLayoutComponent,
  PublicLayoutComponent,
  NavbarComponent,
  LoaderComponent,
  DateAgoPipe,
  ClickOutsideDirective,
  StoryModalComponent
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    HomeModule,
    NgbCarouselModule,
    InlineSVGModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    NgxsModule.forRoot([AuthState, PostState, NotificationState, StoryState, ProfileState]),
    NgxsRouterPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot({
      disabled: environment.production
    }),
    NgxsStoragePluginModule.forRoot({
      key: ['auth.token', 'auth.id', 'auth.profilePhoto', 'auth.provider']
    })
  ],
  exports: [COMPONENTS],
  entryComponents: [StoryModalComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    {
      provide: NGXS_PLUGINS,
      useValue: logoutPlugin,
      multi: true
    }
  ]
})
export class CoreModule {}
