import { Routes } from '@angular/router';
import { HomeComponent } from '@app/home/home.component';

// Route for content layout with navbar, footer etc...
export const privateRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'profile',
    loadChildren: () => import('../../profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'post',
    loadChildren: () => import('../../post/post.module').then(m => m.PostModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('../../notifications/notifications.module').then(m => m.NotificationsModule)
  },
  {
    path: 'explore',
    loadChildren: () => import('../../explore/explore.module').then(m => m.ExploreModule)
  }
];
