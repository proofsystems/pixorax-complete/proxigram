export const environment = {
  production: true,
  url: 'https://proxigram-api.sidewalkdeveloper.com/api/v1',
  urlForImage: 'https://proxigram-api.sidewalkdeveloper.com/'
};
