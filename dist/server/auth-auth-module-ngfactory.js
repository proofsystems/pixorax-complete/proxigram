exports.ids = [3];
exports.modules = {

/***/ "Ite/":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/tslib/tslib.js
var tslib = __webpack_require__("zOht");

// EXTERNAL MODULE: ./src/app/core/services/countries.service.ts
var countries_service = __webpack_require__("NtT5");

// CONCATENATED MODULE: ./src/app/core/models/country.model.ts
class CountryStateModel {
}

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/operators/tap.js
var tap = __webpack_require__("vkgz");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/operators/catchError.js
var catchError = __webpack_require__("JIr8");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/observable/throwError.js
var throwError = __webpack_require__("z6cu");

// EXTERNAL MODULE: ./node_modules/@ngxs/store/fesm2015/ngxs-store.js
var ngxs_store = __webpack_require__("e1JD");

// EXTERNAL MODULE: ./src/app/shared/store/country.action.ts
var country_action = __webpack_require__("xR19");

// CONCATENATED MODULE: ./src/app/shared/store/country.state.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return country_state_CountryState; });





// NGXS


let country_state_CountryState = class CountryState {
    constructor(countriesService, store) {
        this.countriesService = countriesService;
        this.store = store;
    }
    static getAllCountries(state) {
        return state.countries;
    }
    getAllCountries({ patchState }, {}) {
        return this.countriesService.getAll().pipe(Object(tap["a" /* tap */])((result) => {
            console.log(result);
            patchState({
                countries: result
            });
        }), Object(catchError["a" /* catchError */])(err => {
            return Object(throwError["a" /* throwError */])(err);
        }));
    }
};
tslib["__decorate"]([
    Object(ngxs_store["a" /* Action */])(country_action["a" /* GetAllCountries */]),
    tslib["__metadata"]("design:type", Function),
    tslib["__metadata"]("design:paramtypes", [Object, country_action["a" /* GetAllCountries */]]),
    tslib["__metadata"]("design:returntype", void 0)
], country_state_CountryState.prototype, "getAllCountries", null);
tslib["__decorate"]([
    Object(ngxs_store["g" /* Selector */])(),
    tslib["__metadata"]("design:type", Function),
    tslib["__metadata"]("design:paramtypes", [CountryStateModel]),
    tslib["__metadata"]("design:returntype", void 0)
], country_state_CountryState, "getAllCountries", null);
country_state_CountryState = tslib["__decorate"]([
    Object(ngxs_store["h" /* State */])({
        name: 'country',
        defaults: {
            countries: []
        }
    }),
    tslib["__metadata"]("design:paramtypes", [countries_service["a" /* CountriesService */], ngxs_store["j" /* Store */]])
], country_state_CountryState);



/***/ }),

/***/ "NtT5":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CountriesService; });
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("IheW");




class CountriesService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_0__[/* environment */ "a"].url}/countries`;
    }
    getAll() {
        return this.httpClient.get(`${this.url}/all`);
    }
}
CountriesService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ factory: function CountriesService_Factory() { return new CountriesService(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); }, token: CountriesService, providedIn: "root" });


/***/ }),

/***/ "PCNd":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MODULES */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("SVse");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("iInd");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("s7LF");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("wTG2");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("G0yt");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("Nv++");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("419G");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng_inline_svg__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("MNke");



// NG-Select

// NG-Bootstrap


// Font Awesome

// NG-Inline-SVG

// NGX Infinite Scroll

const MODULES = [
    _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
    _angular_router__WEBPACK_IMPORTED_MODULE_1__[/* RouterModule */ "r"],
    _angular_forms__WEBPACK_IMPORTED_MODULE_2__[/* ReactiveFormsModule */ "q"],
    _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__[/* FontAwesomeModule */ "j"],
    _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__[/* NgSelectModule */ "c"],
    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__[/* NgbModalModule */ "v"],
    ng_inline_svg__WEBPACK_IMPORTED_MODULE_6__["InlineSVGModule"],
    ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_7__[/* InfiniteScrollModule */ "b"],
    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__[/* NgbCarouselModule */ "k"]
];
class SharedModule {
}


/***/ }),

/***/ "cAcB":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@angular/core/fesm2015/core.js
var core = __webpack_require__("8Y7J");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/ReplaySubject.js
var ReplaySubject = __webpack_require__("jtHE");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/BehaviorSubject.js
var BehaviorSubject = __webpack_require__("2Vo4");

// EXTERNAL MODULE: ./node_modules/@angular/common/fesm2015/common.js
var common = __webpack_require__("SVse");

// CONCATENATED MODULE: ./node_modules/angularx-social-login/angularx-social-login.js




/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthServiceConfig {
    /**
     * @param {?} providers
     */
    constructor(providers) {
        this.lazyLoad = false;
        this.providers = new Map();
        for (let i = 0; i < providers.length; i++) {
            /** @type {?} */
            let element = providers[i];
            this.providers.set(element.id, element.provider);
            this.lazyLoad = this.lazyLoad || element.lazyLoad;
        }
    }
}
class angularx_social_login_AuthService {
    /**
     * @param {?} config
     */
    constructor(config) {
        this._user = null;
        this._authState = new ReplaySubject["a" /* ReplaySubject */](1);
        this._readyState = new BehaviorSubject["a" /* BehaviorSubject */]([]);
        this.initialized = false;
        this.providers = config.providers;
        if (!config.lazyLoad) {
            this.initialize();
        }
    }
    /**
     * @return {?}
     */
    get authState() {
        return this._authState.asObservable();
    }
    /**
     * Provides an array of provider ID's as they become ready
     * @return {?}
     */
    get readyState() {
        return this._readyState.asObservable();
    }
    /**
     * @private
     * @return {?}
     */
    initialize() {
        this.initialized = true;
        this.providers.forEach((/**
         * @param {?} provider
         * @param {?} key
         * @return {?}
         */
        (provider, key) => {
            provider.initialize().then((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                let readyProviders = this._readyState.getValue();
                readyProviders.push(key);
                this._readyState.next(readyProviders);
                provider.getLoginStatus().then((/**
                 * @param {?} user
                 * @return {?}
                 */
                (user) => {
                    user.provider = key;
                    this._user = user;
                    this._authState.next(user);
                })).catch((/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    this._authState.next(null);
                }));
            }));
        }));
    }
    /**
     * @param {?} providerId
     * @param {?=} opt
     * @return {?}
     */
    signIn(providerId, opt) {
        if (!this.initialized) {
            this.initialize();
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let providerObject = this.providers.get(providerId);
            if (providerObject) {
                providerObject.signIn(opt).then((/**
                 * @param {?} user
                 * @return {?}
                 */
                (user) => {
                    user.provider = providerId;
                    resolve(user);
                    this._user = user;
                    this._authState.next(user);
                })).catch((/**
                 * @param {?} err
                 * @return {?}
                 */
                err => {
                    reject(err);
                }));
            }
            else {
                reject(angularx_social_login_AuthService.ERR_LOGIN_PROVIDER_NOT_FOUND);
            }
        }));
    }
    /**
     * @param {?=} revoke
     * @return {?}
     */
    signOut(revoke = false) {
        if (!this.initialized) {
            this.initialize();
        }
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            if (!this._user) {
                reject(angularx_social_login_AuthService.ERR_NOT_LOGGED_IN);
            }
            else {
                /** @type {?} */
                let providerId = this._user.provider;
                /** @type {?} */
                let providerObject = this.providers.get(providerId);
                if (providerObject) {
                    providerObject.signOut(revoke).then((/**
                     * @return {?}
                     */
                    () => {
                        resolve();
                        this._user = null;
                        this._authState.next(null);
                    })).catch((/**
                     * @param {?} err
                     * @return {?}
                     */
                    (err) => {
                        reject(err);
                    }));
                }
                else {
                    reject(angularx_social_login_AuthService.ERR_LOGIN_PROVIDER_NOT_FOUND);
                }
            }
        }));
    }
}
angularx_social_login_AuthService.ERR_LOGIN_PROVIDER_NOT_FOUND = 'Login provider not found';
angularx_social_login_AuthService.ERR_NOT_LOGGED_IN = 'Not logged in';
angularx_social_login_AuthService.decorators = [
    { type: core["Injectable"] },
];
/** @nocollapse */
angularx_social_login_AuthService.ctorParameters = () => [
    { type: AuthServiceConfig }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SocialLoginModule {
    /**
     * @param {?} config
     * @return {?}
     */
    static initialize(config) {
        return {
            ngModule: SocialLoginModule,
            providers: [
                angularx_social_login_AuthService,
                {
                    provide: AuthServiceConfig,
                    useValue: config
                }
            ]
        };
    }
}
SocialLoginModule.decorators = [
    { type: core["NgModule"], args: [{
                imports: [
                    common["CommonModule"]
                ],
                providers: [
                    angularx_social_login_AuthService
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SocialUser {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class angularx_social_login_BaseLoginProvider {
    constructor() {
        this._readyState = new BehaviorSubject["a" /* BehaviorSubject */](false);
    }
    /**
     * @protected
     * @return {?}
     */
    onReady() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this._readyState.subscribe((/**
             * @param {?} isReady
             * @return {?}
             */
            (isReady) => {
                if (isReady) {
                    resolve();
                }
            }));
        }));
    }
    /**
     * @param {?} id
     * @param {?} src
     * @param {?} onload
     * @param {?=} async
     * @param {?=} inner_text_content
     * @return {?}
     */
    loadScript(id, src, onload, async = true, inner_text_content = '') {
        // get document if platform is only browser
        if (typeof document !== 'undefined' && !document.getElementById(id)) {
            /** @type {?} */
            let signInJS = document.createElement('script');
            signInJS.async = async;
            signInJS.src = src;
            signInJS.onload = onload;
            /*
            if (inner_text_content) // LinkedIn
                signInJS.text = inner_text_content;
            */
            document.head.appendChild(signInJS);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GoogleLoginProvider extends angularx_social_login_BaseLoginProvider {
    /**
     * @param {?} clientId
     * @param {?=} opt
     */
    constructor(clientId, opt = { scope: 'email' }) {
        super();
        this.clientId = clientId;
        this.opt = opt;
    }
    /**
     * @return {?}
     */
    initialize() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.loadScript(GoogleLoginProvider.PROVIDER_ID, 'https://apis.google.com/js/platform.js', (/**
             * @return {?}
             */
            () => {
                gapi.load('auth2', (/**
                 * @return {?}
                 */
                () => {
                    this.auth2 = gapi.auth2.init(Object.assign({}, this.opt, { client_id: this.clientId }));
                    this.auth2.then((/**
                     * @return {?}
                     */
                    () => {
                        this._readyState.next(true);
                        resolve();
                    })).catch((/**
                     * @param {?} err
                     * @return {?}
                     */
                    (err) => {
                        reject(err);
                    }));
                }));
            }));
        }));
    }
    /**
     * @return {?}
     */
    getLoginStatus() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.onReady().then((/**
             * @return {?}
             */
            () => {
                if (this.auth2.isSignedIn.get()) {
                    /** @type {?} */
                    let user = new SocialUser();
                    /** @type {?} */
                    let profile = this.auth2.currentUser.get().getBasicProfile();
                    /** @type {?} */
                    let token = this.auth2.currentUser.get().getAuthResponse(true).access_token;
                    /** @type {?} */
                    let backendToken = this.auth2.currentUser.get().getAuthResponse(true).id_token;
                    user.id = profile.getId();
                    user.name = profile.getName();
                    user.email = profile.getEmail();
                    user.photoUrl = profile.getImageUrl();
                    user.firstName = profile.getGivenName();
                    user.lastName = profile.getFamilyName();
                    user.authToken = token;
                    user.idToken = backendToken;
                    resolve(user);
                }
                else {
                    reject('No user is currently logged in.');
                }
            }));
        }));
    }
    /**
     * @param {?=} opt
     * @return {?}
     */
    signIn(opt) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.onReady().then((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const offlineAccess = (opt && opt.offline_access) || (this.opt && this.opt.offline_access);
                /** @type {?} */
                let promise = !offlineAccess ? this.auth2.signIn(opt) : this.auth2.grantOfflineAccess(opt);
                promise.then((/**
                 * @param {?} response
                 * @return {?}
                 */
                (response) => {
                    /** @type {?} */
                    let user = new SocialUser();
                    /** @type {?} */
                    let profile = this.auth2.currentUser.get().getBasicProfile();
                    /** @type {?} */
                    let token = this.auth2.currentUser.get().getAuthResponse(true).access_token;
                    /** @type {?} */
                    let backendToken = this.auth2.currentUser.get().getAuthResponse(true).id_token;
                    user.id = profile.getId();
                    user.name = profile.getName();
                    user.email = profile.getEmail();
                    user.photoUrl = profile.getImageUrl();
                    user.firstName = profile.getGivenName();
                    user.lastName = profile.getFamilyName();
                    user.authToken = token;
                    user.idToken = backendToken;
                    if (response && response.code) {
                        user.authorizationCode = response.code;
                    }
                    resolve(user);
                }), (/**
                 * @param {?} closed
                 * @return {?}
                 */
                (closed) => {
                    reject('User cancelled login or did not fully authorize.');
                })).catch((/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    reject(err);
                }));
            }));
        }));
    }
    /**
     * @param {?=} revoke
     * @return {?}
     */
    signOut(revoke) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.onReady().then((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                let signOutPromise;
                if (revoke) {
                    signOutPromise = this.auth2.disconnect();
                }
                else {
                    signOutPromise = this.auth2.signOut();
                }
                signOutPromise.then((/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve();
                    }
                })).catch((/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    reject(err);
                }));
            }));
        }));
    }
}
GoogleLoginProvider.PROVIDER_ID = 'GOOGLE';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FacebookLoginProvider extends angularx_social_login_BaseLoginProvider {
    /**
     * @param {?} clientId
     * @param {?=} opt
     * @param {?=} locale
     * @param {?=} fields
     * @param {?=} version
     */
    constructor(clientId, opt = { scope: 'email,public_profile' }, locale = 'en_US', fields = 'name,email,picture,first_name,last_name', version = 'v4.0') {
        super();
        this.clientId = clientId;
        this.opt = opt;
        this.locale = locale;
        this.fields = fields;
        this.version = version;
    }
    /**
     * @return {?}
     */
    initialize() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.loadScript(FacebookLoginProvider.PROVIDER_ID, `//connect.facebook.net/${this.locale}/sdk.js`, (/**
             * @return {?}
             */
            () => {
                FB.init({
                    appId: this.clientId,
                    autoLogAppEvents: true,
                    cookie: true,
                    xfbml: true,
                    version: this.version
                });
                // FB.AppEvents.logPageView(); #FIX for #18
                this._readyState.next(true);
                resolve();
            }));
        }));
    }
    /**
     * @return {?}
     */
    getLoginStatus() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.onReady().then((/**
             * @return {?}
             */
            () => {
                FB.getLoginStatus((/**
                 * @param {?} response
                 * @return {?}
                 */
                (response) => {
                    if (response.status === 'connected') {
                        /** @type {?} */
                        let authResponse = response.authResponse;
                        FB.api(`/me?fields=${this.fields}`, (/**
                         * @param {?} fbUser
                         * @return {?}
                         */
                        (fbUser) => {
                            /** @type {?} */
                            let user = new SocialUser();
                            user.id = fbUser.id;
                            user.name = fbUser.name;
                            user.email = fbUser.email;
                            user.photoUrl = 'https://graph.facebook.com/' + fbUser.id + '/picture?type=normal';
                            user.firstName = fbUser.first_name;
                            user.lastName = fbUser.last_name;
                            user.authToken = authResponse.accessToken;
                            user.facebook = fbUser;
                            resolve(user);
                        }));
                    }
                    else {
                        reject('No user is currently logged in.');
                    }
                }));
            }));
        }));
    }
    /**
     * @param {?=} opt
     * @return {?}
     */
    signIn(opt) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.onReady().then((/**
             * @return {?}
             */
            () => {
                FB.login((/**
                 * @param {?} response
                 * @return {?}
                 */
                (response) => {
                    if (response.authResponse) {
                        /** @type {?} */
                        let authResponse = response.authResponse;
                        FB.api(`/me?fields=${this.fields}`, (/**
                         * @param {?} fbUser
                         * @return {?}
                         */
                        (fbUser) => {
                            /** @type {?} */
                            let user = new SocialUser();
                            user.id = fbUser.id;
                            user.name = fbUser.name;
                            user.email = fbUser.email;
                            user.photoUrl = 'https://graph.facebook.com/' + fbUser.id + '/picture?type=normal';
                            user.firstName = fbUser.first_name;
                            user.lastName = fbUser.last_name;
                            user.authToken = authResponse.accessToken;
                            user.facebook = fbUser;
                            resolve(user);
                        }));
                    }
                    else {
                        reject('User cancelled login or did not fully authorize.');
                    }
                }), this.opt);
            }));
        }));
    }
    /**
     * @return {?}
     */
    signOut() {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.onReady().then((/**
             * @return {?}
             */
            () => {
                FB.logout((/**
                 * @param {?} response
                 * @return {?}
                 */
                (response) => {
                    resolve();
                }));
            }));
        }));
    }
}
FacebookLoginProvider.PROVIDER_ID = 'FACEBOOK';


//# sourceMappingURL=angularx-social-login.js.map

// CONCATENATED MODULE: ./src/app/auth/auth.module.ts


const ɵ0 = GoogleLoginProvider.PROVIDER_ID, ɵ1 = FacebookLoginProvider.PROVIDER_ID;
const auth_module_config = new AuthServiceConfig([
    {
        id: ɵ0,
        provider: new GoogleLoginProvider('690267179396-ipek08rj1ok45s0p1ddul85tj50s9of8.apps.googleusercontent.com')
    },
    {
        id: ɵ1,
        provider: new FacebookLoginProvider('2404930759822904')
    }
]);
function provideConfig() {
    return auth_module_config;
}
class AuthModule {
}


// EXTERNAL MODULE: ./node_modules/@angular/router/router.ngfactory.js
var router_ngfactory = __webpack_require__("pMnS");

// CONCATENATED MODULE: ./src/app/auth/login/login.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = [""];


// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/svg-cache.service.js
var svg_cache_service = __webpack_require__("YAkT");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.config.js
var inline_svg_config = __webpack_require__("SsaO");

// EXTERNAL MODULE: ./node_modules/@angular/common/fesm2015/http.js
var http = __webpack_require__("IheW");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.directive.js
var inline_svg_directive = __webpack_require__("Iv/j");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.service.js
var inline_svg_service = __webpack_require__("yLZu");

// EXTERNAL MODULE: ./node_modules/@angular/forms/fesm2015/forms.js
var fesm2015_forms = __webpack_require__("s7LF");

// EXTERNAL MODULE: ./node_modules/@angular/router/fesm2015/router.js
var router = __webpack_require__("iInd");

// EXTERNAL MODULE: ./node_modules/@ngxs/store/fesm2015/ngxs-store.js
var ngxs_store = __webpack_require__("e1JD");

// EXTERNAL MODULE: ./src/app/core/store/auth.action.ts
var auth_action = __webpack_require__("72RC");

// CONCATENATED MODULE: ./src/app/auth/login/login.component.ts





class login_component_LoginComponent {
    constructor(formBuilder, store, authService) {
        this.formBuilder = formBuilder;
        this.store = store;
        this.authService = authService;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.user = SocialUser;
        this.form = this.formBuilder.group({
            email: [null, fesm2015_forms["s" /* Validators */].required],
            password: [null, fesm2015_forms["s" /* Validators */].required]
        });
    }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    onLogIn(form) {
        this.formSubmitAttempt = true;
        if (form.valid) {
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            this.store.dispatch(new auth_action["a" /* Login */](form.value)).subscribe(() => {
                this.setSubmitButton(true);
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
    signInWithGoogle(platform) {
        platform = GoogleLoginProvider.PROVIDER_ID;
        this.authService.signIn(platform).then(response => {
            this.user = response.authToken;
            const payload = {
                access_token: response.authToken,
                provider: 'google'
            };
            this.store.dispatch(new auth_action["c" /* LoginGoogle */](payload)).subscribe(() => {
                console.log('success');
            }, err => {
                console.log(err);
            });
        });
    }
    signOut() {
        this.authService.signOut();
    }
    signInWithFacebook(platform) {
        // this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
        platform = FacebookLoginProvider.PROVIDER_ID;
        this.authService.signIn(platform).then(response => {
            this.user = response.authToken;
            const payload = {
                access_token: response.authToken,
                provider: response.provider
            };
            this.store.dispatch(new auth_action["b" /* LoginFacebook */](payload)).subscribe(() => {
                console.log('success');
            }, err => {
                console.log(err);
            });
        });
    }
}

// CONCATENATED MODULE: ./src/app/auth/login/login.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 













var styles_LoginComponent = [styles];
var RenderType_LoginComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_LoginComponent, data: {} });

function View_LoginComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_LoginComponent_2(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_LoginComponent_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 51, "div", [["class", "login auth card"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 16777216, null, null, 2, "div", [["alt", ""], ["class", "logo"]], null, null, null, null, null)), core["ɵprd"](512, null, svg_cache_service["SVGCacheService"], svg_cache_service["SVGCacheService"], [[2, common["APP_BASE_HREF"]], [2, common["PlatformLocation"]], [2, inline_svg_config["InlineSVGConfig"]], http["HttpBackend"], core["RendererFactory2"]]), core["ɵdid"](3, 737280, null, 0, inline_svg_directive["InlineSVGDirective"], [core["ElementRef"], core["ViewContainerRef"], core["ComponentFactoryResolver"], svg_cache_service["SVGCacheService"], core["Renderer2"], inline_svg_service["InlineSVGService"], [2, inline_svg_config["InlineSVGConfig"]], core["PLATFORM_ID"]], { inlineSVG: [0, "inlineSVG"] }, null), (_l()(), core["ɵeld"](4, 0, null, null, 1, "div", [["class", "title"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Welcome to Pixorax"])), (_l()(), core["ɵeld"](6, 0, null, null, 45, "form", [["class", "form"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (core["ɵnov"](_v, 8).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (core["ɵnov"](_v, 8).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.onLogIn(_co.form) !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), core["ɵdid"](7, 16384, null, 0, fesm2015_forms["x" /* ɵangular_packages_forms_forms_z */], [], null, null), core["ɵdid"](8, 540672, null, 0, fesm2015_forms["g" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), core["ɵprd"](2048, null, fesm2015_forms["b" /* ControlContainer */], null, [fesm2015_forms["g" /* FormGroupDirective */]]), core["ɵdid"](10, 16384, null, 0, fesm2015_forms["m" /* NgControlStatusGroup */], [[4, fesm2015_forms["b" /* ControlContainer */]]], null, null), (_l()(), core["ɵeld"](11, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "email"], ["placeholder", "Email or Username"], ["type", "email"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 12)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 12).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 12)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 12)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](12, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](14, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](16, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_LoginComponent_1)), core["ɵdid"](18, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](19, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "password"], ["placeholder", "Password"], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 20)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 20).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 20)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 20)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](20, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](22, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](24, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_LoginComponent_2)), core["ɵdid"](26, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](27, 0, null, null, 1, "button", [["class", "primary button"], ["type", "submit"]], [[8, "disabled", 0]], null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Log in "])), (_l()(), core["ɵeld"](29, 0, null, null, 2, "button", [["class", "facebook button"], ["type", "button"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.signInWithFacebook(_co.platform) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), core["ɵeld"](30, 0, null, null, 0, "i", [["class", "fa fa-facebook-square icon"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Log in with Facebook "])), (_l()(), core["ɵeld"](32, 0, null, null, 2, "button", [["class", "google button"], ["type", "button"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.signInWithGoogle(_co.platform) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), core["ɵeld"](33, 0, null, null, 0, "i", [["class", "fa fa-google icon"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Log in Google "])), (_l()(), core["ɵeld"](35, 0, null, null, 8, "div", [["class", "links"]], null, null, null, null, null)), (_l()(), core["ɵeld"](36, 0, null, null, 3, "a", [["class", "forgot-password"], ["href", ""]], [[1, "target", 0], [8, "href", 4]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 37).onClick($event.button, $event.ctrlKey, $event.metaKey, $event.shiftKey) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](37, 671744, null, 0, router["q" /* RouterLinkWithHref */], [router["o" /* Router */], router["a" /* ActivatedRoute */], common["LocationStrategy"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](38, 1), (_l()(), core["ɵted"](-1, null, ["Forgot Password?"])), (_l()(), core["ɵeld"](40, 0, null, null, 3, "a", [["class", "sign-up"], ["href", ""]], [[1, "target", 0], [8, "href", 4]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 41).onClick($event.button, $event.ctrlKey, $event.metaKey, $event.shiftKey) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](41, 671744, null, 0, router["q" /* RouterLinkWithHref */], [router["o" /* Router */], router["a" /* ActivatedRoute */], common["LocationStrategy"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](42, 1), (_l()(), core["ɵted"](-1, null, ["Sign up!"])), (_l()(), core["ɵeld"](44, 0, null, null, 7, "div", [["class", "terms"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" By continuing, you agree to our "])), (_l()(), core["ɵeld"](46, 0, null, null, 0, "br", [], null, null, null, null, null)), (_l()(), core["ɵeld"](47, 0, null, null, 1, "span", [["class", "emphasize"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Terms of Service"])), (_l()(), core["ɵted"](-1, null, [" and "])), (_l()(), core["ɵeld"](50, 0, null, null, 1, "span", [["class", "emphasize"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Privacy Policy"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "assets/svg/proxigram_logo.svg"; _ck(_v, 3, 0, currVal_0); var currVal_8 = _co.form; _ck(_v, 8, 0, currVal_8); var currVal_16 = "email"; _ck(_v, 14, 0, currVal_16); var currVal_17 = ((!_co.form.get("email").valid && _co.form.get("email").touched) || (_co.form.get("email").untouched && _co.formSubmitAttempt)); _ck(_v, 18, 0, currVal_17); var currVal_25 = "password"; _ck(_v, 22, 0, currVal_25); var currVal_26 = ((!_co.form.get("password").valid && _co.form.get("password").touched) || (_co.form.get("password").untouched && _co.formSubmitAttempt)); _ck(_v, 26, 0, currVal_26); var currVal_30 = _ck(_v, 38, 0, "/forgot-password"); _ck(_v, 37, 0, currVal_30); var currVal_33 = _ck(_v, 42, 0, "/signup"); _ck(_v, 41, 0, currVal_33); }, function (_ck, _v) { var _co = _v.component; var currVal_1 = core["ɵnov"](_v, 10).ngClassUntouched; var currVal_2 = core["ɵnov"](_v, 10).ngClassTouched; var currVal_3 = core["ɵnov"](_v, 10).ngClassPristine; var currVal_4 = core["ɵnov"](_v, 10).ngClassDirty; var currVal_5 = core["ɵnov"](_v, 10).ngClassValid; var currVal_6 = core["ɵnov"](_v, 10).ngClassInvalid; var currVal_7 = core["ɵnov"](_v, 10).ngClassPending; _ck(_v, 6, 0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6, currVal_7); var currVal_9 = core["ɵnov"](_v, 16).ngClassUntouched; var currVal_10 = core["ɵnov"](_v, 16).ngClassTouched; var currVal_11 = core["ɵnov"](_v, 16).ngClassPristine; var currVal_12 = core["ɵnov"](_v, 16).ngClassDirty; var currVal_13 = core["ɵnov"](_v, 16).ngClassValid; var currVal_14 = core["ɵnov"](_v, 16).ngClassInvalid; var currVal_15 = core["ɵnov"](_v, 16).ngClassPending; _ck(_v, 11, 0, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15); var currVal_18 = core["ɵnov"](_v, 24).ngClassUntouched; var currVal_19 = core["ɵnov"](_v, 24).ngClassTouched; var currVal_20 = core["ɵnov"](_v, 24).ngClassPristine; var currVal_21 = core["ɵnov"](_v, 24).ngClassDirty; var currVal_22 = core["ɵnov"](_v, 24).ngClassValid; var currVal_23 = core["ɵnov"](_v, 24).ngClassInvalid; var currVal_24 = core["ɵnov"](_v, 24).ngClassPending; _ck(_v, 19, 0, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23, currVal_24); var currVal_27 = !_co.getSubmitButton(); _ck(_v, 27, 0, currVal_27); var currVal_28 = core["ɵnov"](_v, 37).target; var currVal_29 = core["ɵnov"](_v, 37).href; _ck(_v, 36, 0, currVal_28, currVal_29); var currVal_31 = core["ɵnov"](_v, 41).target; var currVal_32 = core["ɵnov"](_v, 41).href; _ck(_v, 40, 0, currVal_31, currVal_32); }); }
function View_LoginComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-login", [], null, null, null, View_LoginComponent_0, RenderType_LoginComponent)), core["ɵdid"](1, 49152, null, 0, login_component_LoginComponent, [fesm2015_forms["d" /* FormBuilder */], ngxs_store["j" /* Store */], angularx_social_login_AuthService], null, null)], null, null); }
var LoginComponentNgFactory = core["ɵccf"]("app-login", login_component_LoginComponent, View_LoginComponent_Host_0, {}, {}, []);


// CONCATENATED MODULE: ./src/app/auth/signup/signup.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var signup_component_scss_shim_ngstyle_styles = [""];


// EXTERNAL MODULE: ./node_modules/@ng-select/ng-select/ng-select-ng-select.ngfactory.js
var ng_select_ng_select_ngfactory = __webpack_require__("MJJn");

// EXTERNAL MODULE: ./node_modules/@ng-select/ng-select/fesm2015/ng-select-ng-select.js
var ng_select_ng_select = __webpack_require__("wTG2");

// EXTERNAL MODULE: ./node_modules/tslib/tslib.js
var tslib = __webpack_require__("zOht");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/Observable.js + 1 modules
var Observable = __webpack_require__("HDdC");

// CONCATENATED MODULE: ./src/app/shared/reusable-custom-validators/matching-passwords.validator.ts
function checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
    return (group) => {
        const passwordInput = group.controls[passwordKey], passwordConfirmationInput = group.controls[passwordConfirmationKey];
        if (passwordInput.value !== passwordConfirmationInput.value) {
            return passwordConfirmationInput.setErrors({ notEquivalent: true });
        }
        else {
            return passwordConfirmationInput.setErrors(null);
        }
    };
}

// EXTERNAL MODULE: ./src/app/shared/store/country.state.ts + 1 modules
var country_state = __webpack_require__("Ite/");

// EXTERNAL MODULE: ./src/app/shared/store/country.action.ts
var country_action = __webpack_require__("xR19");

// EXTERNAL MODULE: ./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js
var ngxs_router_plugin = __webpack_require__("AUKP");

// EXTERNAL MODULE: ./node_modules/sweetalert2/dist/sweetalert2.all.js
var sweetalert2_all = __webpack_require__("PSD3");
var sweetalert2_all_default = /*#__PURE__*/__webpack_require__.n(sweetalert2_all);

// CONCATENATED MODULE: ./src/app/auth/signup/signup.component.ts




// NGXS





// SweetAlert

class signup_component_SignupComponent {
    constructor(formBuilder, store) {
        this.formBuilder = formBuilder;
        this.store = store;
        this.emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.form = this.formBuilder.group({
            email: [null, [fesm2015_forms["s" /* Validators */].required, fesm2015_forms["s" /* Validators */].pattern(this.emailRegEx)]],
            username: [null, fesm2015_forms["s" /* Validators */].required],
            password: [null, [fesm2015_forms["s" /* Validators */].required, fesm2015_forms["s" /* Validators */].minLength(8)]],
            confirm_password: [null, [fesm2015_forms["s" /* Validators */].required, fesm2015_forms["s" /* Validators */].minLength(8)]],
            fname: [null, fesm2015_forms["s" /* Validators */].required],
            lname: [null, fesm2015_forms["s" /* Validators */].required],
            country_id: [null, fesm2015_forms["s" /* Validators */].required]
        }, { validator: checkIfMatchingPasswords('password', 'confirm_password') });
        this.store.dispatch(new country_action["a" /* GetAllCountries */]());
    }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    onSignUp(form) {
        console.log(form.value);
        this.formSubmitAttempt = true;
        if (form.valid) {
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            this.store.dispatch(new auth_action["h" /* SignUp */](form.value)).subscribe(() => {
                sweetalert2_all_default.a.fire('Success', 'Registered Successfully', 'success').then(() => {
                    this.store.dispatch(new ngxs_router_plugin["b" /* Navigate */](['/home']));
                });
                this.setSubmitButton(true);
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
}
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(country_state["a" /* CountryState */].getAllCountries),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], signup_component_SignupComponent.prototype, "countries$", void 0);

// CONCATENATED MODULE: ./src/app/auth/signup/signup.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 














var styles_SignupComponent = [signup_component_scss_shim_ngstyle_styles];
var RenderType_SignupComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_SignupComponent, data: {} });

function View_SignupComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_SignupComponent_2(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_SignupComponent_3(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This is not a valid email address! "]))], null, null); }
function View_SignupComponent_4(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_SignupComponent_5(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Password must be at least 8 characters! "]))], null, null); }
function View_SignupComponent_6(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Password do not match! "]))], null, null); }
function View_SignupComponent_7(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_SignupComponent_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 94, "div", [["class", "signup auth card"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 16777216, null, null, 2, "div", [["alt", ""], ["class", "logo"]], null, null, null, null, null)), core["ɵprd"](512, null, svg_cache_service["SVGCacheService"], svg_cache_service["SVGCacheService"], [[2, common["APP_BASE_HREF"]], [2, common["PlatformLocation"]], [2, inline_svg_config["InlineSVGConfig"]], http["HttpBackend"], core["RendererFactory2"]]), core["ɵdid"](3, 737280, null, 0, inline_svg_directive["InlineSVGDirective"], [core["ElementRef"], core["ViewContainerRef"], core["ComponentFactoryResolver"], svg_cache_service["SVGCacheService"], core["Renderer2"], inline_svg_service["InlineSVGService"], [2, inline_svg_config["InlineSVGConfig"]], core["PLATFORM_ID"]], { inlineSVG: [0, "inlineSVG"] }, null), (_l()(), core["ɵeld"](4, 0, null, null, 1, "div", [["class", "title"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Sign up to Pixorax"])), (_l()(), core["ɵeld"](6, 0, null, null, 88, "form", [["class", "form"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (core["ɵnov"](_v, 8).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (core["ɵnov"](_v, 8).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.onSignUp(_co.form) !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), core["ɵdid"](7, 16384, null, 0, fesm2015_forms["x" /* ɵangular_packages_forms_forms_z */], [], null, null), core["ɵdid"](8, 540672, null, 0, fesm2015_forms["g" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), core["ɵprd"](2048, null, fesm2015_forms["b" /* ControlContainer */], null, [fesm2015_forms["g" /* FormGroupDirective */]]), core["ɵdid"](10, 16384, null, 0, fesm2015_forms["m" /* NgControlStatusGroup */], [[4, fesm2015_forms["b" /* ControlContainer */]]], null, null), (_l()(), core["ɵeld"](11, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "fname"], ["placeholder", "First Name"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 12)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 12).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 12)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 12)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](12, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](14, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](16, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_SignupComponent_1)), core["ɵdid"](18, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](19, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "lname"], ["placeholder", "Last Name"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 20)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 20).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 20)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 20)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](20, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](22, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](24, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_SignupComponent_2)), core["ɵdid"](26, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](27, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "email"], ["placeholder", "Email"], ["type", "email"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 28)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 28).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 28)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 28)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](28, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](30, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](32, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_SignupComponent_3)), core["ɵdid"](34, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](35, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "username"], ["placeholder", "Username"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 36)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 36).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 36)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 36)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](36, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](38, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](40, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_SignupComponent_4)), core["ɵdid"](42, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](43, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "password"], ["placeholder", "Password"], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 44)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 44).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 44)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 44)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](44, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](46, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](48, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_SignupComponent_5)), core["ɵdid"](50, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](51, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "confirm_password"], ["placeholder", "Confirm Password"], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 52)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 52).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 52)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 52)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](52, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](54, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](56, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_SignupComponent_6)), core["ɵdid"](58, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](59, 0, null, null, 19, "ng-select", [["bindLabel", "country_name"], ["bindValue", "id"], ["class", "input custom ng-select"], ["formControlName", "country_id"], ["placeholder", "-- Select Country --"], ["role", "listbox"]], [[2, "ng-select-single", null], [2, "ng-select-typeahead", null], [2, "ng-select-multiple", null], [2, "ng-select-taggable", null], [2, "ng-select-searchable", null], [2, "ng-select-clearable", null], [2, "ng-select-opened", null], [2, "ng-select-disabled", null], [2, "ng-select-filtered", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "keydown"]], function (_v, en, $event) { var ad = true; if (("keydown" === en)) {
        var pd_0 = (core["ɵnov"](_v, 61).handleKeyDown($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, ng_select_ng_select_ngfactory["b" /* View_NgSelectComponent_0 */], ng_select_ng_select_ngfactory["a" /* RenderType_NgSelectComponent */])), core["ɵprd"](4608, null, ng_select_ng_select["f" /* ɵd */], ng_select_ng_select["f" /* ɵd */], []), core["ɵdid"](61, 4964352, null, 12, ng_select_ng_select["a" /* NgSelectComponent */], [[8, "input custom"], [8, null], ng_select_ng_select["b" /* NgSelectConfig */], ng_select_ng_select["d" /* SELECTION_MODEL_FACTORY */], core["ElementRef"], core["ChangeDetectorRef"], ng_select_ng_select["j" /* ɵs */]], { bindLabel: [0, "bindLabel"], bindValue: [1, "bindValue"], placeholder: [2, "placeholder"], items: [3, "items"] }, null), core["ɵqud"](335544320, 1, { optionTemplate: 0 }), core["ɵqud"](335544320, 2, { optgroupTemplate: 0 }), core["ɵqud"](335544320, 3, { labelTemplate: 0 }), core["ɵqud"](335544320, 4, { multiLabelTemplate: 0 }), core["ɵqud"](603979776, 5, { headerTemplate: 0 }), core["ɵqud"](603979776, 6, { footerTemplate: 0 }), core["ɵqud"](335544320, 7, { notFoundTemplate: 0 }), core["ɵqud"](335544320, 8, { typeToSearchTemplate: 0 }), core["ɵqud"](335544320, 9, { loadingTextTemplate: 0 }), core["ɵqud"](335544320, 10, { tagTemplate: 0 }), core["ɵqud"](335544320, 11, { loadingSpinnerTemplate: 0 }), core["ɵqud"](603979776, 12, { ngOptions: 1 }), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]]), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [ng_select_ng_select["a" /* NgSelectComponent */]]), core["ɵdid"](76, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](78, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_SignupComponent_7)), core["ɵdid"](80, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](81, 0, null, null, 1, "button", [["class", "primary button"], ["type", "submit"]], [[8, "disabled", 0]], null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Sign up "])), (_l()(), core["ɵeld"](83, 0, null, null, 3, "a", [["class", "login-link"]], [[1, "target", 0], [8, "href", 4]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 84).onClick($event.button, $event.ctrlKey, $event.metaKey, $event.shiftKey) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](84, 671744, null, 0, router["q" /* RouterLinkWithHref */], [router["o" /* Router */], router["a" /* ActivatedRoute */], common["LocationStrategy"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](85, 1), (_l()(), core["ɵted"](-1, null, ["Already have an account?"])), (_l()(), core["ɵeld"](87, 0, null, null, 7, "div", [["class", "terms"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" By continuing, you agree to our "])), (_l()(), core["ɵeld"](89, 0, null, null, 0, "br", [], null, null, null, null, null)), (_l()(), core["ɵeld"](90, 0, null, null, 1, "span", [["class", "emphasize"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Terms of Service"])), (_l()(), core["ɵted"](-1, null, [" and "])), (_l()(), core["ɵeld"](93, 0, null, null, 1, "span", [["class", "emphasize"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Privacy Policy"]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "assets/svg/proxigram_logo.svg"; _ck(_v, 3, 0, currVal_0); var currVal_8 = _co.form; _ck(_v, 8, 0, currVal_8); var currVal_16 = "fname"; _ck(_v, 14, 0, currVal_16); var currVal_17 = ((!_co.form.get("fname").valid && _co.form.get("fname").touched) || (_co.form.get("fname").untouched && _co.formSubmitAttempt)); _ck(_v, 18, 0, currVal_17); var currVal_25 = "lname"; _ck(_v, 22, 0, currVal_25); var currVal_26 = ((!_co.form.get("lname").valid && _co.form.get("lname").touched) || (_co.form.get("lname").untouched && _co.formSubmitAttempt)); _ck(_v, 26, 0, currVal_26); var currVal_34 = "email"; _ck(_v, 30, 0, currVal_34); var currVal_35 = ((!_co.form.get("email").valid && _co.form.get("email").touched) || (_co.form.get("email").untouched && _co.formSubmitAttempt)); _ck(_v, 34, 0, currVal_35); var currVal_43 = "username"; _ck(_v, 38, 0, currVal_43); var currVal_44 = ((!_co.form.get("username").valid && _co.form.get("username").touched) || (_co.form.get("username").untouched && _co.formSubmitAttempt)); _ck(_v, 42, 0, currVal_44); var currVal_52 = "password"; _ck(_v, 46, 0, currVal_52); var currVal_53 = ((!_co.form.get("password").valid && _co.form.get("password").touched) || (_co.form.get("password").untouched && _co.formSubmitAttempt)); _ck(_v, 50, 0, currVal_53); var currVal_61 = "confirm_password"; _ck(_v, 54, 0, currVal_61); var currVal_62 = ((!_co.form.get("confirm_password").valid && _co.form.get("confirm_password").touched) || (_co.form.get("confirm_password").untouched && _co.formSubmitAttempt)); _ck(_v, 58, 0, currVal_62); var currVal_79 = "country_name"; var currVal_80 = "id"; var currVal_81 = "-- Select Country --"; var currVal_82 = core["ɵunv"](_v, 61, 3, core["ɵnov"](_v, 74).transform(_co.countries$)); _ck(_v, 61, 0, currVal_79, currVal_80, currVal_81, currVal_82); var currVal_83 = "country_id"; _ck(_v, 76, 0, currVal_83); var currVal_84 = ((!_co.form.get("country_id").valid && _co.form.get("country_id").touched) || (_co.form.get("country_id").untouched && _co.formSubmitAttempt)); _ck(_v, 80, 0, currVal_84); var currVal_88 = _ck(_v, 85, 0, "/login"); _ck(_v, 84, 0, currVal_88); }, function (_ck, _v) { var _co = _v.component; var currVal_1 = core["ɵnov"](_v, 10).ngClassUntouched; var currVal_2 = core["ɵnov"](_v, 10).ngClassTouched; var currVal_3 = core["ɵnov"](_v, 10).ngClassPristine; var currVal_4 = core["ɵnov"](_v, 10).ngClassDirty; var currVal_5 = core["ɵnov"](_v, 10).ngClassValid; var currVal_6 = core["ɵnov"](_v, 10).ngClassInvalid; var currVal_7 = core["ɵnov"](_v, 10).ngClassPending; _ck(_v, 6, 0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6, currVal_7); var currVal_9 = core["ɵnov"](_v, 16).ngClassUntouched; var currVal_10 = core["ɵnov"](_v, 16).ngClassTouched; var currVal_11 = core["ɵnov"](_v, 16).ngClassPristine; var currVal_12 = core["ɵnov"](_v, 16).ngClassDirty; var currVal_13 = core["ɵnov"](_v, 16).ngClassValid; var currVal_14 = core["ɵnov"](_v, 16).ngClassInvalid; var currVal_15 = core["ɵnov"](_v, 16).ngClassPending; _ck(_v, 11, 0, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15); var currVal_18 = core["ɵnov"](_v, 24).ngClassUntouched; var currVal_19 = core["ɵnov"](_v, 24).ngClassTouched; var currVal_20 = core["ɵnov"](_v, 24).ngClassPristine; var currVal_21 = core["ɵnov"](_v, 24).ngClassDirty; var currVal_22 = core["ɵnov"](_v, 24).ngClassValid; var currVal_23 = core["ɵnov"](_v, 24).ngClassInvalid; var currVal_24 = core["ɵnov"](_v, 24).ngClassPending; _ck(_v, 19, 0, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23, currVal_24); var currVal_27 = core["ɵnov"](_v, 32).ngClassUntouched; var currVal_28 = core["ɵnov"](_v, 32).ngClassTouched; var currVal_29 = core["ɵnov"](_v, 32).ngClassPristine; var currVal_30 = core["ɵnov"](_v, 32).ngClassDirty; var currVal_31 = core["ɵnov"](_v, 32).ngClassValid; var currVal_32 = core["ɵnov"](_v, 32).ngClassInvalid; var currVal_33 = core["ɵnov"](_v, 32).ngClassPending; _ck(_v, 27, 0, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31, currVal_32, currVal_33); var currVal_36 = core["ɵnov"](_v, 40).ngClassUntouched; var currVal_37 = core["ɵnov"](_v, 40).ngClassTouched; var currVal_38 = core["ɵnov"](_v, 40).ngClassPristine; var currVal_39 = core["ɵnov"](_v, 40).ngClassDirty; var currVal_40 = core["ɵnov"](_v, 40).ngClassValid; var currVal_41 = core["ɵnov"](_v, 40).ngClassInvalid; var currVal_42 = core["ɵnov"](_v, 40).ngClassPending; _ck(_v, 35, 0, currVal_36, currVal_37, currVal_38, currVal_39, currVal_40, currVal_41, currVal_42); var currVal_45 = core["ɵnov"](_v, 48).ngClassUntouched; var currVal_46 = core["ɵnov"](_v, 48).ngClassTouched; var currVal_47 = core["ɵnov"](_v, 48).ngClassPristine; var currVal_48 = core["ɵnov"](_v, 48).ngClassDirty; var currVal_49 = core["ɵnov"](_v, 48).ngClassValid; var currVal_50 = core["ɵnov"](_v, 48).ngClassInvalid; var currVal_51 = core["ɵnov"](_v, 48).ngClassPending; _ck(_v, 43, 0, currVal_45, currVal_46, currVal_47, currVal_48, currVal_49, currVal_50, currVal_51); var currVal_54 = core["ɵnov"](_v, 56).ngClassUntouched; var currVal_55 = core["ɵnov"](_v, 56).ngClassTouched; var currVal_56 = core["ɵnov"](_v, 56).ngClassPristine; var currVal_57 = core["ɵnov"](_v, 56).ngClassDirty; var currVal_58 = core["ɵnov"](_v, 56).ngClassValid; var currVal_59 = core["ɵnov"](_v, 56).ngClassInvalid; var currVal_60 = core["ɵnov"](_v, 56).ngClassPending; _ck(_v, 51, 0, currVal_54, currVal_55, currVal_56, currVal_57, currVal_58, currVal_59, currVal_60); var currVal_63 = !core["ɵnov"](_v, 61).multiple; var currVal_64 = core["ɵnov"](_v, 61).typeahead; var currVal_65 = core["ɵnov"](_v, 61).multiple; var currVal_66 = core["ɵnov"](_v, 61).addTag; var currVal_67 = core["ɵnov"](_v, 61).searchable; var currVal_68 = core["ɵnov"](_v, 61).clearable; var currVal_69 = core["ɵnov"](_v, 61).isOpen; var currVal_70 = core["ɵnov"](_v, 61).disabled; var currVal_71 = core["ɵnov"](_v, 61).filtered; var currVal_72 = core["ɵnov"](_v, 78).ngClassUntouched; var currVal_73 = core["ɵnov"](_v, 78).ngClassTouched; var currVal_74 = core["ɵnov"](_v, 78).ngClassPristine; var currVal_75 = core["ɵnov"](_v, 78).ngClassDirty; var currVal_76 = core["ɵnov"](_v, 78).ngClassValid; var currVal_77 = core["ɵnov"](_v, 78).ngClassInvalid; var currVal_78 = core["ɵnov"](_v, 78).ngClassPending; _ck(_v, 59, 1, [currVal_63, currVal_64, currVal_65, currVal_66, currVal_67, currVal_68, currVal_69, currVal_70, currVal_71, currVal_72, currVal_73, currVal_74, currVal_75, currVal_76, currVal_77, currVal_78]); var currVal_85 = !_co.getSubmitButton(); _ck(_v, 81, 0, currVal_85); var currVal_86 = core["ɵnov"](_v, 84).target; var currVal_87 = core["ɵnov"](_v, 84).href; _ck(_v, 83, 0, currVal_86, currVal_87); }); }
function View_SignupComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-signup", [], null, null, null, View_SignupComponent_0, RenderType_SignupComponent)), core["ɵdid"](1, 49152, null, 0, signup_component_SignupComponent, [fesm2015_forms["d" /* FormBuilder */], ngxs_store["j" /* Store */]], null, null)], null, null); }
var SignupComponentNgFactory = core["ɵccf"]("app-signup", signup_component_SignupComponent, View_SignupComponent_Host_0, {}, {}, []);


// CONCATENATED MODULE: ./src/app/auth/forgot-password/forgot-password.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var forgot_password_component_scss_shim_ngstyle_styles = [""];


// CONCATENATED MODULE: ./src/app/auth/forgot-password/forgot-password.component.ts


// NGXS

class forgot_password_component_ForgotPasswordComponent {
    constructor(formBuilder, store) {
        this.formBuilder = formBuilder;
        this.store = store;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        this.form = this.formBuilder.group({
            email: [null, [fesm2015_forms["s" /* Validators */].required, fesm2015_forms["s" /* Validators */].pattern(this.emailRegEx)]]
        });
    }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    onRequestResetPassword(form) {
        this.formSubmitAttempt = true;
        if (form.valid) {
            console.log(form.value);
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            this.store.dispatch(new auth_action["f" /* RequestResetPassword */](form.value)).subscribe(() => {
                this.setSubmitButton(true);
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
}

// CONCATENATED MODULE: ./src/app/auth/forgot-password/forgot-password.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 












var styles_ForgotPasswordComponent = [forgot_password_component_scss_shim_ngstyle_styles];
var RenderType_ForgotPasswordComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_ForgotPasswordComponent, data: {} });

function View_ForgotPasswordComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This is not a valid email address! "]))], null, null); }
function View_ForgotPasswordComponent_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 36, "div", [["class", "forgot-password auth card"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 16777216, null, null, 2, "div", [["alt", ""], ["class", "logo"]], null, null, null, null, null)), core["ɵprd"](512, null, svg_cache_service["SVGCacheService"], svg_cache_service["SVGCacheService"], [[2, common["APP_BASE_HREF"]], [2, common["PlatformLocation"]], [2, inline_svg_config["InlineSVGConfig"]], http["HttpBackend"], core["RendererFactory2"]]), core["ɵdid"](3, 737280, null, 0, inline_svg_directive["InlineSVGDirective"], [core["ElementRef"], core["ViewContainerRef"], core["ComponentFactoryResolver"], svg_cache_service["SVGCacheService"], core["Renderer2"], inline_svg_service["InlineSVGService"], [2, inline_svg_config["InlineSVGConfig"]], core["PLATFORM_ID"]], { inlineSVG: [0, "inlineSVG"] }, null), (_l()(), core["ɵeld"](4, 0, null, null, 1, "div", [["class", "title"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Forgot Password"])), (_l()(), core["ɵeld"](6, 0, null, null, 1, "p", [["class", "info"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Enter the email address associated with your account."])), (_l()(), core["ɵeld"](8, 0, null, null, 28, "form", [["class", "form"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (core["ɵnov"](_v, 10).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (core["ɵnov"](_v, 10).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.onRequestResetPassword(_co.form) !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), core["ɵdid"](9, 16384, null, 0, fesm2015_forms["x" /* ɵangular_packages_forms_forms_z */], [], null, null), core["ɵdid"](10, 540672, null, 0, fesm2015_forms["g" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), core["ɵprd"](2048, null, fesm2015_forms["b" /* ControlContainer */], null, [fesm2015_forms["g" /* FormGroupDirective */]]), core["ɵdid"](12, 16384, null, 0, fesm2015_forms["m" /* NgControlStatusGroup */], [[4, fesm2015_forms["b" /* ControlContainer */]]], null, null), (_l()(), core["ɵeld"](13, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "email"], ["placeholder", "Email"], ["type", "email"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 14)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 14).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 14)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 14)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](14, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](16, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](18, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ForgotPasswordComponent_1)), core["ɵdid"](20, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](21, 0, null, null, 1, "button", [["class", "primary button"], ["type", "submit"]], [[8, "disabled", 0]], null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Submit "])), (_l()(), core["ɵeld"](23, 0, null, null, 4, "div", [["class", "seperator"]], null, null, null, null, null)), (_l()(), core["ɵeld"](24, 0, null, null, 0, "div", [["class", "side"]], null, null, null, null, null)), (_l()(), core["ɵeld"](25, 0, null, null, 1, "div", [["class", "middle"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["OR"])), (_l()(), core["ɵeld"](27, 0, null, null, 0, "div", [["class", "side"]], null, null, null, null, null)), (_l()(), core["ɵeld"](28, 0, null, null, 3, "button", [["class", "primary button"], ["type", "button"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 29).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](29, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](30, 1), (_l()(), core["ɵted"](-1, null, [" Create new account "])), (_l()(), core["ɵeld"](32, 0, null, null, 0, "br", [], null, null, null, null, null)), (_l()(), core["ɵeld"](33, 0, null, null, 3, "button", [["class", "white button"], ["type", "button"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 34).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](34, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](35, 1), (_l()(), core["ɵted"](-1, null, [" Back to log in "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "assets/svg/proxigram_logo.svg"; _ck(_v, 3, 0, currVal_0); var currVal_8 = _co.form; _ck(_v, 10, 0, currVal_8); var currVal_16 = "email"; _ck(_v, 16, 0, currVal_16); var currVal_17 = ((!_co.form.get("email").valid && _co.form.get("email").touched) || (_co.form.get("email").untouched && _co.formSubmitAttempt)); _ck(_v, 20, 0, currVal_17); var currVal_19 = _ck(_v, 30, 0, "/signup"); _ck(_v, 29, 0, currVal_19); var currVal_20 = _ck(_v, 35, 0, "/login"); _ck(_v, 34, 0, currVal_20); }, function (_ck, _v) { var _co = _v.component; var currVal_1 = core["ɵnov"](_v, 12).ngClassUntouched; var currVal_2 = core["ɵnov"](_v, 12).ngClassTouched; var currVal_3 = core["ɵnov"](_v, 12).ngClassPristine; var currVal_4 = core["ɵnov"](_v, 12).ngClassDirty; var currVal_5 = core["ɵnov"](_v, 12).ngClassValid; var currVal_6 = core["ɵnov"](_v, 12).ngClassInvalid; var currVal_7 = core["ɵnov"](_v, 12).ngClassPending; _ck(_v, 8, 0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6, currVal_7); var currVal_9 = core["ɵnov"](_v, 18).ngClassUntouched; var currVal_10 = core["ɵnov"](_v, 18).ngClassTouched; var currVal_11 = core["ɵnov"](_v, 18).ngClassPristine; var currVal_12 = core["ɵnov"](_v, 18).ngClassDirty; var currVal_13 = core["ɵnov"](_v, 18).ngClassValid; var currVal_14 = core["ɵnov"](_v, 18).ngClassInvalid; var currVal_15 = core["ɵnov"](_v, 18).ngClassPending; _ck(_v, 13, 0, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15); var currVal_18 = !_co.getSubmitButton(); _ck(_v, 21, 0, currVal_18); }); }
function View_ForgotPasswordComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-forgot-password", [], null, null, null, View_ForgotPasswordComponent_0, RenderType_ForgotPasswordComponent)), core["ɵdid"](1, 49152, null, 0, forgot_password_component_ForgotPasswordComponent, [fesm2015_forms["d" /* FormBuilder */], ngxs_store["j" /* Store */]], null, null)], null, null); }
var ForgotPasswordComponentNgFactory = core["ɵccf"]("app-forgot-password", forgot_password_component_ForgotPasswordComponent, View_ForgotPasswordComponent_Host_0, {}, {}, []);


// CONCATENATED MODULE: ./src/app/auth/reset-password/reset-password.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var reset_password_component_scss_shim_ngstyle_styles = [""];


// CONCATENATED MODULE: ./src/app/auth/reset-password/reset-password.component.ts




// NGXS

class reset_password_component_ResetPasswordComponent {
    constructor(formBuilder, store, route) {
        this.formBuilder = formBuilder;
        this.store = store;
        this.route = route;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.form = this.formBuilder.group({
            email: [null, fesm2015_forms["s" /* Validators */].required],
            token: [null, fesm2015_forms["s" /* Validators */].required],
            password: [null, [fesm2015_forms["s" /* Validators */].required, fesm2015_forms["s" /* Validators */].minLength(8)]],
            password_confirmation: [null, [fesm2015_forms["s" /* Validators */].required, fesm2015_forms["s" /* Validators */].minLength(8)]]
        }, { validator: checkIfMatchingPasswords('password', 'password_confirmation') });
        this.route.url.subscribe(val => {
            console.log(val[2].path);
            console.log(val[3].path);
            this.form.get('email').setValue(val[2].path);
            this.form.get('token').setValue(val[3].path);
        });
    }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    onResetPassword(form) {
        this.formSubmitAttempt = true;
        if (form.valid) {
            console.log(form.value);
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            this.store.dispatch(new auth_action["g" /* ResetPassword */](form.value)).subscribe(() => {
                this.setSubmitButton(true);
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
}

// CONCATENATED MODULE: ./src/app/auth/reset-password/reset-password.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 












var styles_ResetPasswordComponent = [reset_password_component_scss_shim_ngstyle_styles];
var RenderType_ResetPasswordComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_ResetPasswordComponent, data: {} });

function View_ResetPasswordComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Password must be at least 8 characters! "]))], null, null); }
function View_ResetPasswordComponent_2(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Password do not match! "]))], null, null); }
function View_ResetPasswordComponent_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 28, "div", [["class", "forgot-password auth card"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 16777216, null, null, 2, "div", [["alt", ""], ["class", "logo"]], null, null, null, null, null)), core["ɵprd"](512, null, svg_cache_service["SVGCacheService"], svg_cache_service["SVGCacheService"], [[2, common["APP_BASE_HREF"]], [2, common["PlatformLocation"]], [2, inline_svg_config["InlineSVGConfig"]], http["HttpBackend"], core["RendererFactory2"]]), core["ɵdid"](3, 737280, null, 0, inline_svg_directive["InlineSVGDirective"], [core["ElementRef"], core["ViewContainerRef"], core["ComponentFactoryResolver"], svg_cache_service["SVGCacheService"], core["Renderer2"], inline_svg_service["InlineSVGService"], [2, inline_svg_config["InlineSVGConfig"]], core["PLATFORM_ID"]], { inlineSVG: [0, "inlineSVG"] }, null), (_l()(), core["ɵeld"](4, 0, null, null, 1, "div", [["class", "title"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Reset Password"])), (_l()(), core["ɵeld"](6, 0, null, null, 22, "form", [["class", "form"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (core["ɵnov"](_v, 8).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (core["ɵnov"](_v, 8).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.onResetPassword(_co.form) !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), core["ɵdid"](7, 16384, null, 0, fesm2015_forms["x" /* ɵangular_packages_forms_forms_z */], [], null, null), core["ɵdid"](8, 540672, null, 0, fesm2015_forms["g" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), core["ɵprd"](2048, null, fesm2015_forms["b" /* ControlContainer */], null, [fesm2015_forms["g" /* FormGroupDirective */]]), core["ɵdid"](10, 16384, null, 0, fesm2015_forms["m" /* NgControlStatusGroup */], [[4, fesm2015_forms["b" /* ControlContainer */]]], null, null), (_l()(), core["ɵeld"](11, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "password"], ["placeholder", "Password"], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 12)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 12).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 12)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 12)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](12, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](14, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](16, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ResetPasswordComponent_1)), core["ɵdid"](18, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](19, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "password_confirmation"], ["placeholder", "Confirm Password"], ["type", "password"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 20)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 20).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 20)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 20)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](20, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](22, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](24, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ResetPasswordComponent_2)), core["ɵdid"](26, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](27, 0, null, null, 1, "button", [["class", "primary button"], ["type", "submit"]], [[8, "disabled", 0]], null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Submit "]))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "assets/svg/proxigram_logo.svg"; _ck(_v, 3, 0, currVal_0); var currVal_8 = _co.form; _ck(_v, 8, 0, currVal_8); var currVal_16 = "password"; _ck(_v, 14, 0, currVal_16); var currVal_17 = ((!_co.form.get("password").valid && _co.form.get("password").touched) || (_co.form.get("password").untouched && _co.formSubmitAttempt)); _ck(_v, 18, 0, currVal_17); var currVal_25 = "password_confirmation"; _ck(_v, 22, 0, currVal_25); var currVal_26 = ((!_co.form.get("password_confirmation").valid && _co.form.get("password_confirmation").touched) || (_co.form.get("password_confirmation").untouched && _co.formSubmitAttempt)); _ck(_v, 26, 0, currVal_26); }, function (_ck, _v) { var _co = _v.component; var currVal_1 = core["ɵnov"](_v, 10).ngClassUntouched; var currVal_2 = core["ɵnov"](_v, 10).ngClassTouched; var currVal_3 = core["ɵnov"](_v, 10).ngClassPristine; var currVal_4 = core["ɵnov"](_v, 10).ngClassDirty; var currVal_5 = core["ɵnov"](_v, 10).ngClassValid; var currVal_6 = core["ɵnov"](_v, 10).ngClassInvalid; var currVal_7 = core["ɵnov"](_v, 10).ngClassPending; _ck(_v, 6, 0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6, currVal_7); var currVal_9 = core["ɵnov"](_v, 16).ngClassUntouched; var currVal_10 = core["ɵnov"](_v, 16).ngClassTouched; var currVal_11 = core["ɵnov"](_v, 16).ngClassPristine; var currVal_12 = core["ɵnov"](_v, 16).ngClassDirty; var currVal_13 = core["ɵnov"](_v, 16).ngClassValid; var currVal_14 = core["ɵnov"](_v, 16).ngClassInvalid; var currVal_15 = core["ɵnov"](_v, 16).ngClassPending; _ck(_v, 11, 0, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15); var currVal_18 = core["ɵnov"](_v, 24).ngClassUntouched; var currVal_19 = core["ɵnov"](_v, 24).ngClassTouched; var currVal_20 = core["ɵnov"](_v, 24).ngClassPristine; var currVal_21 = core["ɵnov"](_v, 24).ngClassDirty; var currVal_22 = core["ɵnov"](_v, 24).ngClassValid; var currVal_23 = core["ɵnov"](_v, 24).ngClassInvalid; var currVal_24 = core["ɵnov"](_v, 24).ngClassPending; _ck(_v, 19, 0, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23, currVal_24); var currVal_27 = !_co.getSubmitButton(); _ck(_v, 27, 0, currVal_27); }); }
function View_ResetPasswordComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-reset-password", [], null, null, null, View_ResetPasswordComponent_0, RenderType_ResetPasswordComponent)), core["ɵdid"](1, 49152, null, 0, reset_password_component_ResetPasswordComponent, [fesm2015_forms["d" /* FormBuilder */], ngxs_store["j" /* Store */], router["a" /* ActivatedRoute */]], null, null)], null, null); }
var ResetPasswordComponentNgFactory = core["ɵccf"]("app-reset-password", reset_password_component_ResetPasswordComponent, View_ResetPasswordComponent_Host_0, {}, {}, []);


// EXTERNAL MODULE: ./node_modules/@fortawesome/angular-fontawesome/angular-fontawesome.ngfactory.js
var angular_fontawesome_ngfactory = __webpack_require__("fNgX");

// EXTERNAL MODULE: ./node_modules/@ng-bootstrap/ng-bootstrap/ng-bootstrap.ngfactory.js
var ng_bootstrap_ngfactory = __webpack_require__("9AJC");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.component.ngfactory.js
var inline_svg_component_ngfactory = __webpack_require__("yBPo");

// EXTERNAL MODULE: ./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js
var ng_bootstrap = __webpack_require__("G0yt");

// EXTERNAL MODULE: ./src/app/core/services/countries.service.ts
var countries_service = __webpack_require__("NtT5");

// EXTERNAL MODULE: ./src/app/core/store/auth.state.ts + 1 modules
var auth_state = __webpack_require__("llJr");

// CONCATENATED MODULE: ./src/app/core/guards/public-guard.service.ts

// NGXS





class public_guard_service_PublicGuard {
    constructor(store, router) {
        this.store = store;
        this.router = router;
    }
    canActivate() {
        const token = this.store.selectSnapshot(auth_state["a" /* AuthState */].token);
        if (token) {
            this.router.navigate(['/home']);
            return false;
        }
        else {
            return true;
        }
    }
}
public_guard_service_PublicGuard.ngInjectableDef = core["ɵɵdefineInjectable"]({ factory: function PublicGuard_Factory() { return new public_guard_service_PublicGuard(core["ɵɵinject"](ngxs_store["j" /* Store */]), core["ɵɵinject"](router["o" /* Router */])); }, token: public_guard_service_PublicGuard, providedIn: "root" });

// CONCATENATED MODULE: ./src/app/auth/auth-routing.module.ts






const routes = [
    {
        path: 'login',
        component: login_component_LoginComponent,
        canActivate: [public_guard_service_PublicGuard]
    },
    {
        path: 'signup',
        component: signup_component_SignupComponent,
        canActivate: [public_guard_service_PublicGuard]
    },
    {
        path: 'forgot-password',
        component: forgot_password_component_ForgotPasswordComponent,
        canActivate: [public_guard_service_PublicGuard]
    },
    {
        path: 'user/reset/:id/:id',
        component: reset_password_component_ResetPasswordComponent,
        canActivate: [public_guard_service_PublicGuard]
    }
];
class AuthRoutingModule {
}

// EXTERNAL MODULE: ./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js
var angular_fontawesome = __webpack_require__("Nv++");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.module.js
var inline_svg_module = __webpack_require__("0ibv");

// EXTERNAL MODULE: ./node_modules/ngx-infinite-scroll/modules/ngx-infinite-scroll.js
var ngx_infinite_scroll = __webpack_require__("MNke");

// EXTERNAL MODULE: ./node_modules/@ngxs/store/fesm2015/ngxs-store-internals.js
var ngxs_store_internals = __webpack_require__("Mrqg");

// EXTERNAL MODULE: ./src/app/shared/shared.module.ts
var shared_module = __webpack_require__("PCNd");

// CONCATENATED MODULE: ./src/app/auth/auth.module.ngfactory.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModuleNgFactory", function() { return AuthModuleNgFactory; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 






























var AuthModuleNgFactory = core["ɵcmf"](AuthModule, [], function (_l) { return core["ɵmod"]([core["ɵmpd"](512, core["ComponentFactoryResolver"], core["ɵCodegenComponentFactoryResolver"], [[8, [router_ngfactory["a" /* ɵangular_packages_router_router_lNgFactory */], LoginComponentNgFactory, SignupComponentNgFactory, ForgotPasswordComponentNgFactory, ResetPasswordComponentNgFactory, angular_fontawesome_ngfactory["b" /* FaIconComponentNgFactory */], angular_fontawesome_ngfactory["a" /* FaDuotoneIconComponentNgFactory */], ng_bootstrap_ngfactory["c" /* ɵwNgFactory */], ng_bootstrap_ngfactory["d" /* ɵxNgFactory */], inline_svg_component_ngfactory["a" /* InlineSVGComponentNgFactory */]]], [3, core["ComponentFactoryResolver"]], core["NgModuleRef"]]), core["ɵmpd"](4608, common["NgLocalization"], common["NgLocaleLocalization"], [core["LOCALE_ID"], [2, common["ɵangular_packages_common_common_a"]]]), core["ɵmpd"](4608, fesm2015_forms["d" /* FormBuilder */], fesm2015_forms["d" /* FormBuilder */], []), core["ɵmpd"](4608, fesm2015_forms["u" /* ɵangular_packages_forms_forms_o */], fesm2015_forms["u" /* ɵangular_packages_forms_forms_o */], []), core["ɵmpd"](4608, ng_bootstrap["t" /* NgbModal */], ng_bootstrap["t" /* NgbModal */], [core["ComponentFactoryResolver"], core["Injector"], ng_bootstrap["ib" /* ɵy */], ng_bootstrap["u" /* NgbModalConfig */]]), core["ɵmpd"](4608, ngxs_store["C" /* ɵr */], ngxs_store["C" /* ɵr */], [[3, ngxs_store["C" /* ɵr */]], [2, ngxs_store["d" /* NGXS_PLUGINS */]]]), core["ɵmpd"](4608, country_state["a" /* CountryState */], country_state["a" /* CountryState */], [countries_service["a" /* CountriesService */], ngxs_store["j" /* Store */]]), core["ɵmpd"](5120, AuthServiceConfig, provideConfig, []), core["ɵmpd"](4608, angularx_social_login_AuthService, angularx_social_login_AuthService, [AuthServiceConfig]), core["ɵmpd"](1073742336, router["r" /* RouterModule */], router["r" /* RouterModule */], [[2, router["x" /* ɵangular_packages_router_router_a */]], [2, router["o" /* Router */]]]), core["ɵmpd"](1073742336, AuthRoutingModule, AuthRoutingModule, []), core["ɵmpd"](1073742336, common["CommonModule"], common["CommonModule"], []), core["ɵmpd"](1073742336, fesm2015_forms["t" /* ɵangular_packages_forms_forms_d */], fesm2015_forms["t" /* ɵangular_packages_forms_forms_d */], []), core["ɵmpd"](1073742336, fesm2015_forms["q" /* ReactiveFormsModule */], fesm2015_forms["q" /* ReactiveFormsModule */], []), core["ɵmpd"](1073742336, angular_fontawesome["j" /* FontAwesomeModule */], angular_fontawesome["j" /* FontAwesomeModule */], []), core["ɵmpd"](1073742336, ng_select_ng_select["c" /* NgSelectModule */], ng_select_ng_select["c" /* NgSelectModule */], []), core["ɵmpd"](1073742336, ng_bootstrap["v" /* NgbModalModule */], ng_bootstrap["v" /* NgbModalModule */], []), core["ɵmpd"](1073742336, inline_svg_module["InlineSVGModule"], inline_svg_module["InlineSVGModule"], []), core["ɵmpd"](1073742336, ngx_infinite_scroll["b" /* InfiniteScrollModule */], ngx_infinite_scroll["b" /* InfiniteScrollModule */], []), core["ɵmpd"](1073742336, ng_bootstrap["k" /* NgbCarouselModule */], ng_bootstrap["k" /* NgbCarouselModule */], []), core["ɵmpd"](512, ngxs_store["x" /* ɵm */], ngxs_store["x" /* ɵm */], [core["Injector"], ngxs_store["u" /* ɵj */], [3, ngxs_store["x" /* ɵm */]], ngxs_store["p" /* ɵb */], ngxs_store["y" /* ɵn */], ngxs_store["A" /* ɵp */], [2, ngxs_store_internals["a" /* INITIAL_STATE_TOKEN */]]]), core["ɵmpd"](1024, ngxs_store["r" /* ɵf */], function () { return [[country_state["a" /* CountryState */]]]; }, []), core["ɵmpd"](1073742336, ngxs_store["I" /* ɵx */], ngxs_store["I" /* ɵx */], [ngxs_store["j" /* Store */], ngxs_store["B" /* ɵq */], ngxs_store["x" /* ɵm */], [2, ngxs_store["r" /* ɵf */]], ngxs_store["H" /* ɵw */]]), core["ɵmpd"](1073742336, shared_module["a" /* SharedModule */], shared_module["a" /* SharedModule */], []), core["ɵmpd"](1073742336, SocialLoginModule, SocialLoginModule, []), core["ɵmpd"](1073742336, AuthModule, AuthModule, []), core["ɵmpd"](1024, router["l" /* ROUTES */], function () { return [[{ path: "login", component: login_component_LoginComponent, canActivate: [public_guard_service_PublicGuard] }, { path: "signup", component: signup_component_SignupComponent, canActivate: [public_guard_service_PublicGuard] }, { path: "forgot-password", component: forgot_password_component_ForgotPasswordComponent, canActivate: [public_guard_service_PublicGuard] }, { path: "user/reset/:id/:id", component: reset_password_component_ResetPasswordComponent, canActivate: [public_guard_service_PublicGuard] }]]; }, []), core["ɵmpd"](256, ng_select_ng_select["d" /* SELECTION_MODEL_FACTORY */], ng_select_ng_select["e" /* ɵb */], [])]); });



/***/ }),

/***/ "xR19":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetAllCountries; });
class GetAllCountries {
}
GetAllCountries.type = '[Sign Up Page] GetAllCountries';


/***/ })

};;