exports.ids = [8];
exports.modules = {

/***/ "Ite/":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/tslib/tslib.js
var tslib = __webpack_require__("zOht");

// EXTERNAL MODULE: ./src/app/core/services/countries.service.ts
var countries_service = __webpack_require__("NtT5");

// CONCATENATED MODULE: ./src/app/core/models/country.model.ts
class CountryStateModel {
}

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/operators/tap.js
var tap = __webpack_require__("vkgz");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/operators/catchError.js
var catchError = __webpack_require__("JIr8");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/observable/throwError.js
var throwError = __webpack_require__("z6cu");

// EXTERNAL MODULE: ./node_modules/@ngxs/store/fesm2015/ngxs-store.js
var ngxs_store = __webpack_require__("e1JD");

// EXTERNAL MODULE: ./src/app/shared/store/country.action.ts
var country_action = __webpack_require__("xR19");

// CONCATENATED MODULE: ./src/app/shared/store/country.state.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return country_state_CountryState; });





// NGXS


let country_state_CountryState = class CountryState {
    constructor(countriesService, store) {
        this.countriesService = countriesService;
        this.store = store;
    }
    static getAllCountries(state) {
        return state.countries;
    }
    getAllCountries({ patchState }, {}) {
        return this.countriesService.getAll().pipe(Object(tap["a" /* tap */])((result) => {
            console.log(result);
            patchState({
                countries: result
            });
        }), Object(catchError["a" /* catchError */])(err => {
            return Object(throwError["a" /* throwError */])(err);
        }));
    }
};
tslib["__decorate"]([
    Object(ngxs_store["a" /* Action */])(country_action["a" /* GetAllCountries */]),
    tslib["__metadata"]("design:type", Function),
    tslib["__metadata"]("design:paramtypes", [Object, country_action["a" /* GetAllCountries */]]),
    tslib["__metadata"]("design:returntype", void 0)
], country_state_CountryState.prototype, "getAllCountries", null);
tslib["__decorate"]([
    Object(ngxs_store["g" /* Selector */])(),
    tslib["__metadata"]("design:type", Function),
    tslib["__metadata"]("design:paramtypes", [CountryStateModel]),
    tslib["__metadata"]("design:returntype", void 0)
], country_state_CountryState, "getAllCountries", null);
country_state_CountryState = tslib["__decorate"]([
    Object(ngxs_store["h" /* State */])({
        name: 'country',
        defaults: {
            countries: []
        }
    }),
    tslib["__metadata"]("design:paramtypes", [countries_service["a" /* CountriesService */], ngxs_store["j" /* Store */]])
], country_state_CountryState);



/***/ }),

/***/ "NtT5":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CountriesService; });
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("IheW");




class CountriesService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = `${_env_environment__WEBPACK_IMPORTED_MODULE_0__[/* environment */ "a"].url}/countries`;
    }
    getAll() {
        return this.httpClient.get(`${this.url}/all`);
    }
}
CountriesService.ngInjectableDef = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ factory: function CountriesService_Factory() { return new CountriesService(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); }, token: CountriesService, providedIn: "root" });


/***/ }),

/***/ "PCNd":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MODULES */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("SVse");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("iInd");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("s7LF");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("wTG2");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("G0yt");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("Nv++");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("419G");
/* harmony import */ var ng_inline_svg__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng_inline_svg__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("MNke");



// NG-Select

// NG-Bootstrap


// Font Awesome

// NG-Inline-SVG

// NGX Infinite Scroll

const MODULES = [
    _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
    _angular_router__WEBPACK_IMPORTED_MODULE_1__[/* RouterModule */ "r"],
    _angular_forms__WEBPACK_IMPORTED_MODULE_2__[/* ReactiveFormsModule */ "q"],
    _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__[/* FontAwesomeModule */ "j"],
    _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__[/* NgSelectModule */ "c"],
    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__[/* NgbModalModule */ "v"],
    ng_inline_svg__WEBPACK_IMPORTED_MODULE_6__["InlineSVGModule"],
    ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_7__[/* InfiniteScrollModule */ "b"],
    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__[/* NgbCarouselModule */ "k"]
];
class SharedModule {
}


/***/ }),

/***/ "UIoA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@angular/core/fesm2015/core.js
var core = __webpack_require__("8Y7J");

// CONCATENATED MODULE: ./src/app/profile/profile.module.ts
class ProfileModule {
}

// EXTERNAL MODULE: ./node_modules/@angular/router/router.ngfactory.js
var router_ngfactory = __webpack_require__("pMnS");

// CONCATENATED MODULE: ./src/app/profile/profile-view/profile-view.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var styles = [""];


// EXTERNAL MODULE: ./node_modules/@angular/common/fesm2015/common.js
var common = __webpack_require__("SVse");

// EXTERNAL MODULE: ./node_modules/@angular/router/fesm2015/router.js
var router = __webpack_require__("iInd");

// EXTERNAL MODULE: ./node_modules/@fortawesome/angular-fontawesome/angular-fontawesome.ngfactory.js
var angular_fontawesome_ngfactory = __webpack_require__("fNgX");

// EXTERNAL MODULE: ./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js
var angular_fontawesome = __webpack_require__("Nv++");

// EXTERNAL MODULE: ./node_modules/@angular/platform-browser/fesm2015/platform-browser.js
var platform_browser = __webpack_require__("cUpR");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/svg-cache.service.js
var svg_cache_service = __webpack_require__("YAkT");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.config.js
var inline_svg_config = __webpack_require__("SsaO");

// EXTERNAL MODULE: ./node_modules/@angular/common/fesm2015/http.js
var http = __webpack_require__("IheW");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.directive.js
var inline_svg_directive = __webpack_require__("Iv/j");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.service.js
var inline_svg_service = __webpack_require__("yLZu");

// EXTERNAL MODULE: ./node_modules/ngx-infinite-scroll/modules/ngx-infinite-scroll.js
var ngx_infinite_scroll = __webpack_require__("MNke");

// EXTERNAL MODULE: ./node_modules/tslib/tslib.js
var tslib = __webpack_require__("zOht");

// EXTERNAL MODULE: ./node_modules/rxjs/_esm2015/internal/Observable.js + 1 modules
var Observable = __webpack_require__("HDdC");

// EXTERNAL MODULE: ./src/environments/environment.ts
var environment = __webpack_require__("AytR");

// EXTERNAL MODULE: ./src/app/core/modals/story-modal/story-modal.component.ts
var story_modal_component = __webpack_require__("oPyH");

// EXTERNAL MODULE: ./node_modules/@ngxs/store/fesm2015/ngxs-store.js
var ngxs_store = __webpack_require__("e1JD");

// EXTERNAL MODULE: ./src/app/core/store/profile.state.ts + 1 modules
var profile_state = __webpack_require__("6PzV");

// EXTERNAL MODULE: ./src/app/core/store/profile.action.ts
var profile_action = __webpack_require__("1tnE");

// EXTERNAL MODULE: ./src/app/core/store/post.state.ts + 1 modules
var post_state = __webpack_require__("c145");

// EXTERNAL MODULE: ./src/app/core/store/post.action.ts
var post_action = __webpack_require__("nxNj");

// EXTERNAL MODULE: ./node_modules/@fortawesome/free-solid-svg-icons/index.js
var free_solid_svg_icons = __webpack_require__("YTkO");

// EXTERNAL MODULE: ./src/app/core/store/auth.state.ts + 1 modules
var auth_state = __webpack_require__("llJr");

// EXTERNAL MODULE: ./src/app/core/store/auth.action.ts
var auth_action = __webpack_require__("72RC");

// EXTERNAL MODULE: ./src/app/core/store/story.action.ts
var story_action = __webpack_require__("SmgI");

// CONCATENATED MODULE: ./src/app/profile/profile-view/profile-view.component.ts




// NGXS









// NG-Bootstrap

class profile_view_component_ProfileViewComponent {
    constructor(store, modalService) {
        this.store = store;
        this.modalService = modalService;
        this.currentPage = 1;
        this.provider = this.store.selectSnapshot(auth_state["a" /* AuthState */].getProvider);
        // Font Awesome
        this.faPen = free_solid_svg_icons["faPen"];
        this.lastPage$.subscribe(val => {
            this.lastPage = val;
        });
        this.store.dispatch(new profile_action["q" /* GetProfile */]());
        this.store.dispatch(new post_action["m" /* GetOwnPostsFirstPage */]('', '1'));
    }
    getImageUrl(url) {
        return `${environment["a" /* environment */].urlForImage}${url}`;
    }
    onScroll() {
        if (this.currentPage < this.lastPage) {
            this.currentPage += 1;
            this.store.dispatch(new post_action["n" /* GetOwnPostsPerPage */]('', this.currentPage.toString()));
        }
    }
    onLogoutOut() {
        this.store.dispatch(new auth_action["d" /* Logout */]());
    }
    withStory(stories) {
        if (stories) {
            return true;
        }
        else {
            return false;
        }
    }
    openStoryModal(stories) {
        this.store.dispatch(new story_action["c" /* SelectStory */](stories)).subscribe(() => {
            this.modalService.open(story_modal_component["a" /* StoryModalComponent */]);
        });
    }
}
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(post_state["a" /* PostState */].getLastPage),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_view_component_ProfileViewComponent.prototype, "lastPage$", void 0);
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(profile_state["a" /* ProfileState */].getProfile),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_view_component_ProfileViewComponent.prototype, "profile$", void 0);
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(post_state["a" /* PostState */].getOwnPost),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_view_component_ProfileViewComponent.prototype, "posts$", void 0);

// EXTERNAL MODULE: ./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js
var ng_bootstrap = __webpack_require__("G0yt");

// CONCATENATED MODULE: ./src/app/profile/profile-view/profile-view.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
















var styles_ProfileViewComponent = [styles];
var RenderType_ProfileViewComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_ProfileViewComponent, data: {} });

function View_ProfileViewComponent_2(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 3, "div", [["class", "photo"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.openStoryModal(_v.parent.context.ngIf) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵprd"](512, null, common["ɵNgClassImpl"], common["ɵNgClassR2Impl"], [core["IterableDiffers"], core["KeyValueDiffers"], core["ElementRef"], core["Renderer2"]]), core["ɵdid"](2, 278528, null, 0, common["NgClass"], [common["ɵNgClassImpl"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), (_l()(), core["ɵeld"](3, 0, null, null, 0, "img", [["alt", ""]], [[8, "src", 4]], null, null, null, null))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "photo"; var currVal_1 = (_co.withStory(((_v.parent.context.ngIf.has_stories.length > 0) ? _v.parent.context.ngIf.has_stories : null)) ? "with-story" : ""); _ck(_v, 2, 0, currVal_0, currVal_1); }, function (_ck, _v) { var _co = _v.component; var currVal_2 = (_v.parent.context.ngIf.image ? _co.getImageUrl(_v.parent.context.ngIf.image) : "assets/images/no-image.png"); _ck(_v, 3, 0, currVal_2); }); }
function View_ProfileViewComponent_3(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "photo"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 0, null, null, 0, "img", [["alt", ""]], [[8, "src", 4]], null, null, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = (_v.parent.context.ngIf.image ? _co.getImageUrl(_v.parent.context.ngIf.image) : "assets/images/no-image.png"); _ck(_v, 1, 0, currVal_0); }); }
function View_ProfileViewComponent_5(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 2, "img", [["class", "avatar"]], [[8, "src", 4]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 1).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](1, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](2, 2)], function (_ck, _v) { var currVal_1 = _ck(_v, 2, 0, "/post/", _v.parent.context.$implicit.id); _ck(_v, 1, 0, currVal_1); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.getImageUrl(_v.parent.context.$implicit.contents[0].content); _ck(_v, 0, 0, currVal_0); }); }
function View_ProfileViewComponent_6(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 4, "div", [["class", "video-overlay"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 1).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](1, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](2, 2), (_l()(), core["ɵeld"](3, 0, null, null, 1, "video", [], null, null, null, null, null)), (_l()(), core["ɵeld"](4, 0, null, null, 0, "source", [], [[8, "src", 4]], null, null, null, null))], function (_ck, _v) { var currVal_0 = _ck(_v, 2, 0, "/post/", _v.parent.context.$implicit.id); _ck(_v, 1, 0, currVal_0); }, function (_ck, _v) { var _co = _v.component; var currVal_1 = _co.getImageUrl(_v.parent.context.$implicit.contents[0].content); _ck(_v, 4, 0, currVal_1); }); }
function View_ProfileViewComponent_4(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 4, "div", [["class", "item"]], null, null, null, null, null)), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewComponent_5)), core["ɵdid"](2, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewComponent_6)), core["ɵdid"](4, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var currVal_0 = (_v.context.$implicit.contents[0].type !== "video"); _ck(_v, 2, 0, currVal_0); var currVal_1 = (_v.context.$implicit.contents[0].type === "video"); _ck(_v, 4, 0, currVal_1); }, null); }
function View_ProfileViewComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 62, "div", [["class", "profile-view"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 0, null, null, 57, "div", [["class", "top"]], null, null, null, null, null)), (_l()(), core["ɵeld"](2, 0, null, null, 9, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewComponent_2)), core["ɵdid"](4, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewComponent_3)), core["ɵdid"](6, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](7, 0, null, null, 4, "div", [["class", "text-center"]], null, null, null, null, null)), core["ɵprd"](512, null, common["ɵNgStyleImpl"], common["ɵNgStyleR2Impl"], [core["ElementRef"], core["KeyValueDiffers"], core["Renderer2"]]), core["ɵdid"](9, 278528, null, 0, common["NgStyle"], [common["ɵNgStyleImpl"]], { ngStyle: [0, "ngStyle"] }, null), core["ɵpod"](10, { "color": 0 }), (_l()(), core["ɵted"](11, null, [" ", " "])), (_l()(), core["ɵeld"](12, 0, null, null, 46, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](13, 0, null, null, 16, "div", [["class", "name"]], null, null, null, null, null)), (_l()(), core["ɵted"](14, null, [" ", " ", " "])), (_l()(), core["ɵeld"](15, 0, null, null, 4, "button", [["class", "edit-button"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 16).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](16, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](17, 1), (_l()(), core["ɵeld"](18, 0, null, null, 1, "fa-icon", [["class", "ng-fa-icon"]], [[1, "title", 0], [8, "innerHTML", 1]], null, null, angular_fontawesome_ngfactory["d" /* View_FaIconComponent_0 */], angular_fontawesome_ngfactory["c" /* RenderType_FaIconComponent */])), core["ɵdid"](19, 573440, null, 0, angular_fontawesome["c" /* FaIconComponent */], [platform_browser["b" /* DomSanitizer */], angular_fontawesome["a" /* FaConfig */], angular_fontawesome["d" /* FaIconLibrary */], [2, angular_fontawesome["i" /* FaStackItemSizeDirective */]]], { icon: [0, "icon"] }, null), (_l()(), core["ɵeld"](20, 0, null, null, 9, "div", [["class", "dropdown"]], null, null, null, null, null)), (_l()(), core["ɵeld"](21, 16777216, null, null, 2, "div", [["class", "settings-button"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = ((_co.dropdownLogout = !_co.dropdownLogout) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵprd"](512, null, svg_cache_service["SVGCacheService"], svg_cache_service["SVGCacheService"], [[2, common["APP_BASE_HREF"]], [2, common["PlatformLocation"]], [2, inline_svg_config["InlineSVGConfig"]], http["HttpBackend"], core["RendererFactory2"]]), core["ɵdid"](23, 737280, null, 0, inline_svg_directive["InlineSVGDirective"], [core["ElementRef"], core["ViewContainerRef"], core["ComponentFactoryResolver"], svg_cache_service["SVGCacheService"], core["Renderer2"], inline_svg_service["InlineSVGService"], [2, inline_svg_config["InlineSVGConfig"]], core["PLATFORM_ID"]], { inlineSVG: [0, "inlineSVG"] }, null), (_l()(), core["ɵeld"](24, 0, null, null, 5, "div", [["class", "dropdown-content"]], null, null, null, null, null)), core["ɵprd"](512, null, common["ɵNgClassImpl"], common["ɵNgClassR2Impl"], [core["IterableDiffers"], core["KeyValueDiffers"], core["ElementRef"], core["Renderer2"]]), core["ɵdid"](26, 278528, null, 0, common["NgClass"], [common["ɵNgClassImpl"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), core["ɵpod"](27, { "active": 0 }), (_l()(), core["ɵeld"](28, 0, null, null, 1, "div", [["class", "item"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.onLogoutOut() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), core["ɵted"](-1, null, [" Logout "])), (_l()(), core["ɵeld"](30, 0, null, null, 1, "div", [["class", "email"]], null, null, null, null, null)), (_l()(), core["ɵted"](31, null, ["", ""])), (_l()(), core["ɵeld"](32, 0, null, null, 24, "div", [["class", "stats"]], null, null, null, null, null)), (_l()(), core["ɵeld"](33, 0, null, null, 9, "div", [["class", "stats-title"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 34).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](34, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { queryParams: [0, "queryParams"], routerLink: [1, "routerLink"] }, null), core["ɵpod"](35, { type: 0 }), core["ɵpad"](36, 1), core["ɵprd"](512, null, common["ɵNgStyleImpl"], common["ɵNgStyleR2Impl"], [core["ElementRef"], core["KeyValueDiffers"], core["Renderer2"]]), core["ɵdid"](38, 278528, null, 0, common["NgStyle"], [common["ɵNgStyleImpl"]], { ngStyle: [0, "ngStyle"] }, null), core["ɵpod"](39, { "cursor": 0 }), (_l()(), core["ɵted"](-1, null, [" Followers "])), (_l()(), core["ɵeld"](41, 0, null, null, 1, "div", [["class", "stats-number"]], null, null, null, null, null)), (_l()(), core["ɵted"](42, null, ["", ""])), (_l()(), core["ɵeld"](43, 0, null, null, 9, "div", [["class", "stats-title"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 44).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](44, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { queryParams: [0, "queryParams"], routerLink: [1, "routerLink"] }, null), core["ɵpod"](45, { type: 0 }), core["ɵpad"](46, 1), core["ɵprd"](512, null, common["ɵNgStyleImpl"], common["ɵNgStyleR2Impl"], [core["ElementRef"], core["KeyValueDiffers"], core["Renderer2"]]), core["ɵdid"](48, 278528, null, 0, common["NgStyle"], [common["ɵNgStyleImpl"]], { ngStyle: [0, "ngStyle"] }, null), core["ɵpod"](49, { "cursor": 0 }), (_l()(), core["ɵted"](-1, null, [" Following "])), (_l()(), core["ɵeld"](51, 0, null, null, 1, "div", [["class", "stats-number"]], null, null, null, null, null)), (_l()(), core["ɵted"](52, null, ["", ""])), (_l()(), core["ɵeld"](53, 0, null, null, 3, "div", [["class", "stats-title"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Posts "])), (_l()(), core["ɵeld"](55, 0, null, null, 1, "div", [["class", "stats-number"]], null, null, null, null, null)), (_l()(), core["ɵted"](56, null, ["", ""])), (_l()(), core["ɵeld"](57, 0, null, null, 1, "div", [["class", "description"]], null, null, null, null, null)), (_l()(), core["ɵted"](58, null, [" ", " "])), (_l()(), core["ɵeld"](59, 0, null, null, 3, "div", [["class", "posts-collection mt3"]], null, null, null, null, null)), (_l()(), core["ɵand"](16777216, null, null, 2, null, View_ProfileViewComponent_4)), core["ɵdid"](61, 278528, null, 0, common["NgForOf"], [core["ViewContainerRef"], core["TemplateRef"], core["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]])], function (_ck, _v) { var _co = _v.component; var currVal_0 = (_v.context.ngIf.has_stories.length > 0); _ck(_v, 4, 0, currVal_0); var currVal_1 = (_v.context.ngIf.has_stories.length === 0); _ck(_v, 6, 0, currVal_1); var currVal_2 = _ck(_v, 10, 0, "#656565"); _ck(_v, 9, 0, currVal_2); var currVal_6 = _ck(_v, 17, 0, "edit"); _ck(_v, 16, 0, currVal_6); var currVal_9 = _co.faPen; _ck(_v, 19, 0, currVal_9); var currVal_10 = "assets/svg/settings.svg"; _ck(_v, 23, 0, currVal_10); var currVal_11 = "dropdown-content"; var currVal_12 = _ck(_v, 27, 0, _co.dropdownLogout); _ck(_v, 26, 0, currVal_11, currVal_12); var currVal_14 = _ck(_v, 35, 0, "followers"); var currVal_15 = _ck(_v, 36, 0, "follow"); _ck(_v, 34, 0, currVal_14, currVal_15); var currVal_16 = _ck(_v, 39, 0, "pointer"); _ck(_v, 38, 0, currVal_16); var currVal_18 = _ck(_v, 45, 0, "following"); var currVal_19 = _ck(_v, 46, 0, "follow"); _ck(_v, 44, 0, currVal_18, currVal_19); var currVal_20 = _ck(_v, 49, 0, "pointer"); _ck(_v, 48, 0, currVal_20); var currVal_24 = core["ɵunv"](_v, 61, 0, core["ɵnov"](_v, 62).transform(_co.posts$)); _ck(_v, 61, 0, currVal_24); }, function (_ck, _v) { var currVal_3 = ((_v.context.ngIf == null) ? null : ((_v.context.ngIf.country == null) ? null : _v.context.ngIf.country.country_name)); _ck(_v, 11, 0, currVal_3); var currVal_4 = _v.context.ngIf.fname; var currVal_5 = _v.context.ngIf.lname; _ck(_v, 14, 0, currVal_4, currVal_5); var currVal_7 = core["ɵnov"](_v, 19).title; var currVal_8 = core["ɵnov"](_v, 19).renderedIconHTML; _ck(_v, 18, 0, currVal_7, currVal_8); var currVal_13 = _v.context.ngIf.email; _ck(_v, 31, 0, currVal_13); var currVal_17 = _v.context.ngIf.followers_count; _ck(_v, 42, 0, currVal_17); var currVal_21 = _v.context.ngIf.following_count; _ck(_v, 52, 0, currVal_21); var currVal_22 = _v.context.ngIf.num_post; _ck(_v, 56, 0, currVal_22); var currVal_23 = _v.context.ngIf.description; _ck(_v, 58, 0, currVal_23); }); }
function View_ProfileViewComponent_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 4, "div", [["infiniteScroll", ""]], null, [[null, "scrolled"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("scrolled" === en)) {
        var pd_0 = (_co.onScroll() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](1, 4866048, null, 0, ngx_infinite_scroll["a" /* InfiniteScrollDirective */], [core["ElementRef"], core["NgZone"]], { infiniteScrollDistance: [0, "infiniteScrollDistance"], infiniteScrollThrottle: [1, "infiniteScrollThrottle"] }, { scrolled: "scrolled" }), (_l()(), core["ɵand"](16777216, null, null, 2, null, View_ProfileViewComponent_1)), core["ɵdid"](3, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]])], function (_ck, _v) { var _co = _v.component; var currVal_0 = 0.2; var currVal_1 = 300; _ck(_v, 1, 0, currVal_0, currVal_1); var currVal_2 = core["ɵunv"](_v, 3, 0, core["ɵnov"](_v, 4).transform(_co.profile$)); _ck(_v, 3, 0, currVal_2); }, null); }
function View_ProfileViewComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-profile-view", [], null, null, null, View_ProfileViewComponent_0, RenderType_ProfileViewComponent)), core["ɵdid"](1, 49152, null, 0, profile_view_component_ProfileViewComponent, [ngxs_store["j" /* Store */], ng_bootstrap["t" /* NgbModal */]], null, null)], null, null); }
var ProfileViewComponentNgFactory = core["ɵccf"]("app-profile-view", profile_view_component_ProfileViewComponent, View_ProfileViewComponent_Host_0, {}, {}, []);


// CONCATENATED MODULE: ./src/app/profile/profile-edit/profile-edit.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var profile_edit_component_scss_shim_ngstyle_styles = [""];


// EXTERNAL MODULE: ./node_modules/@angular/forms/fesm2015/forms.js
var fesm2015_forms = __webpack_require__("s7LF");

// EXTERNAL MODULE: ./node_modules/@ng-select/ng-select/ng-select-ng-select.ngfactory.js
var ng_select_ng_select_ngfactory = __webpack_require__("MJJn");

// EXTERNAL MODULE: ./node_modules/@ng-select/ng-select/fesm2015/ng-select-ng-select.js
var ng_select_ng_select = __webpack_require__("wTG2");

// CONCATENATED MODULE: ./src/app/shared/reusable-custom-validators/set-as-touched.validator.ts

function setAsTouched(group) {
    group.markAsTouched();
    for (const i in group.controls) {
        if (group.controls[i] instanceof fesm2015_forms["e" /* FormControl */]) {
            group.controls[i].markAsTouched();
        }
        else {
            this.setAsTouched(group.controls[i]);
        }
    }
}

// EXTERNAL MODULE: ./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js
var ngxs_router_plugin = __webpack_require__("AUKP");

// EXTERNAL MODULE: ./src/app/shared/store/country.action.ts
var country_action = __webpack_require__("xR19");

// EXTERNAL MODULE: ./src/app/shared/store/country.state.ts + 1 modules
var country_state = __webpack_require__("Ite/");

// CONCATENATED MODULE: ./src/app/profile/profile-edit/profile-edit.component.ts





// NGXS








class profile_edit_component_ProfileEditComponent {
    constructor(store, formBuilder) {
        this.store = store;
        this.formBuilder = formBuilder;
        this.submitButtonStatus = true;
        this.formSubmitAttempt = false;
        this.form = this.formBuilder.group({
            fname: [null, fesm2015_forms["s" /* Validators */].required],
            lname: [null, fesm2015_forms["s" /* Validators */].required],
            email: [null, fesm2015_forms["s" /* Validators */].required],
            username: [null, fesm2015_forms["s" /* Validators */].required],
            country_id: [null, fesm2015_forms["s" /* Validators */].required],
            description: [null, fesm2015_forms["s" /* Validators */].required],
            image: [null]
        });
        this.store.dispatch(new country_action["a" /* GetAllCountries */]());
        this.store.dispatch(new profile_action["q" /* GetProfile */]()).subscribe(val => {
            console.log(val.profile.profile);
            setAsTouched(this.form);
            // this.form.patchValue(val.profile.profile);
            this.form.get('fname').setValue(val.profile.profile.fname);
            this.form.get('lname').setValue(val.profile.profile.lname);
            this.form.get('email').setValue(val.profile.profile.email);
            this.form.get('country_id').setValue(val.profile.profile.country_id);
            this.form.get('username').setValue(val.profile.profile.username);
            this.form.get('description').setValue(val.profile.profile.description);
            this.username = val.profile.profile.username;
        });
    }
    getUrl(url) {
        return `${environment["a" /* environment */].urlForImage}${url}`;
    }
    onSelectImage(event) {
        this.store.dispatch(new post_action["x" /* PreviewImageVideo */](event, this.url));
        this.selectedImage = event.target.files[0];
    }
    setSubmitButton(flag) {
        this.submitButtonStatus = flag;
    }
    getSubmitButton() {
        return this.submitButtonStatus;
    }
    onEditProfile(form) {
        console.log(form.value);
        const formData = new FormData();
        formData.append('fname', form.value.fname);
        formData.append('lname', form.value.lname);
        formData.append('country_id', form.value.country_id);
        if (this.username !== form.value.username) {
            formData.append('username', form.value.username);
        }
        formData.append('description', form.value.description);
        if (this.selectedImage) {
            formData.append('image', this.selectedImage);
        }
        console.log(formData);
        this.formSubmitAttempt = true;
        if (form.valid) {
            this.setSubmitButton(false);
            this.formSubmitAttempt = false;
            console.log('yeah');
            this.store.dispatch(new profile_action["b" /* EditProfile */](formData)).subscribe(() => {
                this.setSubmitButton(true);
                this.store.dispatch(new ngxs_router_plugin["b" /* Navigate */](['/profile']));
            }, err => {
                console.log(err);
                this.setSubmitButton(true);
            });
        }
    }
}
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(profile_state["a" /* ProfileState */].getProfile),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_edit_component_ProfileEditComponent.prototype, "profile$", void 0);
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(post_state["a" /* PostState */].getFileUrl),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_edit_component_ProfileEditComponent.prototype, "url$", void 0);
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(country_state["a" /* CountryState */].getAllCountries),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_edit_component_ProfileEditComponent.prototype, "countries$", void 0);

// CONCATENATED MODULE: ./src/app/profile/profile-edit/profile-edit.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 








var styles_ProfileEditComponent = [profile_edit_component_scss_shim_ngstyle_styles];
var RenderType_ProfileEditComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_ProfileEditComponent, data: {} });

function View_ProfileEditComponent_2(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 2, "img", [["class", "image"], ["id", "profile"]], [[8, "src", 4]], null, null, null, null)), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]]), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]])], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = (core["ɵunv"](_v, 0, 0, core["ɵnov"](_v, 1).transform(_co.url$)) ? core["ɵunv"](_v, 0, 0, core["ɵnov"](_v, 2).transform(_co.url$)) : (_v.parent.context.ngIf.image ? _co.getUrl(_v.parent.context.ngIf.image) : "assets/images/no-image.png")); _ck(_v, 0, 0, currVal_0); }); }
function View_ProfileEditComponent_3(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_ProfileEditComponent_4(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_ProfileEditComponent_5(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_ProfileEditComponent_6(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_ProfileEditComponent_7(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "alert-danger"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" This field is required! "]))], null, null); }
function View_ProfileEditComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 108, "div", [["class", "profile-edit"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 0, null, null, 1, "div", [["class", "title"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Edit Profile"])), (_l()(), core["ɵeld"](3, 0, null, null, 11, "label", [["class", "file"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; if (("submit" === en)) {
        var pd_0 = (core["ɵnov"](_v, 4).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (core["ɵnov"](_v, 4).onReset() !== false);
        ad = (pd_1 && ad);
    } return ad; }, null, null)), core["ɵdid"](4, 540672, null, 0, fesm2015_forms["g" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, null), core["ɵprd"](2048, null, fesm2015_forms["b" /* ControlContainer */], null, [fesm2015_forms["g" /* FormGroupDirective */]]), core["ɵdid"](6, 16384, null, 0, fesm2015_forms["m" /* NgControlStatusGroup */], [[4, fesm2015_forms["b" /* ControlContainer */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileEditComponent_2)), core["ɵdid"](8, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](9, 0, null, null, 5, "input", [["accept", "image/*"], ["formControlName", "image"], ["type", "file"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "change"], [null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 10)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 10).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 10)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 10)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } if (("change" === en)) {
        var pd_4 = (_co.onSelectImage($event) !== false);
        ad = (pd_4 && ad);
    } return ad; }, null, null)), core["ɵdid"](10, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](12, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](14, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵeld"](15, 0, null, null, 93, "form", [["class", "form"], ["novalidate", ""]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "ngSubmit"], [null, "submit"], [null, "reset"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("submit" === en)) {
        var pd_0 = (core["ɵnov"](_v, 17).onSubmit($event) !== false);
        ad = (pd_0 && ad);
    } if (("reset" === en)) {
        var pd_1 = (core["ɵnov"](_v, 17).onReset() !== false);
        ad = (pd_1 && ad);
    } if (("ngSubmit" === en)) {
        var pd_2 = (_co.onEditProfile(_co.form) !== false);
        ad = (pd_2 && ad);
    } return ad; }, null, null)), core["ɵdid"](16, 16384, null, 0, fesm2015_forms["x" /* ɵangular_packages_forms_forms_z */], [], null, null), core["ɵdid"](17, 540672, null, 0, fesm2015_forms["g" /* FormGroupDirective */], [[8, null], [8, null]], { form: [0, "form"] }, { ngSubmit: "ngSubmit" }), core["ɵprd"](2048, null, fesm2015_forms["b" /* ControlContainer */], null, [fesm2015_forms["g" /* FormGroupDirective */]]), core["ɵdid"](19, 16384, null, 0, fesm2015_forms["m" /* NgControlStatusGroup */], [[4, fesm2015_forms["b" /* ControlContainer */]]], null, null), (_l()(), core["ɵeld"](20, 0, null, null, 88, "div", [["class", "grid wrap"]], null, null, null, null, null)), (_l()(), core["ɵeld"](21, 0, null, null, 2, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵeld"](22, 0, null, null, 1, "label", [["class", "label"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["First Name"])), (_l()(), core["ɵeld"](24, 0, null, null, 8, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](25, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "fname"], ["placeholder", "First Name"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 26)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 26).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 26)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 26)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](26, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](28, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](30, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileEditComponent_3)), core["ɵdid"](32, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](33, 0, null, null, 2, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵeld"](34, 0, null, null, 1, "label", [["class", "label"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Last Name"])), (_l()(), core["ɵeld"](36, 0, null, null, 8, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](37, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "lname"], ["placeholder", "Last Name"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 38)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 38).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 38)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 38)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](38, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](40, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](42, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileEditComponent_4)), core["ɵdid"](44, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](45, 0, null, null, 2, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵeld"](46, 0, null, null, 1, "label", [["class", "label"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Email address"])), (_l()(), core["ɵeld"](48, 0, null, null, 6, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](49, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "email"], ["placeholder", "Email"], ["readonly", ""], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 50)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 50).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 50)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 50)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](50, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](52, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](54, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵeld"](55, 0, null, null, 2, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵeld"](56, 0, null, null, 1, "label", [["class", "label"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Username"])), (_l()(), core["ɵeld"](58, 0, null, null, 8, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](59, 0, null, null, 5, "input", [["class", "input"], ["formControlName", "username"], ["placeholder", "Username"], ["type", "text"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 60)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 60).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 60)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 60)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](60, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](62, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](64, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileEditComponent_5)), core["ɵdid"](66, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](67, 0, null, null, 2, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵeld"](68, 0, null, null, 1, "label", [["class", "label"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["Country"])), (_l()(), core["ɵeld"](70, 0, null, null, 22, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](71, 0, null, null, 19, "ng-select", [["bindLabel", "country_name"], ["bindValue", "id"], ["class", "input custom ng-select"], ["formControlName", "country_id"], ["placeholder", "-- Select Country --"], ["role", "listbox"]], [[2, "ng-select-single", null], [2, "ng-select-typeahead", null], [2, "ng-select-multiple", null], [2, "ng-select-taggable", null], [2, "ng-select-searchable", null], [2, "ng-select-clearable", null], [2, "ng-select-opened", null], [2, "ng-select-disabled", null], [2, "ng-select-filtered", null], [2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "keydown"]], function (_v, en, $event) { var ad = true; if (("keydown" === en)) {
        var pd_0 = (core["ɵnov"](_v, 73).handleKeyDown($event) !== false);
        ad = (pd_0 && ad);
    } return ad; }, ng_select_ng_select_ngfactory["b" /* View_NgSelectComponent_0 */], ng_select_ng_select_ngfactory["a" /* RenderType_NgSelectComponent */])), core["ɵprd"](4608, null, ng_select_ng_select["f" /* ɵd */], ng_select_ng_select["f" /* ɵd */], []), core["ɵdid"](73, 4964352, null, 12, ng_select_ng_select["a" /* NgSelectComponent */], [[8, "input custom"], [8, null], ng_select_ng_select["b" /* NgSelectConfig */], ng_select_ng_select["d" /* SELECTION_MODEL_FACTORY */], core["ElementRef"], core["ChangeDetectorRef"], ng_select_ng_select["j" /* ɵs */]], { bindLabel: [0, "bindLabel"], bindValue: [1, "bindValue"], placeholder: [2, "placeholder"], items: [3, "items"] }, null), core["ɵqud"](335544320, 1, { optionTemplate: 0 }), core["ɵqud"](335544320, 2, { optgroupTemplate: 0 }), core["ɵqud"](335544320, 3, { labelTemplate: 0 }), core["ɵqud"](335544320, 4, { multiLabelTemplate: 0 }), core["ɵqud"](603979776, 5, { headerTemplate: 0 }), core["ɵqud"](603979776, 6, { footerTemplate: 0 }), core["ɵqud"](335544320, 7, { notFoundTemplate: 0 }), core["ɵqud"](335544320, 8, { typeToSearchTemplate: 0 }), core["ɵqud"](335544320, 9, { loadingTextTemplate: 0 }), core["ɵqud"](335544320, 10, { tagTemplate: 0 }), core["ɵqud"](335544320, 11, { loadingSpinnerTemplate: 0 }), core["ɵqud"](603979776, 12, { ngOptions: 1 }), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]]), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [ng_select_ng_select["a" /* NgSelectComponent */]]), core["ɵdid"](88, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](90, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileEditComponent_6)), core["ɵdid"](92, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](93, 0, null, null, 2, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵeld"](94, 0, null, null, 1, "label", [["class", "label"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, ["About you"])), (_l()(), core["ɵeld"](96, 0, null, null, 8, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](97, 0, null, null, 5, "textarea", [["class", "input"], ["formControlName", "description"], ["rows", "4"]], [[2, "ng-untouched", null], [2, "ng-touched", null], [2, "ng-pristine", null], [2, "ng-dirty", null], [2, "ng-valid", null], [2, "ng-invalid", null], [2, "ng-pending", null]], [[null, "input"], [null, "blur"], [null, "compositionstart"], [null, "compositionend"]], function (_v, en, $event) { var ad = true; if (("input" === en)) {
        var pd_0 = (core["ɵnov"](_v, 98)._handleInput($event.target.value) !== false);
        ad = (pd_0 && ad);
    } if (("blur" === en)) {
        var pd_1 = (core["ɵnov"](_v, 98).onTouched() !== false);
        ad = (pd_1 && ad);
    } if (("compositionstart" === en)) {
        var pd_2 = (core["ɵnov"](_v, 98)._compositionStart() !== false);
        ad = (pd_2 && ad);
    } if (("compositionend" === en)) {
        var pd_3 = (core["ɵnov"](_v, 98)._compositionEnd($event.target.value) !== false);
        ad = (pd_3 && ad);
    } return ad; }, null, null)), core["ɵdid"](98, 16384, null, 0, fesm2015_forms["c" /* DefaultValueAccessor */], [core["Renderer2"], core["ElementRef"], [2, fesm2015_forms["a" /* COMPOSITION_BUFFER_MODE */]]], null, null), core["ɵprd"](1024, null, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */], function (p0_0) { return [p0_0]; }, [fesm2015_forms["c" /* DefaultValueAccessor */]]), core["ɵdid"](100, 671744, null, 0, fesm2015_forms["f" /* FormControlName */], [[3, fesm2015_forms["b" /* ControlContainer */]], [8, null], [8, null], [6, fesm2015_forms["j" /* NG_VALUE_ACCESSOR */]], [2, fesm2015_forms["v" /* ɵangular_packages_forms_forms_q */]]], { name: [0, "name"] }, null), core["ɵprd"](2048, null, fesm2015_forms["k" /* NgControl */], null, [fesm2015_forms["f" /* FormControlName */]]), core["ɵdid"](102, 16384, null, 0, fesm2015_forms["l" /* NgControlStatus */], [[4, fesm2015_forms["k" /* NgControl */]]], null, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileEditComponent_7)), core["ɵdid"](104, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](105, 0, null, null, 0, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵeld"](106, 0, null, null, 2, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](107, 0, null, null, 1, "button", [["class", "primary button"], ["type", "submit"]], [[8, "disabled", 0]], null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Edit "]))], function (_ck, _v) { var _co = _v.component; var currVal_7 = _co.form; _ck(_v, 4, 0, currVal_7); var currVal_8 = _co.profile$; _ck(_v, 8, 0, currVal_8); var currVal_16 = "image"; _ck(_v, 12, 0, currVal_16); var currVal_24 = _co.form; _ck(_v, 17, 0, currVal_24); var currVal_32 = "fname"; _ck(_v, 28, 0, currVal_32); var currVal_33 = ((!_co.form.get("fname").valid && _co.form.get("fname").touched) || (_co.form.get("fname").untouched && _co.formSubmitAttempt)); _ck(_v, 32, 0, currVal_33); var currVal_41 = "lname"; _ck(_v, 40, 0, currVal_41); var currVal_42 = ((!_co.form.get("lname").valid && _co.form.get("lname").touched) || (_co.form.get("lname").untouched && _co.formSubmitAttempt)); _ck(_v, 44, 0, currVal_42); var currVal_50 = "email"; _ck(_v, 52, 0, currVal_50); var currVal_58 = "username"; _ck(_v, 62, 0, currVal_58); var currVal_59 = ((!_co.form.get("username").valid && _co.form.get("username").touched) || (_co.form.get("username").untouched && _co.formSubmitAttempt)); _ck(_v, 66, 0, currVal_59); var currVal_76 = "country_name"; var currVal_77 = "id"; var currVal_78 = "-- Select Country --"; var currVal_79 = core["ɵunv"](_v, 73, 3, core["ɵnov"](_v, 86).transform(_co.countries$)); _ck(_v, 73, 0, currVal_76, currVal_77, currVal_78, currVal_79); var currVal_80 = "country_id"; _ck(_v, 88, 0, currVal_80); var currVal_81 = ((!_co.form.get("country_id").valid && _co.form.get("country_id").touched) || (_co.form.get("country_id").untouched && _co.formSubmitAttempt)); _ck(_v, 92, 0, currVal_81); var currVal_89 = "description"; _ck(_v, 100, 0, currVal_89); var currVal_90 = ((!_co.form.get("description").valid && _co.form.get("description").touched) || (_co.form.get("description").untouched && _co.formSubmitAttempt)); _ck(_v, 104, 0, currVal_90); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = core["ɵnov"](_v, 6).ngClassUntouched; var currVal_1 = core["ɵnov"](_v, 6).ngClassTouched; var currVal_2 = core["ɵnov"](_v, 6).ngClassPristine; var currVal_3 = core["ɵnov"](_v, 6).ngClassDirty; var currVal_4 = core["ɵnov"](_v, 6).ngClassValid; var currVal_5 = core["ɵnov"](_v, 6).ngClassInvalid; var currVal_6 = core["ɵnov"](_v, 6).ngClassPending; _ck(_v, 3, 0, currVal_0, currVal_1, currVal_2, currVal_3, currVal_4, currVal_5, currVal_6); var currVal_9 = core["ɵnov"](_v, 14).ngClassUntouched; var currVal_10 = core["ɵnov"](_v, 14).ngClassTouched; var currVal_11 = core["ɵnov"](_v, 14).ngClassPristine; var currVal_12 = core["ɵnov"](_v, 14).ngClassDirty; var currVal_13 = core["ɵnov"](_v, 14).ngClassValid; var currVal_14 = core["ɵnov"](_v, 14).ngClassInvalid; var currVal_15 = core["ɵnov"](_v, 14).ngClassPending; _ck(_v, 9, 0, currVal_9, currVal_10, currVal_11, currVal_12, currVal_13, currVal_14, currVal_15); var currVal_17 = core["ɵnov"](_v, 19).ngClassUntouched; var currVal_18 = core["ɵnov"](_v, 19).ngClassTouched; var currVal_19 = core["ɵnov"](_v, 19).ngClassPristine; var currVal_20 = core["ɵnov"](_v, 19).ngClassDirty; var currVal_21 = core["ɵnov"](_v, 19).ngClassValid; var currVal_22 = core["ɵnov"](_v, 19).ngClassInvalid; var currVal_23 = core["ɵnov"](_v, 19).ngClassPending; _ck(_v, 15, 0, currVal_17, currVal_18, currVal_19, currVal_20, currVal_21, currVal_22, currVal_23); var currVal_25 = core["ɵnov"](_v, 30).ngClassUntouched; var currVal_26 = core["ɵnov"](_v, 30).ngClassTouched; var currVal_27 = core["ɵnov"](_v, 30).ngClassPristine; var currVal_28 = core["ɵnov"](_v, 30).ngClassDirty; var currVal_29 = core["ɵnov"](_v, 30).ngClassValid; var currVal_30 = core["ɵnov"](_v, 30).ngClassInvalid; var currVal_31 = core["ɵnov"](_v, 30).ngClassPending; _ck(_v, 25, 0, currVal_25, currVal_26, currVal_27, currVal_28, currVal_29, currVal_30, currVal_31); var currVal_34 = core["ɵnov"](_v, 42).ngClassUntouched; var currVal_35 = core["ɵnov"](_v, 42).ngClassTouched; var currVal_36 = core["ɵnov"](_v, 42).ngClassPristine; var currVal_37 = core["ɵnov"](_v, 42).ngClassDirty; var currVal_38 = core["ɵnov"](_v, 42).ngClassValid; var currVal_39 = core["ɵnov"](_v, 42).ngClassInvalid; var currVal_40 = core["ɵnov"](_v, 42).ngClassPending; _ck(_v, 37, 0, currVal_34, currVal_35, currVal_36, currVal_37, currVal_38, currVal_39, currVal_40); var currVal_43 = core["ɵnov"](_v, 54).ngClassUntouched; var currVal_44 = core["ɵnov"](_v, 54).ngClassTouched; var currVal_45 = core["ɵnov"](_v, 54).ngClassPristine; var currVal_46 = core["ɵnov"](_v, 54).ngClassDirty; var currVal_47 = core["ɵnov"](_v, 54).ngClassValid; var currVal_48 = core["ɵnov"](_v, 54).ngClassInvalid; var currVal_49 = core["ɵnov"](_v, 54).ngClassPending; _ck(_v, 49, 0, currVal_43, currVal_44, currVal_45, currVal_46, currVal_47, currVal_48, currVal_49); var currVal_51 = core["ɵnov"](_v, 64).ngClassUntouched; var currVal_52 = core["ɵnov"](_v, 64).ngClassTouched; var currVal_53 = core["ɵnov"](_v, 64).ngClassPristine; var currVal_54 = core["ɵnov"](_v, 64).ngClassDirty; var currVal_55 = core["ɵnov"](_v, 64).ngClassValid; var currVal_56 = core["ɵnov"](_v, 64).ngClassInvalid; var currVal_57 = core["ɵnov"](_v, 64).ngClassPending; _ck(_v, 59, 0, currVal_51, currVal_52, currVal_53, currVal_54, currVal_55, currVal_56, currVal_57); var currVal_60 = !core["ɵnov"](_v, 73).multiple; var currVal_61 = core["ɵnov"](_v, 73).typeahead; var currVal_62 = core["ɵnov"](_v, 73).multiple; var currVal_63 = core["ɵnov"](_v, 73).addTag; var currVal_64 = core["ɵnov"](_v, 73).searchable; var currVal_65 = core["ɵnov"](_v, 73).clearable; var currVal_66 = core["ɵnov"](_v, 73).isOpen; var currVal_67 = core["ɵnov"](_v, 73).disabled; var currVal_68 = core["ɵnov"](_v, 73).filtered; var currVal_69 = core["ɵnov"](_v, 90).ngClassUntouched; var currVal_70 = core["ɵnov"](_v, 90).ngClassTouched; var currVal_71 = core["ɵnov"](_v, 90).ngClassPristine; var currVal_72 = core["ɵnov"](_v, 90).ngClassDirty; var currVal_73 = core["ɵnov"](_v, 90).ngClassValid; var currVal_74 = core["ɵnov"](_v, 90).ngClassInvalid; var currVal_75 = core["ɵnov"](_v, 90).ngClassPending; _ck(_v, 71, 1, [currVal_60, currVal_61, currVal_62, currVal_63, currVal_64, currVal_65, currVal_66, currVal_67, currVal_68, currVal_69, currVal_70, currVal_71, currVal_72, currVal_73, currVal_74, currVal_75]); var currVal_82 = core["ɵnov"](_v, 102).ngClassUntouched; var currVal_83 = core["ɵnov"](_v, 102).ngClassTouched; var currVal_84 = core["ɵnov"](_v, 102).ngClassPristine; var currVal_85 = core["ɵnov"](_v, 102).ngClassDirty; var currVal_86 = core["ɵnov"](_v, 102).ngClassValid; var currVal_87 = core["ɵnov"](_v, 102).ngClassInvalid; var currVal_88 = core["ɵnov"](_v, 102).ngClassPending; _ck(_v, 97, 0, currVal_82, currVal_83, currVal_84, currVal_85, currVal_86, currVal_87, currVal_88); var currVal_91 = !_co.getSubmitButton(); _ck(_v, 107, 0, currVal_91); }); }
function View_ProfileEditComponent_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵand"](16777216, null, null, 2, null, View_ProfileEditComponent_1)), core["ɵdid"](1, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]])], function (_ck, _v) { var _co = _v.component; var currVal_0 = core["ɵunv"](_v, 1, 0, core["ɵnov"](_v, 2).transform(_co.profile$)); _ck(_v, 1, 0, currVal_0); }, null); }
function View_ProfileEditComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-profile-edit", [], null, null, null, View_ProfileEditComponent_0, RenderType_ProfileEditComponent)), core["ɵdid"](1, 49152, null, 0, profile_edit_component_ProfileEditComponent, [ngxs_store["j" /* Store */], fesm2015_forms["d" /* FormBuilder */]], null, null)], null, null); }
var ProfileEditComponentNgFactory = core["ɵccf"]("app-profile-edit", profile_edit_component_ProfileEditComponent, View_ProfileEditComponent_Host_0, {}, {}, []);


// CONCATENATED MODULE: ./src/app/profile/profile-follow/profile-follow.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var profile_follow_component_scss_shim_ngstyle_styles = [""];


// CONCATENATED MODULE: ./src/app/profile/profile-follow/profile-follow.component.ts





// NGXS






// NG-Bootstrap

class profile_follow_component_ProfileFollowComponent {
    constructor(route, store, modalService) {
        this.route = route;
        this.store = store;
        this.modalService = modalService;
        this.currentPage = 1;
        this.userId = this.store.selectSnapshot(auth_state["a" /* AuthState */].getUserId);
        this.id = this.route.snapshot.queryParams['id'];
        this.type = this.route.snapshot.queryParams['type'];
        if (this.id) {
            if (this.type === 'followers') {
                this.store.dispatch(new profile_action["h" /* GetOtherFollowersFirstPage */]('', '1', this.id)).subscribe(() => { });
            }
            else if (this.type === 'following') {
                this.store.dispatch(new profile_action["j" /* GetOtherFollowingsFirstPage */]('', '1', this.id)).subscribe(() => { });
            }
        }
        else {
            if (this.type === 'followers') {
                this.store.dispatch(new profile_action["m" /* GetOwnFollowersFirstPage */]('', '1'));
            }
            else if (this.type === 'following') {
                this.store.dispatch(new profile_action["o" /* GetOwnFollowingsFirstPage */]('', '1'));
            }
        }
    }
    getImageUrl(url) {
        return `${environment["a" /* environment */].urlForImage}${url}`;
    }
    onScroll() {
        if (this.currentPage < this.lastPage) {
            this.currentPage += 1;
            if (this.id) {
                if (this.type === 'followers') {
                    this.store
                        .dispatch(new profile_action["i" /* GetOtherFollowersPerPage */]('', this.currentPage.toString(), this.id))
                        .subscribe(() => { });
                }
                else if (this.type === 'following') {
                    this.store
                        .dispatch(new profile_action["k" /* GetOtherFollowingsPerPage */]('', this.currentPage.toString(), this.id))
                        .subscribe(() => { });
                }
            }
            else {
                if (this.type === 'followers') {
                    this.store.dispatch(new profile_action["n" /* GetOwnFollowersPerPage */]('', this.currentPage.toString()));
                }
                else if (this.type === 'following') {
                    this.store.dispatch(new profile_action["p" /* GetOwnFollowingsPerPage */]('', this.currentPage.toString()));
                }
            }
        }
    }
    onFollow(profile) {
        this.store.dispatch(new profile_action["c" /* FollowUser */](profile)).subscribe(() => {
            // this.store.dispatch(new GetOtherProfile(this.id));
        });
    }
    withStory(stories) {
        if (stories) {
            return true;
        }
        else {
            return false;
        }
    }
    openStoryModal(stories) {
        this.store.dispatch(new story_action["c" /* SelectStory */](stories)).subscribe(() => {
            this.modalService.open(story_modal_component["a" /* StoryModalComponent */]);
        });
    }
    navigateBasedOnStoriesAndUserId(hasStories, userId) {
        hasStories.length > 0
            ? []
            : userId === this.userId
                ? this.store.dispatch(new ngxs_router_plugin["b" /* Navigate */](['/profile']))
                : this.store.dispatch(new ngxs_router_plugin["b" /* Navigate */](['/profile/' + userId]));
    }
    navigateBasedOnUserId(userId) {
        userId === this.userId
            ? this.store.dispatch(new ngxs_router_plugin["b" /* Navigate */](['/profile']))
            : this.store.dispatch(new ngxs_router_plugin["b" /* Navigate */](['/profile/' + userId]));
    }
}
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(profile_state["a" /* ProfileState */].getFollow),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_follow_component_ProfileFollowComponent.prototype, "follows$", void 0);

// CONCATENATED MODULE: ./src/app/profile/profile-follow/profile-follow.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 








var styles_ProfileFollowComponent = [profile_follow_component_scss_shim_ngstyle_styles];
var RenderType_ProfileFollowComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_ProfileFollowComponent, data: {} });

function View_ProfileFollowComponent_2(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "primary button"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.onFollow(_v.parent.context.$implicit) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), core["ɵted"](1, null, [" ", " "]))], null, function (_ck, _v) { var currVal_0 = ((_v.parent.context.$implicit.is_followed == 0) ? "Follow" : "Unfollow"); _ck(_v, 1, 0, currVal_0); }); }
function View_ProfileFollowComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 8, "div", [["class", "item"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 0, null, null, 3, "div", [["class", "follow-thumbnail"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.navigateBasedOnStoriesAndUserId(_v.context.$implicit.has_stories, _v.context.$implicit.id) !== false);
        ad = (pd_0 && ad);
    } if (("click" === en)) {
        var pd_1 = (((_v.context.$implicit.has_stories.length > 0) ? _co.openStoryModal(_v.context.$implicit) : "") !== false);
        ad = (pd_1 && ad);
    } return ad; }, null, null)), core["ɵprd"](512, null, common["ɵNgClassImpl"], common["ɵNgClassR2Impl"], [core["IterableDiffers"], core["KeyValueDiffers"], core["ElementRef"], core["Renderer2"]]), core["ɵdid"](3, 278528, null, 0, common["NgClass"], [common["ɵNgClassImpl"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), (_l()(), core["ɵeld"](4, 0, null, null, 0, "img", [["alt", ""]], [[8, "src", 4]], null, null, null, null)), (_l()(), core["ɵeld"](5, 0, null, null, 1, "div", [["class", "follow-name"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.navigateBasedOnUserId(_v.context.$implicit.id) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), core["ɵted"](6, null, ["", " ", ""])), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileFollowComponent_2)), core["ɵdid"](8, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var _co = _v.component; var currVal_0 = "follow-thumbnail"; var currVal_1 = (_co.withStory((_v.context.$implicit.has_stories.length > 0)) ? "with-story" : ""); _ck(_v, 3, 0, currVal_0, currVal_1); var currVal_5 = (_co.userId !== _v.context.$implicit.id); _ck(_v, 8, 0, currVal_5); }, function (_ck, _v) { var _co = _v.component; var currVal_2 = (_v.context.$implicit.image ? _co.getImageUrl(_v.context.$implicit.image) : "assets/images/no-image.png"); _ck(_v, 4, 0, currVal_2); var currVal_3 = _v.context.$implicit.fname; var currVal_4 = _v.context.$implicit.lname; _ck(_v, 6, 0, currVal_3, currVal_4); }); }
function View_ProfileFollowComponent_0(_l) { return core["ɵvid"](0, [core["ɵpid"](0, common["TitleCasePipe"], []), (_l()(), core["ɵeld"](1, 0, null, null, 8, "div", [["infiniteScroll", ""]], null, [[null, "scrolled"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("scrolled" === en)) {
        var pd_0 = (_co.onScroll() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](2, 4866048, null, 0, ngx_infinite_scroll["a" /* InfiniteScrollDirective */], [core["ElementRef"], core["NgZone"]], { infiniteScrollDistance: [0, "infiniteScrollDistance"], infiniteScrollThrottle: [1, "infiniteScrollThrottle"] }, { scrolled: "scrolled" }), (_l()(), core["ɵeld"](3, 0, null, null, 6, "div", [["class", "follow-page"]], null, null, null, null, null)), (_l()(), core["ɵeld"](4, 0, null, null, 2, "div", [["class", "follow-title"]], null, null, null, null, null)), (_l()(), core["ɵted"](5, null, ["", ""])), core["ɵppd"](6, 1), (_l()(), core["ɵand"](16777216, null, null, 2, null, View_ProfileFollowComponent_1)), core["ɵdid"](8, 278528, null, 0, common["NgForOf"], [core["ViewContainerRef"], core["TemplateRef"], core["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]])], function (_ck, _v) { var _co = _v.component; var currVal_0 = 0.2; var currVal_1 = 300; _ck(_v, 2, 0, currVal_0, currVal_1); var currVal_3 = core["ɵunv"](_v, 8, 0, core["ɵnov"](_v, 9).transform(_co.follows$)); _ck(_v, 8, 0, currVal_3); }, function (_ck, _v) { var _co = _v.component; var currVal_2 = core["ɵunv"](_v, 5, 0, _ck(_v, 6, 0, core["ɵnov"](_v, 0), _co.type)); _ck(_v, 5, 0, currVal_2); }); }
function View_ProfileFollowComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-profile-follow", [], null, null, null, View_ProfileFollowComponent_0, RenderType_ProfileFollowComponent)), core["ɵdid"](1, 49152, null, 0, profile_follow_component_ProfileFollowComponent, [router["a" /* ActivatedRoute */], ngxs_store["j" /* Store */], ng_bootstrap["t" /* NgbModal */]], null, null)], null, null); }
var ProfileFollowComponentNgFactory = core["ɵccf"]("app-profile-follow", profile_follow_component_ProfileFollowComponent, View_ProfileFollowComponent_Host_0, {}, {}, []);


// CONCATENATED MODULE: ./src/app/profile/profile-view-others/profile-view-others.component.scss.shim.ngstyle.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
var profile_view_others_component_scss_shim_ngstyle_styles = [""];


// CONCATENATED MODULE: ./src/app/profile/profile-view-others/profile-view-others.component.ts





// NGXS






// NG-Bootstrap

class profile_view_others_component_ProfileViewOthersComponent {
    constructor(store, route, modalService) {
        this.store = store;
        this.route = route;
        this.modalService = modalService;
        this.currentPage = 1;
        this.route.params.subscribe(paramsId => {
            this.id = paramsId.id;
            this.store.dispatch(new profile_action["l" /* GetOtherProfile */](paramsId.id));
        });
        this.lastPage$.subscribe(val => {
            this.lastPage = val;
        });
        this.store.dispatch(new post_action["j" /* GetOthersPostsFirstPage */]('', '1', this.id));
    }
    getImageUrl(url) {
        return `${environment["a" /* environment */].urlForImage}${url}`;
    }
    onFollow(profile) {
        this.store.dispatch(new profile_action["c" /* FollowUser */](profile)).subscribe(() => {
            this.store.dispatch(new profile_action["l" /* GetOtherProfile */](this.id));
        });
    }
    onScroll() {
        if (this.currentPage < this.lastPage) {
            this.currentPage += 1;
            this.store.dispatch(new post_action["k" /* GetOthersPostsPerPage */]('', this.currentPage.toString(), this.id));
        }
    }
    withStory(stories) {
        if (stories) {
            return true;
        }
        else {
            return false;
        }
    }
    openStoryModal(stories) {
        this.store.dispatch(new story_action["c" /* SelectStory */](stories)).subscribe(() => {
            this.modalService.open(story_modal_component["a" /* StoryModalComponent */]);
        });
    }
}
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(profile_state["a" /* ProfileState */].getProfile),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_view_others_component_ProfileViewOthersComponent.prototype, "profile$", void 0);
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(post_state["a" /* PostState */].getLastPage),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_view_others_component_ProfileViewOthersComponent.prototype, "lastPage$", void 0);
tslib["__decorate"]([
    Object(ngxs_store["f" /* Select */])(post_state["a" /* PostState */].getOwnPost),
    tslib["__metadata"]("design:type", Observable["a" /* Observable */])
], profile_view_others_component_ProfileViewOthersComponent.prototype, "posts$", void 0);

// CONCATENATED MODULE: ./src/app/profile/profile-view-others/profile-view-others.component.ngfactory.js
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 








var styles_ProfileViewOthersComponent = [profile_view_others_component_scss_shim_ngstyle_styles];
var RenderType_ProfileViewOthersComponent = core["ɵcrt"]({ encapsulation: 0, styles: styles_ProfileViewOthersComponent, data: {} });

function View_ProfileViewOthersComponent_2(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 3, "div", [["class", "photo"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.openStoryModal(_v.parent.context.ngIf) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵprd"](512, null, common["ɵNgClassImpl"], common["ɵNgClassR2Impl"], [core["IterableDiffers"], core["KeyValueDiffers"], core["ElementRef"], core["Renderer2"]]), core["ɵdid"](2, 278528, null, 0, common["NgClass"], [common["ɵNgClassImpl"]], { klass: [0, "klass"], ngClass: [1, "ngClass"] }, null), (_l()(), core["ɵeld"](3, 0, null, null, 0, "img", [["alt", ""]], [[8, "src", 4]], null, null, null, null))], function (_ck, _v) { var _co = _v.component; var currVal_0 = "photo"; var currVal_1 = (_co.withStory(((_v.parent.context.ngIf.has_stories.length > 0) ? _v.parent.context.ngIf.has_stories : null)) ? "with-story" : ""); _ck(_v, 2, 0, currVal_0, currVal_1); }, function (_ck, _v) { var _co = _v.component; var currVal_2 = (_v.parent.context.ngIf.image ? _co.getImageUrl(_v.parent.context.ngIf.image) : "assets/images/no-image.png"); _ck(_v, 3, 0, currVal_2); }); }
function View_ProfileViewOthersComponent_3(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "div", [["class", "photo"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 0, null, null, 0, "img", [["alt", ""]], [[8, "src", 4]], null, null, null, null))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = (_v.parent.context.ngIf.image ? _co.getImageUrl(_v.parent.context.ngIf.image) : "assets/images/no-image.png"); _ck(_v, 1, 0, currVal_0); }); }
function View_ProfileViewOthersComponent_5(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 2, "img", [["class", "avatar"]], [[8, "src", 4]], [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 1).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](1, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](2, 2)], function (_ck, _v) { var currVal_1 = _ck(_v, 2, 0, "/post/", _v.parent.context.$implicit.id); _ck(_v, 1, 0, currVal_1); }, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.getImageUrl(_v.parent.context.$implicit.contents[0].content); _ck(_v, 0, 0, currVal_0); }); }
function View_ProfileViewOthersComponent_6(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 4, "div", [["class", "video-overlay"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 1).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](1, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { routerLink: [0, "routerLink"] }, null), core["ɵpad"](2, 2), (_l()(), core["ɵeld"](3, 0, null, null, 1, "video", [], null, null, null, null, null)), (_l()(), core["ɵeld"](4, 0, null, null, 0, "source", [], [[8, "src", 4]], null, null, null, null))], function (_ck, _v) { var currVal_0 = _ck(_v, 2, 0, "/post/", _v.parent.context.$implicit.id); _ck(_v, 1, 0, currVal_0); }, function (_ck, _v) { var _co = _v.component; var currVal_1 = _co.getImageUrl(_v.parent.context.$implicit.contents[0].content); _ck(_v, 4, 0, currVal_1); }); }
function View_ProfileViewOthersComponent_4(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 4, "div", [["class", "item"]], null, null, null, null, null)), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewOthersComponent_5)), core["ɵdid"](2, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewOthersComponent_6)), core["ɵdid"](4, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null)], function (_ck, _v) { var currVal_0 = (_v.context.$implicit.contents[0].type !== "video"); _ck(_v, 2, 0, currVal_0); var currVal_1 = (_v.context.$implicit.contents[0].type === "video"); _ck(_v, 4, 0, currVal_1); }, null); }
function View_ProfileViewOthersComponent_1(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 43, "div", [["class", "profile-view"]], null, null, null, null, null)), (_l()(), core["ɵeld"](1, 0, null, null, 38, "div", [["class", "top"]], null, null, null, null, null)), (_l()(), core["ɵeld"](2, 0, null, null, 9, "div", [["class", "left"]], null, null, null, null, null)), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewOthersComponent_2)), core["ɵdid"](4, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵand"](16777216, null, null, 1, null, View_ProfileViewOthersComponent_3)), core["ɵdid"](6, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), (_l()(), core["ɵeld"](7, 0, null, null, 4, "div", [["class", "text-center"]], null, null, null, null, null)), core["ɵprd"](512, null, common["ɵNgStyleImpl"], common["ɵNgStyleR2Impl"], [core["ElementRef"], core["KeyValueDiffers"], core["Renderer2"]]), core["ɵdid"](9, 278528, null, 0, common["NgStyle"], [common["ɵNgStyleImpl"]], { ngStyle: [0, "ngStyle"] }, null), core["ɵpod"](10, { "color": 0 }), (_l()(), core["ɵted"](11, null, [" ", " "])), (_l()(), core["ɵeld"](12, 0, null, null, 27, "div", [["class", "right"]], null, null, null, null, null)), (_l()(), core["ɵeld"](13, 0, null, null, 1, "div", [["class", "name"]], null, null, null, null, null)), (_l()(), core["ɵted"](14, null, ["", " ", ""])), (_l()(), core["ɵeld"](15, 0, null, null, 1, "div", [["class", "email"]], null, null, null, null, null)), (_l()(), core["ɵted"](16, null, ["", ""])), (_l()(), core["ɵeld"](17, 0, null, null, 1, "button", [["class", "primary button follow"]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.onFollow(_v.context.ngIf) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), core["ɵted"](18, null, [" ", " "])), (_l()(), core["ɵeld"](19, 0, null, null, 18, "div", [["class", "stats"]], null, null, null, null, null)), (_l()(), core["ɵeld"](20, 0, null, null, 6, "div", [["class", "stats-title"], ["div", ""]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 21).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](21, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { queryParams: [0, "queryParams"], routerLink: [1, "routerLink"] }, null), core["ɵpod"](22, { type: 0, id: 1 }), core["ɵpad"](23, 1), (_l()(), core["ɵted"](-1, null, [" Followers "])), (_l()(), core["ɵeld"](25, 0, null, null, 1, "div", [["class", "stats-number"]], null, null, null, null, null)), (_l()(), core["ɵted"](26, null, ["", ""])), (_l()(), core["ɵeld"](27, 0, null, null, 6, "div", [["class", "stats-title"], ["div", ""]], null, [[null, "click"]], function (_v, en, $event) { var ad = true; if (("click" === en)) {
        var pd_0 = (core["ɵnov"](_v, 28).onClick() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](28, 16384, null, 0, router["p" /* RouterLink */], [router["o" /* Router */], router["a" /* ActivatedRoute */], [8, null], core["Renderer2"], core["ElementRef"]], { queryParams: [0, "queryParams"], routerLink: [1, "routerLink"] }, null), core["ɵpod"](29, { type: 0, id: 1 }), core["ɵpad"](30, 1), (_l()(), core["ɵted"](-1, null, [" Following "])), (_l()(), core["ɵeld"](32, 0, null, null, 1, "div", [["class", "stats-number"]], null, null, null, null, null)), (_l()(), core["ɵted"](33, null, ["", ""])), (_l()(), core["ɵeld"](34, 0, null, null, 3, "div", [["class", "stats-title"]], null, null, null, null, null)), (_l()(), core["ɵted"](-1, null, [" Posts "])), (_l()(), core["ɵeld"](36, 0, null, null, 1, "div", [["class", "stats-number"]], null, null, null, null, null)), (_l()(), core["ɵted"](37, null, ["", ""])), (_l()(), core["ɵeld"](38, 0, null, null, 1, "div", [["class", "description"]], null, null, null, null, null)), (_l()(), core["ɵted"](39, null, [" ", " "])), (_l()(), core["ɵeld"](40, 0, null, null, 3, "div", [["class", "posts-collection mt3"]], null, null, null, null, null)), (_l()(), core["ɵand"](16777216, null, null, 2, null, View_ProfileViewOthersComponent_4)), core["ɵdid"](42, 278528, null, 0, common["NgForOf"], [core["ViewContainerRef"], core["TemplateRef"], core["IterableDiffers"]], { ngForOf: [0, "ngForOf"] }, null), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]])], function (_ck, _v) { var _co = _v.component; var currVal_0 = (_v.context.ngIf.has_stories.length > 0); _ck(_v, 4, 0, currVal_0); var currVal_1 = (_v.context.ngIf.has_stories.length === 0); _ck(_v, 6, 0, currVal_1); var currVal_2 = _ck(_v, 10, 0, "#656565"); _ck(_v, 9, 0, currVal_2); var currVal_8 = _ck(_v, 22, 0, "followers", _v.context.ngIf.id); var currVal_9 = _ck(_v, 23, 0, "/profile/follow"); _ck(_v, 21, 0, currVal_8, currVal_9); var currVal_11 = _ck(_v, 29, 0, "following", _v.context.ngIf.id); var currVal_12 = _ck(_v, 30, 0, "/profile/follow"); _ck(_v, 28, 0, currVal_11, currVal_12); var currVal_16 = core["ɵunv"](_v, 42, 0, core["ɵnov"](_v, 43).transform(_co.posts$)); _ck(_v, 42, 0, currVal_16); }, function (_ck, _v) { var currVal_3 = ((_v.context.ngIf == null) ? null : ((_v.context.ngIf.country == null) ? null : _v.context.ngIf.country.country_name)); _ck(_v, 11, 0, currVal_3); var currVal_4 = _v.context.ngIf.fname; var currVal_5 = _v.context.ngIf.lname; _ck(_v, 14, 0, currVal_4, currVal_5); var currVal_6 = _v.context.ngIf.email; _ck(_v, 16, 0, currVal_6); var currVal_7 = ((_v.context.ngIf.is_followed == 0) ? "Follow" : "Unfollow"); _ck(_v, 18, 0, currVal_7); var currVal_10 = _v.context.ngIf.followers_count; _ck(_v, 26, 0, currVal_10); var currVal_13 = _v.context.ngIf.following_count; _ck(_v, 33, 0, currVal_13); var currVal_14 = _v.context.ngIf.num_post; _ck(_v, 37, 0, currVal_14); var currVal_15 = _v.context.ngIf.description; _ck(_v, 39, 0, currVal_15); }); }
function View_ProfileViewOthersComponent_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 4, "div", [["infiniteScroll", ""]], null, [[null, "scrolled"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("scrolled" === en)) {
        var pd_0 = (_co.onScroll() !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), core["ɵdid"](1, 4866048, null, 0, ngx_infinite_scroll["a" /* InfiniteScrollDirective */], [core["ElementRef"], core["NgZone"]], { infiniteScrollDistance: [0, "infiniteScrollDistance"], infiniteScrollThrottle: [1, "infiniteScrollThrottle"] }, { scrolled: "scrolled" }), (_l()(), core["ɵand"](16777216, null, null, 2, null, View_ProfileViewOthersComponent_1)), core["ɵdid"](3, 16384, null, 0, common["NgIf"], [core["ViewContainerRef"], core["TemplateRef"]], { ngIf: [0, "ngIf"] }, null), core["ɵpid"](131072, common["AsyncPipe"], [core["ChangeDetectorRef"]])], function (_ck, _v) { var _co = _v.component; var currVal_0 = 0.2; var currVal_1 = 300; _ck(_v, 1, 0, currVal_0, currVal_1); var currVal_2 = core["ɵunv"](_v, 3, 0, core["ɵnov"](_v, 4).transform(_co.profile$)); _ck(_v, 3, 0, currVal_2); }, null); }
function View_ProfileViewOthersComponent_Host_0(_l) { return core["ɵvid"](0, [(_l()(), core["ɵeld"](0, 0, null, null, 1, "app-profile-view-others", [], null, null, null, View_ProfileViewOthersComponent_0, RenderType_ProfileViewOthersComponent)), core["ɵdid"](1, 49152, null, 0, profile_view_others_component_ProfileViewOthersComponent, [ngxs_store["j" /* Store */], router["a" /* ActivatedRoute */], ng_bootstrap["t" /* NgbModal */]], null, null)], null, null); }
var ProfileViewOthersComponentNgFactory = core["ɵccf"]("app-profile-view-others", profile_view_others_component_ProfileViewOthersComponent, View_ProfileViewOthersComponent_Host_0, {}, {}, []);


// EXTERNAL MODULE: ./node_modules/@ng-bootstrap/ng-bootstrap/ng-bootstrap.ngfactory.js
var ng_bootstrap_ngfactory = __webpack_require__("9AJC");

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.component.ngfactory.js
var inline_svg_component_ngfactory = __webpack_require__("yBPo");

// EXTERNAL MODULE: ./src/app/core/services/countries.service.ts
var countries_service = __webpack_require__("NtT5");

// CONCATENATED MODULE: ./src/app/profile/profile-routing.module.ts



// import { EditProfileGuard } from '@app/core/guards/edit-profile-guard.service';


const routes = [
    {
        path: '',
        component: profile_view_component_ProfileViewComponent
    },
    {
        path: 'edit',
        component: profile_edit_component_ProfileEditComponent
    },
    {
        path: 'follow',
        component: profile_follow_component_ProfileFollowComponent
    },
    {
        path: ':id',
        component: profile_view_others_component_ProfileViewOthersComponent
    }
];
class ProfileRoutingModule {
}

// EXTERNAL MODULE: ./node_modules/ng-inline-svg/lib/inline-svg.module.js
var inline_svg_module = __webpack_require__("0ibv");

// EXTERNAL MODULE: ./node_modules/@ngxs/store/fesm2015/ngxs-store-internals.js
var ngxs_store_internals = __webpack_require__("Mrqg");

// EXTERNAL MODULE: ./src/app/shared/shared.module.ts
var shared_module = __webpack_require__("PCNd");

// CONCATENATED MODULE: ./src/app/profile/profile.module.ngfactory.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileModuleNgFactory", function() { return ProfileModuleNgFactory; });
/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 




























var ProfileModuleNgFactory = core["ɵcmf"](ProfileModule, [], function (_l) { return core["ɵmod"]([core["ɵmpd"](512, core["ComponentFactoryResolver"], core["ɵCodegenComponentFactoryResolver"], [[8, [router_ngfactory["a" /* ɵangular_packages_router_router_lNgFactory */], ProfileViewComponentNgFactory, ProfileEditComponentNgFactory, ProfileFollowComponentNgFactory, ProfileViewOthersComponentNgFactory, angular_fontawesome_ngfactory["b" /* FaIconComponentNgFactory */], angular_fontawesome_ngfactory["a" /* FaDuotoneIconComponentNgFactory */], ng_bootstrap_ngfactory["c" /* ɵwNgFactory */], ng_bootstrap_ngfactory["d" /* ɵxNgFactory */], inline_svg_component_ngfactory["a" /* InlineSVGComponentNgFactory */]]], [3, core["ComponentFactoryResolver"]], core["NgModuleRef"]]), core["ɵmpd"](4608, common["NgLocalization"], common["NgLocaleLocalization"], [core["LOCALE_ID"], [2, common["ɵangular_packages_common_common_a"]]]), core["ɵmpd"](4608, fesm2015_forms["d" /* FormBuilder */], fesm2015_forms["d" /* FormBuilder */], []), core["ɵmpd"](4608, fesm2015_forms["u" /* ɵangular_packages_forms_forms_o */], fesm2015_forms["u" /* ɵangular_packages_forms_forms_o */], []), core["ɵmpd"](4608, ng_bootstrap["t" /* NgbModal */], ng_bootstrap["t" /* NgbModal */], [core["ComponentFactoryResolver"], core["Injector"], ng_bootstrap["ib" /* ɵy */], ng_bootstrap["u" /* NgbModalConfig */]]), core["ɵmpd"](4608, ngxs_store["C" /* ɵr */], ngxs_store["C" /* ɵr */], [[3, ngxs_store["C" /* ɵr */]], [2, ngxs_store["d" /* NGXS_PLUGINS */]]]), core["ɵmpd"](4608, country_state["a" /* CountryState */], country_state["a" /* CountryState */], [countries_service["a" /* CountriesService */], ngxs_store["j" /* Store */]]), core["ɵmpd"](1073742336, router["r" /* RouterModule */], router["r" /* RouterModule */], [[2, router["x" /* ɵangular_packages_router_router_a */]], [2, router["o" /* Router */]]]), core["ɵmpd"](1073742336, ProfileRoutingModule, ProfileRoutingModule, []), core["ɵmpd"](1073742336, common["CommonModule"], common["CommonModule"], []), core["ɵmpd"](1073742336, fesm2015_forms["t" /* ɵangular_packages_forms_forms_d */], fesm2015_forms["t" /* ɵangular_packages_forms_forms_d */], []), core["ɵmpd"](1073742336, fesm2015_forms["q" /* ReactiveFormsModule */], fesm2015_forms["q" /* ReactiveFormsModule */], []), core["ɵmpd"](1073742336, angular_fontawesome["j" /* FontAwesomeModule */], angular_fontawesome["j" /* FontAwesomeModule */], []), core["ɵmpd"](1073742336, ng_select_ng_select["c" /* NgSelectModule */], ng_select_ng_select["c" /* NgSelectModule */], []), core["ɵmpd"](1073742336, ng_bootstrap["v" /* NgbModalModule */], ng_bootstrap["v" /* NgbModalModule */], []), core["ɵmpd"](1073742336, inline_svg_module["InlineSVGModule"], inline_svg_module["InlineSVGModule"], []), core["ɵmpd"](1073742336, ngx_infinite_scroll["b" /* InfiniteScrollModule */], ngx_infinite_scroll["b" /* InfiniteScrollModule */], []), core["ɵmpd"](1073742336, ng_bootstrap["k" /* NgbCarouselModule */], ng_bootstrap["k" /* NgbCarouselModule */], []), core["ɵmpd"](512, ngxs_store["x" /* ɵm */], ngxs_store["x" /* ɵm */], [core["Injector"], ngxs_store["u" /* ɵj */], [3, ngxs_store["x" /* ɵm */]], ngxs_store["p" /* ɵb */], ngxs_store["y" /* ɵn */], ngxs_store["A" /* ɵp */], [2, ngxs_store_internals["a" /* INITIAL_STATE_TOKEN */]]]), core["ɵmpd"](1024, ngxs_store["r" /* ɵf */], function () { return [[country_state["a" /* CountryState */]]]; }, []), core["ɵmpd"](1073742336, ngxs_store["I" /* ɵx */], ngxs_store["I" /* ɵx */], [ngxs_store["j" /* Store */], ngxs_store["B" /* ɵq */], ngxs_store["x" /* ɵm */], [2, ngxs_store["r" /* ɵf */]], ngxs_store["H" /* ɵw */]]), core["ɵmpd"](1073742336, shared_module["a" /* SharedModule */], shared_module["a" /* SharedModule */], []), core["ɵmpd"](1073742336, ProfileModule, ProfileModule, []), core["ɵmpd"](1024, router["l" /* ROUTES */], function () { return [[{ path: "", component: profile_view_component_ProfileViewComponent }, { path: "edit", component: profile_edit_component_ProfileEditComponent }, { path: "follow", component: profile_follow_component_ProfileFollowComponent }, { path: ":id", component: profile_view_others_component_ProfileViewOthersComponent }]]; }, []), core["ɵmpd"](256, ng_select_ng_select["d" /* SELECTION_MODEL_FACTORY */], ng_select_ng_select["e" /* ɵb */], [])]); });



/***/ }),

/***/ "xR19":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetAllCountries; });
class GetAllCountries {
}
GetAllCountries.type = '[Sign Up Page] GetAllCountries';


/***/ })

};;